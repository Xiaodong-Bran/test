#include "color_detector_node.h"

int main(int argc, char **argv) {
  ros::init(argc, argv, "color_detector");
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  try {
    color_detector::ColorDetectorNode color_detector_node(nh, pnh);
    ros::spin();
  }
  catch (const std::exception &e) {
    ROS_ERROR("%s: %s", nh.getNamespace().c_str(), e.what());
  }
}
