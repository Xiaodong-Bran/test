#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "color_detector_node.h"

using namespace  std;
using namespace cv;

namespace color_detector {

ColorDetectorNode::ColorDetectorNode(const ros::NodeHandle &nh,
                                     const ros::NodeHandle &pnh)
  : nh_(nh),
    image_count_(0),
    image_skip_(0),
    resize_factor_(1.0),
    contour_area_threshold_(5000),
    cam_height_(1.2),
    cam_calibrated_(true),
    detect_start_(false),
    it_(nh),
    sub_camera_(it_.subscribeCamera("image_raw", 1, &ColorDetectorNode::CameraCb, this)),
    sub_aprilmission_(nh_.subscribe("april_mission", 1, &ColorDetectorNode::MissionCb, this)),
    pub_detections_(nh_.advertise<sensor_msgs::Image>("detections", 1)),
    pub_landing_pad_(nh_.advertise<apriltag_ros::AprilStatus>("landing_pad_pose",1))
{
  pnh.getParam("image_skip", image_skip_);
  ROS_INFO("Setting to skip every %d image", image_skip_);
  pnh.getParam("resize_factor", resize_factor_);
  ROS_INFO("Setting image resize factor of %f", resize_factor_);
  pnh.getParam("contour_area_threshold", contour_area_threshold_);
  ROS_INFO("Setting to contour_area_threshold of %f", contour_area_threshold_);
  pnh.getParam("cam_height", cam_height_);
  ROS_INFO("Setting to cam_height of %f", cam_height_);
}

void ColorDetectorNode::CameraCb(const sensor_msgs::ImageConstPtr &image_msg,
                                 const sensor_msgs::CameraInfoConstPtr &cinfo_msg)
{
  // Wait until accepting mission start
  if(!detect_start_)
    return;

  // Skip some image
  image_count_ ++;
  if(image_count_ % (image_skip_+1))
    return;

  // Show only the detection if camera is uncalibrated
  if (cinfo_msg->K[0] == 0.0 || cinfo_msg->height == 0) {
    ROS_ERROR_THROTTLE(10, "%s: %s", nh_.getNamespace().c_str(), "Camera not calibrated!");
    cam_calibrated_ = false;
  }

  // Retrieve camera info and image
  model_.fromCameraInfo(cinfo_msg);
  // Modify the camera calibration according to resize factor
  cv::Matx33d cam_K = model_.fullIntrinsicMatrix();
  cam_K(0,0) = cam_K(0,0)/resize_factor_;
  cam_K(1,1) = cam_K(1,1)/resize_factor_;
  cam_K(0,2) = cam_K(0,2)/resize_factor_;
  cam_K(1,2) = cam_K(1,2)/resize_factor_;
  cv::Mat_<double> cam_D = model_.distortionCoeffs().clone();

  // Read image
  cv_bridge::CvImagePtr cv_ptr;
  try{
    cv_ptr = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::BGR8); //MONO8
  }
  catch (cv_bridge::Exception& e){
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  // Resize image
  cv::Mat gray_image;
  cv::resize(cv_ptr->image, cv_ptr->image, cv::Size(cv_ptr->image.size().width/resize_factor_, cv_ptr->image.size().height/resize_factor_));
  cv::cvtColor(cv_ptr->image, gray_image, CV_BGR2GRAY);

  // Red color detection
  cv::Mat hsv_image;
  cv::cvtColor(cv_ptr->image, hsv_image, CV_BGR2HSV );
  cv::Mat lower_red_hue_range;
  cv::Mat upper_red_hue_range;
  cv::inRange(hsv_image, cv::Scalar(0, 40, 50), cv::Scalar(10, 255, 255), lower_red_hue_range);
  cv::inRange(hsv_image, cv::Scalar(160, 40,50), cv::Scalar(179, 255, 255), upper_red_hue_range);

  // Combine the above two images
  cv::Mat red_hue_image;
  cv::addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);
  cv::Mat red_hue_image_gaussian;
  cv::GaussianBlur(red_hue_image, red_hue_image_gaussian, cv::Size(9, 9), 2, 2);

  // Find contours
  vector<vector<cv::Point> > contours;
  vector<cv::Vec4i> hierarchy;
  cv::findContours(red_hue_image, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

  // Approximate contours to polygons + get bounding rects and circles
  vector< vector<Point> > v_contours_poly;
  vector<Rect> v_bound_rect;
  vector<Point2f> v_center;
  vector<float> v_radius;

  // Select the large area contour and get centers
  for( size_t i = 0; i < contours.size(); i++ )
  {
    if( cv::contourArea(contours[i])>contour_area_threshold_ )
    {
      vector<Point> contours_poly;
      cv::approxPolyDP( cv::Mat(contours[i]), contours_poly, 3, true );
      v_contours_poly.push_back(contours_poly);
      v_bound_rect.push_back( boundingRect( cv::Mat(contours_poly) ) );
      cv::Point2f center;
      float radius;
      cv::minEnclosingCircle( (cv::Mat)contours_poly, center, radius );
      v_center.push_back(center);
      v_radius.push_back(radius);
    }
  }

  double max_contour_area=0;
  size_t max_contour_index=0;
  cv::Point2f max_center;
  float max_radius=0;
  for( size_t i = 0; i< v_contours_poly.size(); i++ )
  {
    if(cv::contourArea(v_contours_poly[i])>max_contour_area)
    {
      max_contour_area = cv::contourArea(v_contours_poly[i]);
      max_contour_index = i;
      max_center = v_center[i];
      max_radius = v_radius[i];
    }
  }

  if( v_contours_poly.size()>0 )
  {
    // Draw polygonal contour + bonding rects + circles
    cv::Mat drawing = cv_ptr->image.clone();
    // Show detection
    cv::Scalar color = cv::Scalar( 0,255,0 );
    cv::circle( drawing, max_center, max_radius, color, 2, 8, 0 );
    sensor_msgs::ImagePtr detection_msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", drawing).toImageMsg();
    pub_detections_.publish(detection_msg);

    cv::Mat_<cv::Point2f> points(1,1);
    points(0) = cv::Point2f(max_center.x, max_center.y);;
    cv::Mat points_undist;
    cv::undistortPoints(points, points_undist, cam_K, cam_D);
    cv::Point2f found_center_undist(points_undist.at<float>(0,0), points_undist.at<float>(0,1));
    //cout << "found center:" << found_center.x << " " << found_center.y << endl;
    //cout << "found center undist:" << found_center_undist.x << " " << found_center_undist.y << endl;

    // TODO: distortion correction
    double cam_x = cam_height_ * found_center_undist.x;
    double cam_y = cam_height_ * found_center_undist.y;
    double cam_z = cam_height_;

    // Get tray position
    geometry_msgs::Pose tray_pose_NWU;
    tray_pose_NWU.position.x = -cam_y;
    tray_pose_NWU.position.y = -cam_x;
    tray_pose_NWU.position.z = -cam_z;
    tray_pose_NWU.orientation.x = 0;
    tray_pose_NWU.orientation.y = 0;
    tray_pose_NWU.orientation.z = 0;
    tray_pose_NWU.orientation.w = 1;

    // Publish apriltag status
    apriltag_ros::AprilStatus tray_status;
    tray_status.pose = tray_pose_NWU;
    tray_status.is_valid = true;
    tray_status.mission_type = "tray";
    pub_landing_pad_.publish(tray_status);
  }
}

void ColorDetectorNode::MissionCb(const apriltag_ros::AprilMissionConstPtr &mission_msg)
{
  std::string april_status = mission_msg->status;
  std::string april_mission = mission_msg->mission_type;

  if( april_status == "start")
  {
    detect_start_ = true;
    ROS_INFO("color detection start!");
  }

  if(april_status == "stop")
  {
    detect_start_ = false;
    ROS_INFO("color detection stop!");
  }
}

}  // namespace color_detector
