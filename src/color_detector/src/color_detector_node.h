#ifndef COLOR_DETECTOR_NODE_H_
#define COLOR_DETECTOR_NODE_H_

#include <iostream>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_geometry/pinhole_camera_model.h>

#include <apriltag_ros/AprilMission.h>
#include <apriltag_ros/AprilStatus.h>

namespace color_detector {

class ColorDetectorNode {
 public:
  ColorDetectorNode(const ros::NodeHandle &nh, const ros::NodeHandle &pnh);

 private:
  void CameraCb(const sensor_msgs::ImageConstPtr &image_msg, const sensor_msgs::CameraInfoConstPtr &cinfo_msg);
  void MissionCb(const apriltag_ros::AprilMissionConstPtr &mission_msg);

  ros::NodeHandle nh_;
  int image_count_;
  int image_skip_;
  double resize_factor_;
  double contour_area_threshold_;
  double cam_height_;                       //assume camera if fixed parrallel to ground plane
  bool cam_calibrated_;
  bool detect_start_;

  image_transport::ImageTransport it_;
  image_transport::CameraSubscriber sub_camera_;
  ros::Subscriber sub_aprilmission_;
  ros::Publisher pub_detections_;
  ros::Publisher pub_landing_pad_;

  image_geometry::PinholeCameraModel model_;
};

}  // namespace color_detector

#endif  // COLOR_DETECTOR_NODE_H_
