#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <sstream>
#include <fstream>

#include <ros/ros.h>
#include <sensor_msgs/Image.h>

#include "common_msgs/DetectionController.h"

#include "apriltag_ros/Apriltags.h"
#include "apriltag_ros/AprilMission.h"

namespace vision {


class Controller
{
public:
    Controller(ros::NodeHandle& nh):m_nh(nh){
        m_detection_server = m_nh.advertiseService("DetectionController", &Controller::detectionControllerSrvCallback, this);

        m_april_tags_sub = m_nh.subscribe("/apriltag_ros/apriltags", 10, &Controller::aprilTagsCallback, this);
        m_april_detected_img_sub = m_nh.subscribe("/apriltag_ros/detections", 10, &Controller::aprilTagImgCallback, this);
        m_april_tag_contol_pub = m_nh.advertise<apriltag_ros::AprilMission>("/apriltag_ros/april_mission", 10);
        m_april_is_on = false;
    }

private:
    ros::NodeHandle m_nh;

    // provider service for mission controller
    ros::ServiceServer m_detection_server;

    // April tag
    ros::Subscriber m_april_tags_sub;
    ros::Subscriber m_april_detected_img_sub;
    apriltag_ros::Apriltags m_april_tags;
    sensor_msgs::Image m_april_detected_img;

    ros::Publisher m_april_tag_contol_pub;
    bool m_april_is_on;

public:
    bool detectionControllerSrvCallback(common_msgs::DetectionController::Request &request,
                                        common_msgs::DetectionController::Response &response);

    void aprilTagsCallback(const apriltag_ros::Apriltags::ConstPtr& msg);
    void aprilTagImgCallback(const sensor_msgs::Image::ConstPtr& msg);

};




}



#endif // CONTROLLER_HPP
