#include "detection_controller/controller.hpp"

int main(int argc, char **argv){
    //initialize ros
    ros::init(argc, argv, "detection_controller");
    ros::NodeHandle controller_node("~");

    vision::Controller controller(controller_node);

    ROS_INFO("controller running...");
    ros::spin();
    return 0;
}
