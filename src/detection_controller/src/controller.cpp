#include "controller.hpp"


bool vision::Controller::detectionControllerSrvCallback(common_msgs::DetectionController::Request &request,
                                                        common_msgs::DetectionController::Response &response){
    ROS_INFO("Received command from mission management.");
    if(std::string("april_tag")==request.type){
        if(request.switch_cmd==true){
            // turn on the april detection if necessary
            ROS_INFO("APP");
            if(false==m_april_is_on){
                apriltag_ros::AprilMission control_msg;

                control_msg.status=std::string("start");
                control_msg.mission_type = std::string("landing_pad");
                m_april_tag_contol_pub.publish(control_msg);
                ROS_INFO("Send to laobi");
            }

            // anyway, return the detection result
            if(m_april_tags.apriltags.size()==0){
                response.detect_status = 0;
            } else {
                double time_diff_sec = std::fabs(m_april_tags.header.stamp.toSec()-ros::Time::now().toSec());
                if(time_diff_sec<1){
                    response.detect_status = 1;
                    response.pose = m_april_tags.apriltags.at(0).pose;
                } else {
                    response.detect_status = 0;
                }
                ROS_INFO("sending back response:x=%f,y=%f,z=%f",response.pose.position.x,response.pose.position.y,response.pose.position.z);

            }

            m_april_is_on = true;

        } else {
            // turn off the april detection if necessary
            if(true==m_april_is_on){
                apriltag_ros::AprilMission control_msg;
                control_msg.status=std::string("stop");
                control_msg.mission_type = std::string("landing_pad");
                m_april_tag_contol_pub.publish(control_msg);
            }

            m_april_is_on = false;
        }
    }

    return true;

}


void vision::Controller::aprilTagsCallback(const apriltag_ros::Apriltags::ConstPtr &msg){
    m_april_tags = *msg;
    geometry_msgs::Pose landing_pad_pose_NWU;

    // Suppose camera facing downwards looking forward along the head of drone
    // Transform to body-NWU, N=-y, W=-x, U=-z.
    landing_pad_pose_NWU.position.x = -m_april_tags.apriltags.at(0).pose.position.y;
    landing_pad_pose_NWU.position.y = -m_april_tags.apriltags.at(0).pose.position.x;
    landing_pad_pose_NWU.position.z = -m_april_tags.apriltags.at(0).pose.position.z;

    // Not use orientation now.
    landing_pad_pose_NWU.orientation.x = 0;
    landing_pad_pose_NWU.orientation.y = 0;
    landing_pad_pose_NWU.orientation.z = 0;
    landing_pad_pose_NWU.orientation.w = 1;
    m_april_tags.apriltags.at(0).pose = landing_pad_pose_NWU;
    m_april_tags.header.stamp = ros::Time::now();
}

void vision::Controller::aprilTagImgCallback(const sensor_msgs::Image::ConstPtr &msg){
    m_april_detected_img = *msg;
    m_april_detected_img.header.stamp = ros::Time::now();
}
