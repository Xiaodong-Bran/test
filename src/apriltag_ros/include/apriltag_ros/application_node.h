#ifndef APRILTAG_ROS_DETECTOR_NODE_H_
#define APRILTAG_ROS_DETECTOR_NODE_H_

#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/TransformStamped.h>
#include <image_geometry/pinhole_camera_model.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>

#include <iostream>
// apriltag_ros header
#include "apriltag_ros/visualizer.h"

// msg
#include "apriltag_ros/Apriltag.h"
#include "apriltag_ros/AprilMission.h"
#include "apriltag_ros/AprilStatus.h"

namespace apriltag_ros {

class ApplicationNode {
public:
  ApplicationNode(const ros::NodeHandle& nh, const ros::NodeHandle& pnh);

private:
  void TagsCb(const apriltag_ros::ApriltagsConstPtr& tags_msg);
  void CinfoCb(const sensor_msgs::CameraInfoConstPtr& cinfo_msg);
  void MissionCb(const apriltag_ros::AprilMissionConstPtr& aprilmission_msg);

  ros::NodeHandle nh_;
  ros::NodeHandle pnh_;
  int landing_pad_id_;
  int tray_id_;
  double tray_offset_x_;
  double tray_offset_y_;
  double tray_offset_z_;
  int mission_start_;
  ros::Subscriber sub_tags_;
  ros::Subscriber sub_cinfo_;
  ros::Subscriber sub_aprilmission_;
  ros::Publisher pub_landing_pad_;
  apriltag_ros::ApriltagVisualizer tag_viz_;
  image_geometry::PinholeCameraModel pinhole_model_;
  tf2_ros::TransformBroadcaster tf_broadcaster_;
};

}  // namespace apriltag_ros

#endif  // APRILTAG_ROS_APPLICATION_NODE_H_
