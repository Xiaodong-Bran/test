#include "apriltag_ros/application_node.h"
#include "apriltag_ros/utils.h"

#include <eigen3/Eigen/Dense>

namespace apriltag_ros {

ApplicationNode::ApplicationNode(const ros::NodeHandle& nh, const ros::NodeHandle& pnh)
  : nh_(nh),
    landing_pad_id_(8),
    tray_id_(6),
    tray_offset_x_(0),
    tray_offset_y_(0),
    tray_offset_z_(0),
    mission_start_(0),
    sub_tags_(nh_.subscribe("apriltags", 1, &ApplicationNode::TagsCb, this)),
    sub_cinfo_(nh_.subscribe("camera_info", 1, &ApplicationNode::CinfoCb, this)),
    sub_aprilmission_(nh_.subscribe("april_mission", 1, &ApplicationNode::MissionCb, this)),
    pub_landing_pad_(nh_.advertise<apriltag_ros::AprilStatus>("landing_pad_pose",1)),
    tag_viz_(nh_, "apriltags_show")
{
  pnh.getParam("landing_pad_id", landing_pad_id_);
  pnh.getParam("tray_id", tray_id_);
  pnh.getParam("tray_offset_x", tray_offset_x_);
  pnh.getParam("tray_offset_y", tray_offset_y_);
  pnh.getParam("tray_offset_z", tray_offset_z_);
  ROS_INFO("Setting landing_pad_id to %d", landing_pad_id_);
  ROS_INFO("Setting tray_id to %d", tray_id_);
  tag_viz_.SetColor(apriltag_ros::GREEN);
  tag_viz_.SetAlpha(0.75);
}

void ApplicationNode::TagsCb(const apriltag_ros::ApriltagsConstPtr& tags_msg)
{
  // Do nothing if no detection, this prevents checking in the following steps
  if (tags_msg->apriltags.empty()) {
    ROS_WARN_THROTTLE(1, "No tags detected.");
    return;
  }
  // Do nothing if camera info not received
  if (!pinhole_model_.initialized()) {
    ROS_WARN_THROTTLE(1, "No camera info received");
    return;
  }

  // Check the detected tags
  std::vector<Apriltag> tags = tags_msg->apriltags;

  // Handle landing_pad pose
  bool landing_pad_found = false;
  for (size_t i=0; i<tags.size(); i++)
  {
    if(tags[i].id == landing_pad_id_)
    {
      landing_pad_found = true;
      ROS_INFO("found new landing pad!");

      geometry_msgs::Pose landing_pad_pose_NWU;

      // Suppose camera facing downwards looking forward along the head of drone
      // Transform to body-NWU, N=-y, W=-x, U=-z.
      landing_pad_pose_NWU.position.x = -tags[i].pose.position.y;
      landing_pad_pose_NWU.position.y = -tags[i].pose.position.x;
      landing_pad_pose_NWU.position.z = -tags[i].pose.position.z;

      // Not use orientation now.
      landing_pad_pose_NWU.orientation.x = 0;
      landing_pad_pose_NWU.orientation.y = 0;
      landing_pad_pose_NWU.orientation.z = 0;
      landing_pad_pose_NWU.orientation.w = 1;

      // Publish apriltag status
      apriltag_ros::AprilStatus landing_pad_status;
      landing_pad_status.pose = landing_pad_pose_NWU;
      landing_pad_status.is_valid = true;
      landing_pad_status.mission_type = "landing_pad";
      pub_landing_pad_.publish(landing_pad_status);
    }

    // if not found, set to 0
    if(!landing_pad_found)
    {
      geometry_msgs::Pose landing_pad_pose;
      landing_pad_pose.position.x = 0;
      landing_pad_pose.position.y = 0;
      landing_pad_pose.position.z = 0;
      landing_pad_pose.orientation.x = 0;
      landing_pad_pose.orientation.y = 0;
      landing_pad_pose.orientation.z = 0;
      landing_pad_pose.orientation.w = 1;
    }

    // Handle tray pose
    bool tray_found = false;
    if(tags[i].id == tray_id_)
    {
      tray_found = true;
      ROS_INFO("found new tray!");

      // Correct the position using the offset in local apriltag coordinate
      geometry_msgs::Pose tray_pose_NWU_biased;
      Eigen::Quaterniond q_0(tags[i].pose.orientation.w,tags[i].pose.orientation.x, tags[i].pose.orientation.y,tags[i].pose.orientation.z);
      Eigen::Matrix3d R_0 = q_0.toRotationMatrix();
      Eigen::Vector3d traypos_offset(tray_offset_x_, tray_offset_y_, tray_offset_z_);
      Eigen::Vector3d traypos_CAM(tags[i].pose.position.x, tags[i].pose.position.y, tags[i].pose.position.z);
      Eigen::Vector3d traypos_biased = traypos_CAM + R_0 * traypos_offset;

      tray_pose_NWU_biased.position.x = - traypos_biased(1);
      tray_pose_NWU_biased.position.y = - traypos_biased(0);
      tray_pose_NWU_biased.position.z = - traypos_biased(2);
      tray_pose_NWU_biased.orientation.x = 0;
      tray_pose_NWU_biased.orientation.y = 0;
      tray_pose_NWU_biased.orientation.z = 0;
      tray_pose_NWU_biased.orientation.w = 1;

      // Publish apriltag status
      apriltag_ros::AprilStatus tray_status;
      // !!For Upboard setup
      tray_status.pose = tray_pose_NWU_biased;
      tray_status.is_valid = true;
      tray_status.mission_type = "tray";
      // Todo: change to seperate topics
      pub_landing_pad_.publish(tray_status);
    }

    // if not found, set to 0
    if(!tray_found)
    {
      geometry_msgs::Pose tray_pose;
      tray_pose.position.x = 0;
      tray_pose.position.y = 0;
      tray_pose.position.z = 0;
      tray_pose.orientation.x = 0;
      tray_pose.orientation.y = 0;
      tray_pose.orientation.z = 0;
      tray_pose.orientation.w = 1;
    }
  }

}

void ApplicationNode::CinfoCb(const sensor_msgs::CameraInfoConstPtr& cinfo_msg)
{
  if (pinhole_model_.initialized())
  {
    sub_cinfo_.shutdown();
    ROS_INFO("%s: %s", nh_.getNamespace().c_str(), "Camera initialized");
    return;
  }
  pinhole_model_.fromCameraInfo(cinfo_msg);
}

void ApplicationNode::MissionCb(const AprilMissionConstPtr &aprilmission_msg)
{
  std::string april_status = aprilmission_msg->status;
  std::string april_mission = aprilmission_msg->mission_type;
}

}  // namespace apriltag_ros
