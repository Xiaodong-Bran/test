#include "qrReader.h"

void qrReader::read()
{
	// Create a zbar reader
	ImageScanner scanner;
	// Configure the reader
	scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1);	
	// Convert to grayscale
	cvtColor(frame, frame_grayscale, CV_BGR2GRAY);  
    // Obtain image data
    int width = frame_grayscale.cols;
    int height = frame_grayscale.rows;
    uchar *raw = (uchar *)(frame_grayscale.data);
    // Wrap image data
    Image image(width, height, "Y800", raw, width * height);                
    // Scan the image for barcodes
    //int n = scanner.scan(image);
    scanner.scan(image);
    // Extract results
    int counter = 0;
    for (Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol) {
        time_t now;
        tm *current;
        now = time(0);
        current = localtime(&now);

        // do something useful with results
        cout    << "[" << current->tm_hour << ":" << current->tm_min << ":" << setw(2) << setfill('0') << current->tm_sec << "] " << counter << " "
                << "decoded " << symbol->get_type_name()
                << " symbol \"" << symbol->get_data() << '"' << endl;

        //cout << "Location: (" << symbol->get_location_x(0) << "," << symbol->get_location_y(0) << ")" << endl;
        //cout << "Size: " << symbol->get_location_size() << endl;
      
	fResults = fopen("/home/odroid/imav_catkin_ws/qr2gps.txt", "w"); 	
	fprintf(fResults, "%s \n", symbol->get_data().c_str());	
	fclose(fResults); 

        // Draw location of the symbols found
        if (symbol->get_location_size() == 4) {
            //rectangle(frame, Rect(symbol->get_location_x(i), symbol->get_location_y(i), 10, 10), Scalar(0, 255, 0));
            line(frame, Point(symbol->get_location_x(0), symbol->get_location_y(0)), Point(symbol->get_location_x(1), symbol->get_location_y(1)), Scalar(0, 255, 0), 2, 8, 0);
            line(frame, Point(symbol->get_location_x(1), symbol->get_location_y(1)), Point(symbol->get_location_x(2), symbol->get_location_y(2)), Scalar(0, 255, 0), 2, 8, 0);
            line(frame, Point(symbol->get_location_x(2), symbol->get_location_y(2)), Point(symbol->get_location_x(3), symbol->get_location_y(3)), Scalar(0, 255, 0), 2, 8, 0);
            line(frame, Point(symbol->get_location_x(3), symbol->get_location_y(3)), Point(symbol->get_location_x(0), symbol->get_location_y(0)), Scalar(0, 255, 0), 2, 8, 0);
        }
        
	imshow("frame", frame);
	waitKey(5);

        // Get points
        /*for (Symbol::PointIterator point = symbol.point_begin(); point != symbol.point_end(); ++point) {
            cout << point << endl;
        } */
        counter++;
    }
    // clean up
    image.set_data(NULL, 0); 	
}
