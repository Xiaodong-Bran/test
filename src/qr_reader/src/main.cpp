#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "qrReader.h"

#define DISPLAY 0

using namespace std;
using namespace cv;

int main (int argc, char** argv) {

qrReader my_reader;
  VideoCapture cap(0);

    if (!cap.isOpened())  // if not success, exit program
    {
        cout << "Cannot open the video cam" << endl;
        return -1;
    }


    while (1)
    {
        Mat frame;

        bool bSuccess = cap.read(frame); // read a new frame from video

        if (bSuccess) //if not success, break loop
        {

	imshow("frame", frame);
	waitKey(5);

  frame.copyTo(my_reader.frame);
  my_reader.read();
        }



}

  return 0;
}
