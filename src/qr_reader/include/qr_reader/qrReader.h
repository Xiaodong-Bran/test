#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <zbar.h>
#include <iostream>
#include <iomanip>

using namespace cv;
using namespace std;
using namespace zbar;

class qrReader
{
public:
	qrReader()
	{	
	fResults = fopen("qr2gps.txt", "w"); 	
	fclose(fResults);  	
	};
	void read();
	Mat frame;

	FILE *fResults;

private:
	Mat frame_grayscale;
};