#ifndef BASIC_FUNCTION_H_
#define BASIC_FUNCTION_H_

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <sstream>
#include <fstream>

#include <ros/ros.h>
#include "std_msgs/UInt32.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Bool.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "sensor_msgs/NavSatFix.h"
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/MagneticField.h"
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_datatypes.h>
#include <ros/console.h>
#include "sensor_msgs/Range.h"

#include "common_msgs/NavigationState.h"
#include "common_msgs/GPSVelocity.h"
#include "common_msgs/MeasurementPosition.h"
#include "common_msgs/MeasurementVelocity.h"
#include "common_msgs/MeasurementPosVel.h"
#include "common_msgs/PixhawkCMD.h"
#include "common_msgs/PixhawkServo.h"
#include "common_msgs/MaxDynamics.h"
#include "common_msgs/Reference.h"
#include "common_msgs/Mission.h"
#include "common_msgs/MissionStatus.h"
#include "common_msgs/PathPlanningStatus.h"

#include "common_msgs/UWB_FullRangeInfo.h"
#include "common_msgs/UWB_EchoedRangeInfo.h"
#include "common_msgs/UWB_FullNeighborDatabase.h"
#include "common_msgs/UWB_DataInfo.h"
#include "common_msgs/UWB_SendData.h"

#include "nav_msgs/Odometry.h"


// ros callback
#include <ros/callback_queue.h>
#include <ros/callback_queue_interface.h>
#include <ros/spinner.h>


// boost
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>

namespace uavos{
namespace helper {

// print
// ---------------------------------------------------------------------
void printNavigationPose(const common_msgs::NavigationState& navigation_state, int level_of_detail=0);


// type conversion
// ---------------------------------------------------------------------

// the small delta is to ensure that 180 degree is remain 180, instead of -180.
//#define INPI(angle)		(angle -= floor((angle+M_PI-0.0000000001)/(2*M_PI))*2*M_PI)
//#define INPI(angle)		(angle -= floor((angle+M_PI)/(2*M_PI))*2*M_PI)
template<typename T>
inline std::string num2str(const T src){
    std::stringstream ss;
    ss << src;
    return ss.str();
}

template<typename T>
inline T INPI(const T src){
    T dst = src;

    // the small delta is to ensure that 180 degree is remain 180, instead of -180.
    dst -= floor((dst+M_PI-0.0000000001)/(2*M_PI))*2*M_PI;
    return dst;
}

void quaternion2RPY(const geometry_msgs::Quaternion q, double &roll, double &pitch, double &yaw);
void quaternion2RPY(const tf::Quaternion q, double &roll, double &pitch, double &yaw);
void navigation2RPY(const common_msgs::NavigationState state, double &roll, double &pitch, double &yaw);
common_msgs::NavigationState vector2Navigation(double x, double y, double z, double c,
                                               double vx, double vy, double vz, double vc,
                                               double ax, double ay, double az, double ac);
common_msgs::NavigationState navigationNWU2NED(common_msgs::NavigationState state_NWU);
common_msgs::NavigationState navigationNED2NWU(common_msgs::NavigationState state_NED);
common_msgs::Reference referenceNWU2NED(common_msgs::Reference ref_NWU);
common_msgs::Reference referenceNED2NWU(common_msgs::Reference ref_NED);


// variable assignment, check
// ---------------------------------------------------------------------
void setZeroPixhawkCMD(common_msgs::PixhawkCMD& cmd);
void setZeroPixhawkServo(common_msgs::PixhawkServo& servo);
common_msgs::NavigationState getZeroNavigationState();
void setZeroNavigationState(common_msgs::NavigationState& state);
void setZeroVelocityAccleration4NavigationState(common_msgs::NavigationState& state);
void setZeroAccleration4NavigationState(common_msgs::NavigationState& state);
common_msgs::Reference getZeroReference();
void setZeroReference(common_msgs::Reference& ref);
common_msgs::MaxDynamics loadMaxDynamics(ros::NodeHandle& nh, std::string choice);
double readMaxDynamicsMap(const std::map <std::string, double> & m, const std::string & key, const double & defval);

// geometry transform
// ---------------------------------------------------------------------
common_msgs::NavigationState relative2absolute_planar(const common_msgs::NavigationState& relative, const common_msgs::NavigationState& curr_position);
geometry_msgs::Pose relative2absolute_planar(const geometry_msgs::Pose& relative, const geometry_msgs::Pose& curr_position);
geometry_msgs::Pose absolute2relative_planar(const geometry_msgs::Pose offset, const geometry_msgs::Pose curr_position);


// image processing
// ---------------------------------------------------------------------
bool is_inImageBound(const int& x, const int& y, const int& width, const int& height);
bool is_inImageBound_strict(const float& x, const float& y, const int& width, const int& height);



// type arithmetic
// ---------------------------------------------------------------------
template <typename T>
inline void setZeroVector(std::vector<T> &a){
    for(int i=0;i<a.size();++i){
        a.at(i) = 0;
    }
}

template <typename T, const size_t N>
inline boost::array<T,N> vectorSubtraction(const boost::array<T,N> &a, const boost::array<T,N> &b){
    boost::array<T,N> dst;
    for(int i=0;i<a.size();++i){
        dst.at(i) = ( a.at(i)-b.at(i) );
    }
    return dst;
}

template <typename T, const size_t N>
inline boost::array<T,N> vectorAddition(const boost::array<T,N> &a, const boost::array<T,N> &b){
    boost::array<T,N> dst;
    for(int i=0;i<a.size();++i){
        dst.at(i) = ( a.at(i)+b.at(i) );
    }
    return dst;
}

template <typename T, size_t N>
inline void setZeroVector(boost::array<T,N> &a){
    for(int i=0;i<a.size();++i){
        a.at(i) = 0;
    }
}

template <typename T>
inline std::vector<T> vectorSubtraction(const std::vector<T> &a, const std::vector<T> &b){
    std::vector<T> dst;
    for(int i=0;i<a.size();++i){
        dst.push_back( a.at(i)-b.at(i) );
    }
    return dst;
}

template <typename T>
inline std::vector<T> vectorAddition(const std::vector<T> &a, const std::vector<T> &b){
    std::vector<T> dst;
    for(int i=0;i<a.size();++i){
        dst.push_back( a.at(i)+b.at(i) );
    }
    return dst;
}



// IO
// ---------------------------------------------------------------------
int readFromTxt(std::vector<std::vector<double> > &wayPoints, std::string file_name, int columns);
std::vector<double> lineStr2Double(std::string line, std::string separator);





// other
// ---------------------------------------------------------------------
bool approximateFloatingPointEqual(const double a, const double b, const double epsilon);






}
}

#endif //BASIC_FUNCTION_H_
