#include "common_msgs/basic_function.hpp"
#include <assert.h>

// print
// ---------------------------------------------------------------------
void uavos::helper::printNavigationPose(const common_msgs::NavigationState& navigation_state, int level_of_detail){
    ros::Duration time_since_update = ros::Time::now() - navigation_state.header.stamp;

    double roll, pitch, yaw;
    quaternion2RPY(navigation_state.pose.orientation, roll, pitch, yaw);

    if(level_of_detail==0){
        std::printf("NWU Pose: [%8.2f]   x:%-8.2f y:%-8.2f z:%-8.2f c:%-8.2f\n",
                    time_since_update.toSec(),
                    navigation_state.pose.position.x,
                    navigation_state.pose.position.y,
                    navigation_state.pose.position.z,
                    yaw);
    } else if(level_of_detail==1){
        std::printf("NWU Pose: [%6.2f]   x:%-8.2f y:%-8.2f z:%-8.2f c:%-8.2f vx:%-8.2f vy:%-8.2f vz:%-8.2f vc:%-8.2f\n",
                    time_since_update.toSec(),
                    navigation_state.pose.position.x,
                    navigation_state.pose.position.y,
                    navigation_state.pose.position.z,
                    yaw,
                    navigation_state.velocity.linear.x,
                    navigation_state.velocity.linear.y,
                    navigation_state.velocity.linear.z,
                    navigation_state.velocity.angular.z);
    } else{
        std::printf("NWU Pose: [%6.2f]   x:%-8.2f y:%-8.2f z:%-8.2f c:%-8.2f vx:%-8.2f vy:%-8.2f vz:%-8.2f vc:%-8.2f ax:%-8.2f ay:%-8.2f az:%-8.2f ac:%-8.2f\n",
                    time_since_update.toSec(),
                    navigation_state.pose.position.x,
                    navigation_state.pose.position.y,
                    navigation_state.pose.position.z,
                    yaw,
                    navigation_state.velocity.linear.x,
                    navigation_state.velocity.linear.y,
                    navigation_state.velocity.linear.z,
                    navigation_state.velocity.angular.z,
                    navigation_state.acceleration.linear.x,
                    navigation_state.acceleration.linear.y,
                    navigation_state.acceleration.linear.z,
                    navigation_state.acceleration.angular.z);
    }

}


// type conversion
// ---------------------------------------------------------------------
void uavos::helper::quaternion2RPY(const geometry_msgs::Quaternion q, double &roll, double &pitch, double &yaw){
    tf::Quaternion q_tf(q.x, q.y, q.z, q.w);
    quaternion2RPY(q_tf, roll, pitch, yaw);
}

void uavos::helper::quaternion2RPY(const tf::Quaternion q, double &roll, double &pitch, double &yaw){
    tf::Matrix3x3 m(q);
    m.getRPY(roll, pitch, yaw);

    yaw = INPI<double>(yaw);
    pitch = INPI<double>(pitch);
    roll = INPI<double>(roll);
}
void uavos::helper::navigation2RPY(const common_msgs::NavigationState state, double &roll, double &pitch, double &yaw){
    quaternion2RPY(state.pose.orientation, roll, pitch, yaw);
}

common_msgs::NavigationState uavos::helper::vector2Navigation(double x, double y, double z, double c,
                                                      double vx, double vy, double vz, double vc,
                                                      double ax, double ay, double az, double ac){
    common_msgs::NavigationState navigation_state;

    navigation_state.pose.position.x = x;
    navigation_state.pose.position.y = y;
    navigation_state.pose.position.z = z;
    navigation_state.pose.orientation = tf::createQuaternionMsgFromYaw(INPI<double>(c));

    navigation_state.velocity.linear.x = vx;
    navigation_state.velocity.linear.y = vy;
    navigation_state.velocity.linear.z = vz;
    navigation_state.velocity.angular.x = 0;
    navigation_state.velocity.angular.y = 0;
    navigation_state.velocity.angular.z = vc;

    navigation_state.acceleration.linear.x = ax;
    navigation_state.acceleration.linear.y = ay;
    navigation_state.acceleration.linear.z = az;
    navigation_state.acceleration.angular.x = 0;
    navigation_state.acceleration.angular.y = 0;
    navigation_state.acceleration.angular.z = ac;

    return navigation_state;
}

common_msgs::NavigationState uavos::helper::navigationNWU2NED(common_msgs::NavigationState state_NWU){
    common_msgs::NavigationState state_NED;

    state_NED.pose.position.x = state_NWU.pose.position.x;
    state_NED.pose.position.y = state_NWU.pose.position.y * -1;
    state_NED.pose.position.z = state_NWU.pose.position.z * -1;
    state_NED.pose.orientation.x = state_NWU.pose.orientation.x;
    state_NED.pose.orientation.y = state_NWU.pose.orientation.y * -1;
    state_NED.pose.orientation.z = state_NWU.pose.orientation.z * -1;
    state_NED.pose.orientation.w = state_NWU.pose.orientation.w;

    state_NED.velocity.linear.x = state_NWU.velocity.linear.x;
    state_NED.velocity.linear.y = state_NWU.velocity.linear.y * -1;
    state_NED.velocity.linear.z = state_NWU.velocity.linear.z * -1;
    state_NED.velocity.angular.x = state_NWU.velocity.angular.x;
    state_NED.velocity.angular.y = state_NWU.velocity.angular.y * -1;
    state_NED.velocity.angular.z = state_NWU.velocity.angular.z * -1;

    state_NED.acceleration.linear.x = state_NWU.acceleration.linear.x;
    state_NED.acceleration.linear.y = state_NWU.acceleration.linear.y * -1;
    state_NED.acceleration.linear.z = state_NWU.acceleration.linear.z * -1;
    state_NED.acceleration.angular.x = state_NWU.acceleration.angular.x;
    state_NED.acceleration.angular.y = state_NWU.acceleration.angular.y * -1;
    state_NED.acceleration.angular.z = state_NWU.acceleration.angular.z * -1;

    return state_NED;
}

common_msgs::NavigationState uavos::helper::navigationNED2NWU(common_msgs::NavigationState state_NED){
    return navigationNWU2NED(state_NED);
}

common_msgs::Reference uavos::helper::referenceNWU2NED(common_msgs::Reference ref_NWU){
    common_msgs::Reference ref_NED = ref_NWU;
    ref_NED.navigation = navigationNWU2NED(ref_NWU.navigation);

    return ref_NED;
}

common_msgs::Reference uavos::helper::referenceNED2NWU(common_msgs::Reference ref_NED){
    return referenceNWU2NED(ref_NED);
}


// variable assignment, check
// ---------------------------------------------------------------------
void uavos::helper::setZeroPixhawkCMD(common_msgs::PixhawkCMD& cmd){
    cmd.header.stamp = ros::Time::now();
    for(int i=0;i<3;++i){
        cmd.cmd.at(i)=0;
    }
}
void uavos::helper::setZeroPixhawkServo(common_msgs::PixhawkServo& servo){
    servo.header.stamp = ros::Time::now();
    for(int i=0;i<4;++i){
        servo.servo.at(i)=0;
    }
}

common_msgs::NavigationState uavos::helper::getZeroNavigationState(){
    common_msgs::NavigationState state;
    state.header.stamp = ros::Time::now();

    state.pose.position.x = 0;
    state.pose.position.y = 0;
    state.pose.position.z = 0;
    state.pose.orientation.x = 0;
    state.pose.orientation.y = 0;
    state.pose.orientation.z = 0;
    state.pose.orientation.w = 1;

    state.velocity.linear.x=0;
    state.velocity.linear.y=0;
    state.velocity.linear.z=0;
    state.velocity.angular.x=0;
    state.velocity.angular.y=0;
    state.velocity.angular.z=0;

    state.acceleration.linear.x=0;
    state.acceleration.linear.y=0;
    state.acceleration.linear.z=0;
    state.acceleration.angular.x=0;
    state.acceleration.angular.y=0;
    state.acceleration.angular.z=0;

    return state;
}

void uavos::helper::setZeroNavigationState(common_msgs::NavigationState& state){
    state.header.stamp = ros::Time::now();

    state.pose.position.x = 0;
    state.pose.position.y = 0;
    state.pose.position.z = 0;
    state.pose.orientation.x = 0;
    state.pose.orientation.y = 0;
    state.pose.orientation.z = 0;
    state.pose.orientation.w = 1;

    state.velocity.linear.x=0;
    state.velocity.linear.y=0;
    state.velocity.linear.z=0;
    state.velocity.angular.x=0;
    state.velocity.angular.y=0;
    state.velocity.angular.z=0;

    state.acceleration.linear.x=0;
    state.acceleration.linear.y=0;
    state.acceleration.linear.z=0;
    state.acceleration.angular.x=0;
    state.acceleration.angular.y=0;
    state.acceleration.angular.z=0;
}

void uavos::helper::setZeroVelocityAccleration4NavigationState(common_msgs::NavigationState& state){
    state.header.stamp = ros::Time::now();

    state.velocity.linear.x=0;
    state.velocity.linear.y=0;
    state.velocity.linear.z=0;
    state.velocity.angular.x=0;
    state.velocity.angular.y=0;
    state.velocity.angular.z=0;

    state.acceleration.linear.x=0;
    state.acceleration.linear.y=0;
    state.acceleration.linear.z=0;
    state.acceleration.angular.x=0;
    state.acceleration.angular.y=0;
    state.acceleration.angular.z=0;
}

void uavos::helper::setZeroAccleration4NavigationState(common_msgs::NavigationState& state){
    state.header.stamp = ros::Time::now();

    state.acceleration.linear.x=0;
    state.acceleration.linear.y=0;
    state.acceleration.linear.z=0;
    state.acceleration.angular.x=0;
    state.acceleration.angular.y=0;
    state.acceleration.angular.z=0;
}

common_msgs::Reference uavos::helper::getZeroReference(){
    common_msgs::Reference ref;
    ref.header.stamp = ros::Time::now();
    setZeroNavigationState(ref.navigation);
    setZeroPixhawkCMD(ref.pixhawkCMD);
    setZeroPixhawkServo(ref.pixhawkServo);

    // should be removed later.
    ref.pixhawkCMD.cmd.at(0)= 88;

    return ref;
}

void uavos::helper::setZeroReference(common_msgs::Reference& ref){
    ref.header.stamp = ros::Time::now();
    setZeroNavigationState(ref.navigation);
    setZeroPixhawkCMD(ref.pixhawkCMD);
    setZeroPixhawkServo(ref.pixhawkServo);

    // should be removed later.
    ref.pixhawkCMD.cmd.at(0)= 88;
}

common_msgs::MaxDynamics uavos::helper::loadMaxDynamics(ros::NodeHandle& nh, std::string choice)
{
    std::map<std::string, double> dmap;
    nh.getParam(choice, dmap);

    common_msgs::MaxDynamics max_dynamics;
    max_dynamics.vel_max.linear.x   = readMaxDynamicsMap(dmap, std::string("Vh"), 0.35);
    max_dynamics.vel_max.linear.y   = readMaxDynamicsMap(dmap, std::string("Vh"), 0.35);
    max_dynamics.vel_max.linear.z   = readMaxDynamicsMap(dmap, std::string("Vv"), 0.35);
    max_dynamics.vel_max.angular.x  = readMaxDynamicsMap(dmap, std::string("Vc"), 0.35);
    max_dynamics.vel_max.angular.y  = readMaxDynamicsMap(dmap, std::string("Vc"), 0.35);
    max_dynamics.vel_max.angular.z  = readMaxDynamicsMap(dmap, std::string("Vc"), 0.35);

    max_dynamics.acc_max.linear.x   = readMaxDynamicsMap(dmap, std::string("Ah"), 0.30);
    max_dynamics.acc_max.linear.y   = readMaxDynamicsMap(dmap, std::string("Ah"), 0.30);
    max_dynamics.acc_max.linear.z   = readMaxDynamicsMap(dmap, std::string("Av"), 0.30);
    max_dynamics.acc_max.angular.x  = readMaxDynamicsMap(dmap, std::string("Ac"), 0.30);
    max_dynamics.acc_max.angular.y  = readMaxDynamicsMap(dmap, std::string("Ac"), 0.30);
    max_dynamics.acc_max.angular.z  = readMaxDynamicsMap(dmap, std::string("Ac"), 0.30);

    max_dynamics.jerk_max.linear.x  = readMaxDynamicsMap(dmap, std::string("Jh"), 0.30);
    max_dynamics.jerk_max.linear.y  = readMaxDynamicsMap(dmap, std::string("Jh"), 0.30);
    max_dynamics.jerk_max.linear.z  = readMaxDynamicsMap(dmap, std::string("Jv"), 0.30);
    max_dynamics.jerk_max.angular.x = readMaxDynamicsMap(dmap, std::string("Jc"), 0.30);
    max_dynamics.jerk_max.angular.y = readMaxDynamicsMap(dmap, std::string("Jc"), 0.30);
    max_dynamics.jerk_max.angular.z = readMaxDynamicsMap(dmap, std::string("Jc"), 0.30);

    return max_dynamics;
}

double uavos::helper::readMaxDynamicsMap(const std::map <std::string, double> & m, const std::string & key, const double & defval ) {
    std::map<std::string,double>::const_iterator it = m.find( key );
    if ( it == m.end() ) {
        std::cout<<"\033[1;31m  "<<"Max dynamics loading failed. Go back default value."<<" \033[0m"<<std::endl;
        assert(0);
        return defval;
    }
    else {
        if(it->second > 1e-6) {
            return it->second;
        }
        else {
            std::cout<<"\033[1;31m  "<<"Max dynamics loading failed. Go back to default value."<<" \033[0m"<<std::endl;
            assert(0);
            return defval;
        }
    }
}

// geometry transform
// ---------------------------------------------------------------------
common_msgs::NavigationState uavos::helper::relative2absolute_planar(const common_msgs::NavigationState& relative, const common_msgs::NavigationState& curr_position){
    // transformation matrix (R | T)
    // cos(yaw)  -sin(yaw)  0  x_init
    // sin(yaw)  cos(yaw)   0  y_init
    //    0          0      1  z_init
    //    0          0      0     1

    double roll_init, pitch_init, yaw_init;
    navigation2RPY(curr_position, roll_init, pitch_init, yaw_init);

    double x_init = curr_position.pose.position.x;
    double y_init = curr_position.pose.position.y;
    double z_init = curr_position.pose.position.z;

    // data in the relative coordinate
    double roll, pitch, yaw;
    navigation2RPY(relative, roll, pitch, yaw);

    double x = relative.pose.position.x;
    double y = relative.pose.position.y;
    double z = relative.pose.position.z;

    double vx = relative.velocity.linear.x;
    double vy = relative.velocity.linear.y;
    double vz = relative.velocity.linear.z;

    double ax = relative.acceleration.linear.x;
    double ay = relative.acceleration.linear.y;
    double az = relative.acceleration.linear.z;


    // transform to the absolute coordinate
    common_msgs::NavigationState absolute = relative;
    // position
    absolute.pose.position.x = x*cos(yaw_init) + y*-sin(yaw_init) + x_init;
    absolute.pose.position.y = x*sin(yaw_init) + y*cos(yaw_init) + y_init;
    absolute.pose.position.z = z + z_init;
    absolute.pose.orientation = tf::createQuaternionMsgFromYaw(yaw + yaw_init);

    // velocity
    absolute.velocity.linear.x = vx*cos(yaw_init) + vy*-sin(yaw_init);
    absolute.velocity.linear.y = vx*sin(yaw_init) + vy*cos(yaw_init);
    absolute.velocity.linear.z = vz;

    // acceleration
    absolute.acceleration.linear.x = ax*cos(yaw_init) + ay*-sin(yaw_init);
    absolute.acceleration.linear.y = ax*sin(yaw_init) + ay*cos(yaw_init);
    absolute.acceleration.linear.z = az;

    return absolute;
}

geometry_msgs::Pose uavos::helper::relative2absolute_planar(const geometry_msgs::Pose& relative, const geometry_msgs::Pose& curr_position){
    // transformation matrix (R | T)
    // cos(yaw)  -sin(yaw)  0  x_init
    // sin(yaw)  cos(yaw)   0  y_init
    //    0          0      1  z_init
    //    0          0      0     1

    double roll_init, pitch_init, yaw_init;
    uavos::helper::quaternion2RPY(curr_position.orientation, roll_init, pitch_init, yaw_init);

    double x_init = curr_position.position.x;
    double y_init = curr_position.position.y;
    double z_init = curr_position.position.z;

    // data in the relative coordinate
    double roll, pitch, yaw;
    quaternion2RPY(relative.orientation, roll, pitch, yaw);

    double x = relative.position.x;
    double y = relative.position.y;
    double z = relative.position.z;


    // transform to the absolute coordinate
    geometry_msgs::Pose absolute = relative;
    // position
    absolute.position.x = x*cos(yaw_init) + y*-sin(yaw_init) + x_init;
    absolute.position.y = x*sin(yaw_init) + y*cos(yaw_init) + y_init;
    absolute.position.z = z + z_init;
    absolute.orientation = tf::createQuaternionMsgFromYaw(yaw + yaw_init);


    return absolute;
}

geometry_msgs::Pose uavos::helper::absolute2relative_planar(const geometry_msgs::Pose relative, const geometry_msgs::Pose curr_position){
    // curr_position - relaive

    // transformation matrix
    // R.inv * (x - T)

    // R.inv
    // cos(yaw)   sin(yaw)   0
    // -sin(yaw)  cos(yaw)   0
    //    0          0       1

    double roll_init, pitch_init, yaw_init;
    quaternion2RPY(relative.orientation, roll_init, pitch_init, yaw_init);
    double x_init = relative.position.x;
    double y_init = relative.position.y;
    double z_init = relative.position.z;

    // data in the origin coordinate
    double x = curr_position.position.x;
    double y = curr_position.position.y;
    double z = curr_position.position.z;

    double roll, pitch, yaw;
    uavos::helper::quaternion2RPY(curr_position.orientation, roll, pitch, yaw);

    // transform to the offset coordinat
    geometry_msgs::Pose transformed_to_offset = curr_position;
    // position
    transformed_to_offset.position.x = (x-x_init)*cos(yaw_init) + (y-y_init)*sin(yaw_init);
    transformed_to_offset.position.y = (x-x_init)*-sin(yaw_init) + (y-y_init)*cos(yaw_init);
    transformed_to_offset.position.z = (z-z_init);
    transformed_to_offset.orientation = tf::createQuaternionMsgFromYaw(yaw - yaw_init);

    return transformed_to_offset;
}


// image processing
// ---------------------------------------------------------------------
bool uavos::helper::is_inImageBound(const int& x, const int& y, const int& width, const int& height){
    if(x<0||x>width-1 || y<0||y>height-1) return false;
    else return true;
}
bool uavos::helper::is_inImageBound_strict(const float& x, const float& y, const int& width, const int& height){
    if(x<=1||x>=width-2 || y<=1||y>=height-2) return false;
    else return true;
}




// type arithmetic
// ---------------------------------------------------------------------



// IO
// ---------------------------------------------------------------------
std::vector<double> uavos::helper::lineStr2Double(std::string line, std::string separator){
    std::vector<std::string> tokens;
    boost::split(tokens, line, boost::is_any_of(separator.c_str()));

    std::vector<double> dst;
    for(int i=0;i<tokens.size();++i){
        try{
            dst.push_back( boost::lexical_cast<double>(tokens.at(i)) );
        } catch (boost::bad_lexical_cast const&){
        }
    }

    return dst;
}

int uavos::helper::readFromTxt(std::vector<std::vector<double> > &wayPoints, std::string file_name, int columns){

    std::ifstream txtFILE(file_name.c_str());


    if(!txtFILE){
        printf("Open file failed.\n");
        return 0;
    }

    std::string line;
    while (std::getline(txtFILE, line)){

        std::vector<double> waypoint_single = uavos::helper::lineStr2Double(line, std::string(" "));
        if(waypoint_single.size()==columns || waypoint_single.size()==columns+1)
            wayPoints.push_back(waypoint_single);
        else{
            ROS_INFO("Waypoint input Error.");
            continue;
        }

    }

    return wayPoints.size();
}



// other
// ---------------------------------------------------------------------
bool uavos::helper::approximateFloatingPointEqual(const double a, const double b, const double epsilon){
    if(std::isnan(a) || std::isnan(b)){
        return false;
    } else {
        return std::fabs(a-b)<epsilon;
    }
}

