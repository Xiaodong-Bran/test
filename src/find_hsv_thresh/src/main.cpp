#include <iostream>
#include <stdio.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

Mat illuminationCorrectionColorCLAHE (const Mat &img)
{
    Mat lab_image;
    cvtColor(img, lab_image, CV_BGR2Lab);
    // Extract the L channel
    std::vector<cv::Mat> lab_planes(3);
    cv::split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]
    // apply the CLAHE algorithm to the L channel
    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
    clahe->setClipLimit(4);
    cv::Mat dst;
    clahe->apply(lab_planes[0], dst);
    // Merge the the color planes back into an Lab image
    dst.copyTo(lab_planes[0]);
    cv::merge(lab_planes, lab_image);
    // convert back to RGB
    cv::Mat image_clahe;
    cv::cvtColor(lab_image, image_clahe, CV_Lab2BGR);

    return image_clahe;
}

/** @function main */
int main( int argc, char** argv )
{
	if( argc != 2 )
	{ return -1; }
	
	Mat uav_img_bgr = imread( argv[1], CV_LOAD_IMAGE_COLOR );

	int iLowH = 0;
	int iHighH = 179;
	
	int iLowS = 150; 
	int iHighS = 255;
	
	int iLowV = 60;
	int iHighV = 255;
	
	namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"
	//Create trackbars in "Control" window
	createTrackbar("LowH", "Control", &iLowH, 179); //Hue (0 - 179)
	createTrackbar("HighH", "Control", &iHighH, 179);
	
	createTrackbar("LowS", "Control", &iLowS, 255); //Saturation (0 - 255)
	createTrackbar("HighS", "Control", &iHighS, 255);
	
	createTrackbar("LowV", "Control", &iLowV, 255);//Value (0 - 255)
	createTrackbar("HighV", "Control", &iHighV, 255);
	
	Mat imgHSV;

	while (true)
	{
	//uav_img_bgr = illuminationCorrectionColorCLAHE(uav_img_bgr);
	cvtColor(uav_img_bgr, imgHSV, COLOR_BGR2HSV); //Convert the captured frame from BGR to HSV
	Mat imgThresholded;
	inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded); //Threshold the image
	
	imshow("Control", imgThresholded); //show the thresholded image
    if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
    {
    	cout << "esc key is pressed by user" << endl;
    	break; 
    }
	}
	
	return 0;
}

