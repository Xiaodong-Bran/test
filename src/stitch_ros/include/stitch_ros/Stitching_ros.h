#include <iostream>	// for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion
#include <vector>
#include <cmath>
#include <stdio.h>
#include <fstream>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>


#include <opencv2/opencv.hpp>

#if CV_MAJOR_VERSION == 2
    #include "opencv2/stitching/stitcher.hpp"
#elif CV_MAJOR_VERSION == 3

#endif

#include <ros/ros.h>

#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
//#include <serial_uav/MSG_UAV_ROS.h>


#include <std_msgs/Int8.h>

//include STL containers
using namespace cv;
using namespace std;

class stitch_img_ros
{
public:
    stitch_img_ros();
#if 0
    void uavMsgCallback(const serial_uav::MSG_UAV_ROS::ConstPtr& msg);
#endif

#if 1
    void uavMsgCallback(const std_msgs::Int8::ConstPtr& msg);
    void DirectStitch(void);
#endif
    bool start, finish; 

private:    
    ros::NodeHandle nh;	
	//receive uav command
    ros::Subscriber uav_command;

    char m_filenameimage[256];
    char nameImage[256];
    FILE *fResults;
};
