#ifndef STITCHING_H_
#define STITCHING_H_

#include <stdio.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>
#include <iomanip>
#include <dirent.h>
#include <math.h>

#include <opencv2/opencv.hpp>
#if CV_MAJOR_VERSION == 2
    #include "opencv2/stitching/stitcher.hpp"
#elif CV_MAJOR_VERSION == 3
#endif

using namespace cv;
using namespace std;

#define show_time 0  // Do not show time

class Stitching{
public:
	Mat _M1; //Camera Intrinsic Matrix
	Mat _D1; //Camera Distortion Matrix

	Mat _H_previous;//Previous calculated Homography
	Mat _stitchedFinal; //Final stitched large map
    	Mat _previousIMG_undistorted;

 	Mat currentIMG, Result, H_IMG, ic_IMG;
	Mat vdst;	

	Stitching();

	void stitch_img();
	bool CalcHomographyKLT(Mat scene_IMG, Mat obj_IMG);
	Mat DrawFlows(Mat img, vector<Point2f> featureCurrent, vector<Point2f> featurePrevious, int i);
	void pixelTest(vector<Point2f> vector1, vector<Point2f> vector2,  vector<uchar>& vectorfound, float ratio);
	Mat illuminationCorrectionColorCLAHE (const Mat &img);
	int countImgNum ();
	Mat canvas2map(const Mat &Result);
	Point2f averagePoint(std::vector< Point2f > points);
	int checkAngle(Point2f A, Point2f B);

	double ratio;
	std::vector< Point2f > scene;
	std::vector< Point2f > obj;	

	Point2f _centroid_previous;
	Point2f _centroid_current;
	Point2f translation;
	Point2f position;

	int frequency;
	int resultW, resultH;
	int total_img_num;
	int start_id, end_id;
/*//////////////////////////////////////////////////////////////////////////////////////*/
	int round_count;
	int round_interval;
	int img_save_count;
	int img_save_interval;

	vector<Mat> imgs;
	Mat cur_pano;
	double overlapping_ratio;

	bool needToInit;
	Mat dst;
/*//////////////////////////////////////////////////////////////////////////////////////*/
	Mat current_mask;
	Mat canvas_mask;
	Mat canvas_img;	
	Mat temp_result;	
	Mat_<Vec3f> blend_result;	
/*//////////////////////////////////////////////////////////////////////////////////////*/
	FILE *fResults;	
	char img_folder[256];
	char img_name[256];
    	char save_name[128];	
};

#endif /* STITCHING_H_ */
