#include "opencv/cv.hpp"
#include <time.h>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <sstream>
#include <opencv2/features2d/features2d.hpp>

//include STL containers
using namespace cv;
using namespace std;

double cch=260.08411 , ccw=346.43155, fch=638.05564  , fcw=637.87263;
Mat intrinsic = ( Mat_<double>(3, 3) << fcw, 0, ccw, 0, fch, cch, 0, 0, 1 );

double distortionCoeffs_array[5]= {-0.26396,   0.12099,   -0.00953,   -0.00835,  0.00000};
double K_forDistort_array[9]= {fcw, 0, ccw, 0, fch, cch, 0, 0, 1};

Mat local_KforDistort(3,3,CV_64FC1, K_forDistort_array);
Mat distCoeffs(1, 5, CV_64FC1, distortionCoeffs_array); // distortion coefficients