#include <stitch_ros/Stitching_ros.h>
#include <stitch_ros/Stitching.h>

#if 0
stitch_img_ros::stitch_img_ros()
{
    uav_command = nh.subscribe("imav2visual", 10, &stitch_img_ros::uavMsgCallback, this); //
    sprintf(m_filenameimage, "/home/bran/Downloads/Image Stitching/images/glenelg");
    finish = false; 
    start = false;	      
}


void stitch_img_ros::uavMsgCallback(const serial_uav::MSG_UAV_ROS::ConstPtr& msg)
{
	if (!start)
	{

        fResults = fopen("/home/imav/catkin_imav2015/stitch_time.txt", "w"); ///**///
		fprintf(fResults, "stitching starts\n");	
		fclose(fResults); 

		start = true;
		Stitching s;
		sprintf(s.img_folder, "%s", m_filenameimage);
		s.stitch_img();
		finish = true;
	}
	else
	{

	}	
}
#endif

#if 1
stitch_img_ros::stitch_img_ros()
{
    uav_command = nh.subscribe("easy_one", 10, &stitch_img_ros::uavMsgCallback,this); //
    sprintf(m_filenameimage, "/home/bran/Downloads/ImageStitching/test_images/test_100/");
    //sprintf(m_filenameimage, "/home/bran/Downloads/ImageStitching/test_images_400/");
    //sprintf(m_filenameimage, "/home/bran/Downloads/ImageStitching/test_image_5000/rgb/");

    finish = false;
    start = false;
}
void stitch_img_ros::uavMsgCallback(const std_msgs::Int8::ConstPtr& msg )
{
    if (!start)
    {
        ROS_INFO("In the callback function \n");
        //fResults = fopen("/home/imav/catkin_imav2015/stitch_time.txt", "w"); ///**///
        //fprintf(fResults, "stitching starts\n");
        //fclose(fResults);
        start = true;
        Stitching s;
        sprintf(s.img_folder, "%s", m_filenameimage);
        ROS_INFO("Start stitching: image folder %s \n",s.img_folder);
        s.stitch_img();
        finish = true;
    }
    else
    {

    }
}
void stitch_img_ros::DirectStitch()
{
    ROS_INFO("In the DirectStitch function \n");
    Stitching s;
    sprintf(s.img_folder, "%s", m_filenameimage);
    ROS_INFO("Start stitching: image folder %s \n",s.img_folder);
    s.stitch_img();
}
#endif
