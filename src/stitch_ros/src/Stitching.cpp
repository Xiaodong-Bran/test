#include <stitch_ros/Stitching.h>
#include <stitch_ros/camera.h>

using namespace cv;
using namespace std;

Stitching::Stitching()
{
    frequency = 1; //3

    resultW = 3*1280;
    resultH = 6*960;    
    Result.create(resultH, resultW, CV_8UC1); 
    _H_previous = (Mat_<double>(3,3) << 1, 0, resultW/2,
                                          0, 1, resultH/2,
                                          0, 0, 1);
    position.x = resultW/2;
    position.y = resultH/2; 	
}

void Stitching::stitch_img()
{
	round_count = 1;
	//round_interval = 150;	

	img_save_count = 1;
	img_save_interval = 150;

    start_id = 1;
    total_img_num = countImgNum();

    cout << "total num is :" << total_img_num << endl;

    end_id = total_img_num;
    round_interval = total_img_num / 2; ///

#if show_time
  double time, t1, t2;
t1 = getTickCount();	 
#endif
	for (int id = start_id; id < end_id; id+=frequency)			
	{
#if 0
		fResults = fopen("/home/imav/catkin_imav2015/stitch_time.txt", "a"); 	
		fprintf(fResults, "%d image\n", id);	
        fclose(fResults);

        // sprintf(img_name, "%simg%04d.jpg", img_folder, id);
#endif
        sprintf(img_name, "%s%d.jpg", img_folder, id);
        //cout<< "image names:" << img_name <<endl;

        Mat img = imread(img_name, CV_LOAD_IMAGE_COLOR);

		if(! img.data ) // Check for invalid input
		{
		cout <<  "Could not open or find the image" << std::endl ;
		continue;
        }

/*//////////////////////////////////////////////////////////////////////////////////////*/ 
		//pre processing
		resize(img, currentIMG,Size(320, 240), 0, 0, INTER_LINEAR);		 		
		// ic_IMG = s.illuminationCorrectionColorCLAHE(currentIMG);		
/*//////////////////////////////////////////////////////////////////////////////////////*/	
		if(needToInit)
		{
			currentIMG.copyTo(Result);
			currentIMG.copyTo(_previousIMG_undistorted);
		 	warpPerspective(currentIMG, Result, _H_previous, cv::Size(resultW, resultH), INTER_LINEAR, BORDER_TRANSPARENT); 						
			needToInit = false;

            cout<<"initialized..."<< endl;
				
			continue;				
		}
/*//////////////////////////////////////////////////////////////////////////////////////*/
		if (CalcHomographyKLT(_previousIMG_undistorted,currentIMG))
		{
			position.x += translation.x;
			position.y += translation.y;				
			_H_previous = (Mat_<double>(3,3) << 1, 0, position.x,
												  0, 1, position.y,
												  0, 0, 1); //intrinsic matrix assumption
			//s._H_previous *= s.Homography; 
			 warpPerspective(currentIMG, Result, _H_previous, cv::Size(resultW, resultH), INTER_LINEAR, BORDER_TRANSPARENT); 			
		}	 		 	
/*//////////////////////////////////////////////////////////////////////////////////////*/	
		currentIMG.copyTo(_previousIMG_undistorted);
		resize(Result, vdst, Size(), 0.15, 0.15, INTER_LINEAR);			

		if (id > img_save_count * img_save_interval)
		{
			dst = canvas2map(Result);
            //sprintf(save_name,"/home/imav/catkin_imav2015/results/stitch/map_save_%d.jpg", img_save_count);
            sprintf(save_name,"/home/bran/imav-vision/results/map_save_%d.jpg", img_save_count);
   			imwrite(save_name, dst); 
   			img_save_count ++;   							
		}

		if (id > round_count * round_interval)
		{			
			dst = canvas2map(Result);  

            //sprintf(save_name,"/home/imav/catkin_imav2015/results/stitch/map_stitched_%d.jpg", round_count);
            sprintf(save_name,"/home/bran/imav-vision/results/map_stitched_%d.jpg", round_count);
            imwrite(save_name, dst);

            cout<< "half of the pics is done " << endl;

   			needToInit = true;
   			imgs.push_back(dst);
   			round_count ++;	 		
		}  		

     }
    // end of the loop
			
		dst = canvas2map(Result);
        //sprintf(save_name,"/home/imav/catkin_imav2015/results/stitch/map_stitched_%d.jpg", round_count);
        sprintf(save_name,"/home/bran/imav-vision/results/map_stitched_%d.jpg", round_count);
        imwrite(save_name, dst);

        cout<< "the rest of pics are stitched" << endl;

		imgs.push_back(dst);	
/*//////////////////////////////////////////////////////////////////////////////////////*/	
	Stitcher stitcher = Stitcher::createDefault(false);

	overlapping_ratio = 1;
	vector<vector<Rect> > rois;
	vector<Rect> roi1, roi2;

	Rect newSize1(0, 0, imgs[0].cols * overlapping_ratio, imgs[0].rows);
	roi1.push_back(newSize1);
	rois.push_back(roi1);
	roi1.clear();


	Rect newSize2(imgs[1].cols * (1 - overlapping_ratio), 0, imgs[1].cols, imgs[1].rows);
	roi2.push_back(newSize2);
	rois.push_back(roi2);
	roi2.clear();

    cout<<"starting to stitch the two pars altogher "<<endl;
#if 1
    Stitcher::Status status = stitcher.stitch(imgs, rois, cur_pano);
#endif

#if 0
    Stitcher::Status status = stitcher.stitch(imgs,cur_pano);
#endif

    cout<<"stitching DONE " << endl;

	if(Stitcher::OK == status && cur_pano.size().width > 1)
	{
        //imwrite("/home/imav/catkin_imav2015/pano.jpg", cur_pano);
        imwrite("/home/bran/imav-vision/results/pano.jpg", cur_pano);

        cout<<"the result is /imav-vision/results/pano.jpg"<<endl;
       // display
        namedWindow("Display Image", WINDOW_AUTOSIZE);
        imshow("Display Image",cur_pano);
        waitKey(0);
    }

    else
    {
        cout<<"Stitch failed \n" << endl;
    }

#if show_time
t2 = getTickCount();
time = (t2 - t1) / getTickFrequency();
	fResults = fopen("/home/imav/catkin_imav2015/stitch_time.txt", "w"); 	
	fprintf(fResults, "%lf min\n", time / 60);	
	fclose(fResults); 
#endif		
}

Point2f Stitching::averagePoint(std::vector< Point2f > points)
{
  cv::Mat mean_;
  cv::reduce(points, mean_, CV_REDUCE_AVG, 1);
  // convert from Mat to Point - there may be even a simpler conversion, 
  // but I do not know about it.
  cv::Point2f mean(mean_.at<float>(0,0), mean_.at<float>(0,1));

  return mean;
}
int Stitching::countImgNum ()
{
  int count = 0;

    DIR *dir;
    dir = opendir(img_folder);
    string imgName;
    struct dirent *ent;
    if (dir != NULL) {
        while ((ent = readdir (dir)) != NULL) {
           imgName= ent->d_name;
           if(imgName.compare(".")!= 0 && imgName.compare("..")!= 0)
           {          
             count ++;
           }
        }
        closedir (dir);
    } else {
        cout<<"not present"<<endl;
    }

    return count;
}
bool Stitching::CalcHomographyKLT(Mat scene_IMG, Mat obj_IMG)
{
         Mat gray_image1;
         Mat gray_image2;
         // Convert to Grayscale
#if 0
         cvtColor( scene_IMG, gray_image1, CV_RGB2GRAY );
         cvtColor( obj_IMG, gray_image2, CV_RGB2GRAY );
#endif

#if 1
         if(scene_IMG.empty())
         {
            cout<< "scene image is empty \n";
         }
         else if(scene_IMG.channels()>1)
             cvtColor(scene_IMG, gray_image1, CV_BGR2GRAY);
         else gray_image1 = scene_IMG;

         if(obj_IMG.empty())
         {
            cout<< "object image is empty \n";
         }
         else if(obj_IMG.channels()>1)
             cvtColor(obj_IMG, gray_image2, CV_BGR2GRAY);
         else gray_image2 = obj_IMG;

#endif
         /// equalize the histgram of image
         equalizeHist(gray_image1,gray_image1);
         equalizeHist(gray_image2,gray_image2);

        if( !gray_image1.data || !gray_image2.data )
         { std::cout<< " --(!) Error reading images " << std::endl;}

        /// optical flow parameters ///////////////////////////////////////////////////////////////////
        TermCriteria criteria = TermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 10, 0.01);
        int maxcorner = 300;
        Size winsize = Size(15,15);
        int maxlevel = 5;
        double qualityLevel = 0.0001;
        int minDistance = 20;

        vector<Point2f> featuresFirst, featuresSecond;
        vector<uchar> featureFound;
        /// Function: find corners as good features in first image
        /// output: featureFirst
        goodFeaturesToTrack(gray_image1,featuresFirst,maxcorner, qualityLevel, minDistance,cv::Mat(),3); //First image feature detection
//      cornerSubPix(gray_image1,featuresFirst,Size(5,5),Size(0,0),criteria);
//      cout << "Features vector" << featuresFirst.size() << endl;

        Mat err;
        /// Function: find relative features in second image
        /// output : FeatureSecond : position of the features in the second images
        /// output: featureFound: status to indciate whether the position of the interesting points have changed. 1: changed 0: notchanged
        calcOpticalFlowPyrLK(gray_image1,gray_image2,featuresFirst,featuresSecond,featureFound,err, winsize, maxlevel,criteria,0,0.01);
        /// Filtering the moving feature
        /// first find out the average moving distance
        /// second filter out the OUTLIERS which distance is larger than the average moving distance
        pixelTest(featuresFirst,featuresSecond,featureFound,2);

	obj.clear();
	scene.clear();

	for(unsigned int i = 0; i < featureFound.size(); i++ )
	{
		if(featureFound[i]==1)
		{
		//-- Get the keypoints from the good matches
		obj.push_back(featuresSecond[i]);
		scene.push_back(featuresFirst[i]);
		}
	}

	if(obj.size() < 10)
	{
		return false;
	}

	Point2f scene_mean = averagePoint(featuresFirst);
	Point2f obj_mean = averagePoint(featuresSecond);
	translation = obj_mean - scene_mean;   
	translation *= -1;

  return true;
} 
/// Function: Pixel distance filtering
void Stitching::pixelTest(vector<Point2f> vector1, vector<Point2f> vector2,  vector<uchar>& vectorfound, float ratio)
{
    double max_dist = 0; double min_dist = 100;
    double avg_dist = 0;
    int n = 0;
    for(unsigned int i = 0; i < vector1.size(); i++ )
            {
                if(vectorfound[i])
                {
                double dist = norm(vector1[i]-vector2[i]);
                if( dist < min_dist ) min_dist = dist;
                if( dist > max_dist ) max_dist = dist;
                avg_dist = avg_dist + dist;
                n++;
                }
            }

    avg_dist = avg_dist / n;

                for(unsigned int i = 0; i < vector1.size(); i++ )
            {
                double dist = norm(vector1[i]-vector2[i]);
                if( dist > ratio*avg_dist )
                { vectorfound[i] = 0; }
            }
}
///Function: Display flows
Mat Stitching::DrawFlows(Mat img, vector<Point2f> featureCurrent, vector<Point2f> featurePrevious, int i)
{
    // i=1 -> red
    // i=2 -> blue
    Scalar S;
    if (i == 1)
    {
        S = Scalar(0,0,255);
    }

    if (i == 2)
    {
        S = Scalar(255,0,0);
    }

    Mat display;

    if(img.channels() == 1)
    {
        cvtColor(img, display, CV_GRAY2BGR);
    }
    else
    {
        display = img;
    }


    for(size_t j=0; j<featurePrevious.size(); j++)
            {

                    circle(display,featurePrevious[j],2,Scalar(0,255,0),2); // Flow start point plotted in green circle
                    circle(display,featureCurrent[j],2,Scalar(0,0,255),2); //Flow end point plotted in RED circle
                    line(display,featurePrevious[j],featureCurrent[j],S,2); //Flow line plotted in RED

            }
    return display;
}
int Stitching::checkAngle(Point2f A, Point2f B) {
  int val;
  if(B.x==A.x)
    val = 90;
  else if (B.y==A.y) 
    val = 0;
  else
  {
   val = (B.y-A.y)/(B.x-A.x); // calculate slope between the two points
   double slope = atan(val);
   val = ((int)(slope*180/3.14));
   val = abs(val);    
  }
  // cout << "angle " << abs(val-90) << endl;
  return abs(val-90);
}
/*//////////////////////////////////////////////////////////////////////////////////////*/
Mat Stitching::canvas2map(const Mat &Result)
{
  Mat thres_result,Resultgray;
  cvtColor( Result, Resultgray, CV_BGR2GRAY );
  threshold(Resultgray,thres_result,0,255,THRESH_BINARY);
  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;  
  findContours( thres_result, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
  vector<vector<Point> > contours_poly( contours.size() );
  vector<Rect> boundRect( contours.size() );

  double largest_area = 0;
  int largest_contour_index = 0;
  Rect bounding_rect;

  for( int i = 0; i< contours.size(); i++ ) // iterate through each contour.
  {
    double a=contourArea( contours[i],false);  //  Find the area of contour
    if(a>largest_area){
    largest_area=a;
    largest_contour_index=i;                //Store the index of largest contour
    bounding_rect=boundingRect(contours[i]); // Find the bounding rectangle for biggest contour
    }
  }   
    Mat dst = Result(bounding_rect); 

    return dst;
}
Mat Stitching::illuminationCorrectionColorCLAHE (const Mat &img)
{
    Mat lab_image;
    cvtColor(img, lab_image, CV_BGR2Lab);
    // Extract the L channel
    std::vector<cv::Mat> lab_planes(3);
    cv::split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]
    // apply the CLAHE algorithm to the L channel
    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
    clahe->setClipLimit(4);
    cv::Mat dst;
    clahe->apply(lab_planes[0], dst);
    // Merge the the color planes back into an Lab image
    dst.copyTo(lab_planes[0]);
    cv::merge(lab_planes, lab_image);
    // convert back to RGB
    cv::Mat image_clahe;
    cv::cvtColor(lab_image, image_clahe, CV_Lab2BGR);

    return image_clahe;
}
