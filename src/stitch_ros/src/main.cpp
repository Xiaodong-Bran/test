
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include<opencv2/opencv.hpp>
#include "opencv/cv.hpp"
#if CV_MAJOR_VERSION == 2
#include "opencv2/stitching/stitcher.hpp"
#elif CV_MAJOR_VERSION == 3
#endif

#include <time.h>
#include <iostream>
#include <ros/ros.h>
#include <sstream>
#include <stitch_ros/Stitching_ros.h>
//include STL containers
using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
    ROS_INFO("Starting Stiching Node\n");

    ros::init(argc, argv, "stitch_ros");
    ros::Time::init();
    ros::Rate rosRate(15); // frequency

    stitch_img_ros s; 
    ROS_INFO("Starting Stitching Function...\n");
    s.DirectStitch();

    ros::spin();	

    return 1;
}		
