# CMake generated Testfile for 
# Source directory: /home/bran/Documents/gittest/src
# Build directory: /home/bran/Documents/gittest/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(apriltag_mit)
subdirs(find_hsv_thresh)
subdirs(qr_reader)
subdirs(stitch_ros)
subdirs(common_msgs)
subdirs(apriltag_ros)
subdirs(color_detector)
subdirs(detection_controller)
