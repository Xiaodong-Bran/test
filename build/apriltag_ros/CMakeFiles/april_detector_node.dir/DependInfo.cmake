# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/bran/Documents/gittest/src/apriltag_ros/src/april_detector/april_detector_main.cpp" "/home/bran/Documents/gittest/build/apriltag_ros/CMakeFiles/april_detector_node.dir/src/april_detector/april_detector_main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"apriltag_ros\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/bran/Documents/gittest/devel/include"
  "/home/bran/Documents/gittest/src/apriltag_ros/include"
  "/home/bran/Documents/gittest/src/apriltag_ros/include/apritag_ros"
  "/home/bran/Documents/gittest/src/apriltag_mit/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/opt/ros/kinetic/include/opencv-3.2.0-dev"
  "/opt/ros/kinetic/include/opencv-3.2.0-dev/opencv"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/bran/Documents/gittest/build/apriltag_ros/CMakeFiles/apriltag_ros.dir/DependInfo.cmake"
  "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
