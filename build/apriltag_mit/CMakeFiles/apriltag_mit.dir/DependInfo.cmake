# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/bran/Documents/gittest/src/apriltag_mit/src/Edge.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/Edge.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/FloatImage.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/FloatImage.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/GLine2D.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/GLine2D.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/GLineSegment2D.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/GLineSegment2D.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/Gaussian.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/Gaussian.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/GrayModel.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/GrayModel.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/Homography33.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/Homography33.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/MathUtil.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/MathUtil.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/Quad.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/Quad.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/Segment.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/Segment.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/TagDetection.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/TagDetection.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/TagDetector.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/TagDetector.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/TagFamily.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/TagFamily.cc.o"
  "/home/bran/Documents/gittest/src/apriltag_mit/src/UnionFindSimple.cc" "/home/bran/Documents/gittest/build/apriltag_mit/CMakeFiles/apriltag_mit.dir/src/UnionFindSimple.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/bran/Documents/gittest/src/apriltag_mit/include"
  "/home/bran/Documents/gittest/src/apriltag_mit/include/AprilTags"
  "/usr/include/eigen3"
  "/opt/ros/kinetic/include/opencv-3.2.0-dev"
  "/opt/ros/kinetic/include/opencv-3.2.0-dev/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
