# Install script for directory: /home/bran/Documents/gittest/src/common_msgs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/bran/Documents/gittest/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common_msgs/msg" TYPE FILE FILES
    "/home/bran/Documents/gittest/src/common_msgs/msg/NavigationState.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/GPSVelocity.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/MeasurementPosition.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/MeasurementVelocity.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/PixhawkCMD.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/PixhawkServo.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/MeasurementPosVel.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/MaxDynamics.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/Reference.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/Mission.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/MissionStatus.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/PathPlanningStatus.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/Waypoint.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/UWB_DataInfo.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/UWB_EchoedRangeInfo.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/UWB_FullNeighborDatabase.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/UWB_FullNeighborDatabaseEntry.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/UWB_FullRangeInfo.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/UWB_SendData.msg"
    "/home/bran/Documents/gittest/src/common_msgs/msg/MotionDescriber.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common_msgs/srv" TYPE FILE FILES "/home/bran/Documents/gittest/src/common_msgs/srv/DetectionController.srv")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common_msgs/cmake" TYPE FILE FILES "/home/bran/Documents/gittest/build/common_msgs/catkin_generated/installspace/common_msgs-msg-paths.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/bran/Documents/gittest/devel/include/common_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/bran/Documents/gittest/devel/share/roseus/ros/common_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/bran/Documents/gittest/devel/share/common-lisp/ros/common_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/bran/Documents/gittest/devel/share/gennodejs/ros/common_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND "/usr/bin/python" -m compileall "/home/bran/Documents/gittest/devel/lib/python2.7/dist-packages/common_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/bran/Documents/gittest/devel/lib/python2.7/dist-packages/common_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/bran/Documents/gittest/build/common_msgs/catkin_generated/installspace/common_msgs.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common_msgs/cmake" TYPE FILE FILES "/home/bran/Documents/gittest/build/common_msgs/catkin_generated/installspace/common_msgs-msg-extras.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common_msgs/cmake" TYPE FILE FILES
    "/home/bran/Documents/gittest/build/common_msgs/catkin_generated/installspace/common_msgsConfig.cmake"
    "/home/bran/Documents/gittest/build/common_msgs/catkin_generated/installspace/common_msgsConfig-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common_msgs" TYPE FILE FILES "/home/bran/Documents/gittest/src/common_msgs/package.xml")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcommon_msgs.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcommon_msgs.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcommon_msgs.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/bran/Documents/gittest/devel/lib/libcommon_msgs.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcommon_msgs.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcommon_msgs.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcommon_msgs.so"
         OLD_RPATH "/opt/ros/kinetic/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcommon_msgs.so")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/common_msgs" TYPE DIRECTORY FILES "/home/bran/Documents/gittest/src/common_msgs/include/common_msgs/" REGEX "/\\.svn$" EXCLUDE)
endif()

