;; Auto-generated. Do not edit!


(when (boundp 'apriltag_ros::AprilMission)
  (if (not (find-package "APRILTAG_ROS"))
    (make-package "APRILTAG_ROS"))
  (shadow 'AprilMission (find-package "APRILTAG_ROS")))
(unless (find-package "APRILTAG_ROS::APRILMISSION")
  (make-package "APRILTAG_ROS::APRILMISSION"))

(in-package "ROS")
;;//! \htmlinclude AprilMission.msg.html


(defclass apriltag_ros::AprilMission
  :super ros::object
  :slots (_status _mission_type ))

(defmethod apriltag_ros::AprilMission
  (:init
   (&key
    ((:status __status) "")
    ((:mission_type __mission_type) "")
    )
   (send-super :init)
   (setq _status (string __status))
   (setq _mission_type (string __mission_type))
   self)
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:mission_type
   (&optional __mission_type)
   (if __mission_type (setq _mission_type __mission_type)) _mission_type)
  (:serialization-length
   ()
   (+
    ;; string _status
    4 (length _status)
    ;; string _mission_type
    4 (length _mission_type)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _status
       (write-long (length _status) s) (princ _status s)
     ;; string _mission_type
       (write-long (length _mission_type) s) (princ _mission_type s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _status
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _status (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _mission_type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _mission_type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get apriltag_ros::AprilMission :md5sum-) "16b0c36840e5ecefd5f8b4717b215c8b")
(setf (get apriltag_ros::AprilMission :datatype-) "apriltag_ros/AprilMission")
(setf (get apriltag_ros::AprilMission :definition-)
      "# Apriltag mission
string status
string mission_type

")



(provide :apriltag_ros/AprilMission "16b0c36840e5ecefd5f8b4717b215c8b")


