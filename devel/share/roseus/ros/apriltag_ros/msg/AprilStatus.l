;; Auto-generated. Do not edit!


(when (boundp 'apriltag_ros::AprilStatus)
  (if (not (find-package "APRILTAG_ROS"))
    (make-package "APRILTAG_ROS"))
  (shadow 'AprilStatus (find-package "APRILTAG_ROS")))
(unless (find-package "APRILTAG_ROS::APRILSTATUS")
  (make-package "APRILTAG_ROS::APRILSTATUS"))

(in-package "ROS")
;;//! \htmlinclude AprilStatus.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))


(defclass apriltag_ros::AprilStatus
  :super ros::object
  :slots (_is_valid _mission_type _pose ))

(defmethod apriltag_ros::AprilStatus
  (:init
   (&key
    ((:is_valid __is_valid) nil)
    ((:mission_type __mission_type) "")
    ((:pose __pose) (instance geometry_msgs::Pose :init))
    )
   (send-super :init)
   (setq _is_valid __is_valid)
   (setq _mission_type (string __mission_type))
   (setq _pose __pose)
   self)
  (:is_valid
   (&optional __is_valid)
   (if __is_valid (setq _is_valid __is_valid)) _is_valid)
  (:mission_type
   (&optional __mission_type)
   (if __mission_type (setq _mission_type __mission_type)) _mission_type)
  (:pose
   (&rest __pose)
   (if (keywordp (car __pose))
       (send* _pose __pose)
     (progn
       (if __pose (setq _pose (car __pose)))
       _pose)))
  (:serialization-length
   ()
   (+
    ;; bool _is_valid
    1
    ;; string _mission_type
    4 (length _mission_type)
    ;; geometry_msgs/Pose _pose
    (send _pose :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _is_valid
       (if _is_valid (write-byte -1 s) (write-byte 0 s))
     ;; string _mission_type
       (write-long (length _mission_type) s) (princ _mission_type s)
     ;; geometry_msgs/Pose _pose
       (send _pose :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _is_valid
     (setq _is_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; string _mission_type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _mission_type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; geometry_msgs/Pose _pose
     (send _pose :deserialize buf ptr-) (incf ptr- (send _pose :serialization-length))
   ;;
   self)
  )

(setf (get apriltag_ros::AprilStatus :md5sum-) "3cb78f57ed11ac61e061a148deec0fea")
(setf (get apriltag_ros::AprilStatus :datatype-) "apriltag_ros/AprilStatus")
(setf (get apriltag_ros::AprilStatus :definition-)
      "# Apriltag status
bool is_valid
string mission_type
geometry_msgs/Pose pose

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

")



(provide :apriltag_ros/AprilStatus "3cb78f57ed11ac61e061a148deec0fea")


