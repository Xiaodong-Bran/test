;; Auto-generated. Do not edit!


(when (boundp 'apriltag_ros::GetAprilStatus)
  (if (not (find-package "APRILTAG_ROS"))
    (make-package "APRILTAG_ROS"))
  (shadow 'GetAprilStatus (find-package "APRILTAG_ROS")))
(unless (find-package "APRILTAG_ROS::GETAPRILSTATUS")
  (make-package "APRILTAG_ROS::GETAPRILSTATUS"))
(unless (find-package "APRILTAG_ROS::GETAPRILSTATUSREQUEST")
  (make-package "APRILTAG_ROS::GETAPRILSTATUSREQUEST"))
(unless (find-package "APRILTAG_ROS::GETAPRILSTATUSRESPONSE")
  (make-package "APRILTAG_ROS::GETAPRILSTATUSRESPONSE"))

(in-package "ROS")





(defclass apriltag_ros::GetAprilStatusRequest
  :super ros::object
  :slots (_mission_switch _mission_type ))

(defmethod apriltag_ros::GetAprilStatusRequest
  (:init
   (&key
    ((:mission_switch __mission_switch) "")
    ((:mission_type __mission_type) "")
    )
   (send-super :init)
   (setq _mission_switch (string __mission_switch))
   (setq _mission_type (string __mission_type))
   self)
  (:mission_switch
   (&optional __mission_switch)
   (if __mission_switch (setq _mission_switch __mission_switch)) _mission_switch)
  (:mission_type
   (&optional __mission_type)
   (if __mission_type (setq _mission_type __mission_type)) _mission_type)
  (:serialization-length
   ()
   (+
    ;; string _mission_switch
    4 (length _mission_switch)
    ;; string _mission_type
    4 (length _mission_type)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _mission_switch
       (write-long (length _mission_switch) s) (princ _mission_switch s)
     ;; string _mission_type
       (write-long (length _mission_type) s) (princ _mission_type s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _mission_switch
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _mission_switch (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _mission_type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _mission_type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass apriltag_ros::GetAprilStatusResponse
  :super ros::object
  :slots (_april_status ))

(defmethod apriltag_ros::GetAprilStatusResponse
  (:init
   (&key
    ((:april_status __april_status) (instance apriltag_ros::AprilStatus :init))
    )
   (send-super :init)
   (setq _april_status __april_status)
   self)
  (:april_status
   (&rest __april_status)
   (if (keywordp (car __april_status))
       (send* _april_status __april_status)
     (progn
       (if __april_status (setq _april_status (car __april_status)))
       _april_status)))
  (:serialization-length
   ()
   (+
    ;; apriltag_ros/AprilStatus _april_status
    (send _april_status :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; apriltag_ros/AprilStatus _april_status
       (send _april_status :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; apriltag_ros/AprilStatus _april_status
     (send _april_status :deserialize buf ptr-) (incf ptr- (send _april_status :serialization-length))
   ;;
   self)
  )

(defclass apriltag_ros::GetAprilStatus
  :super ros::object
  :slots ())

(setf (get apriltag_ros::GetAprilStatus :md5sum-) "8b56a804bb0c4311853fc2df1c9bfb2e")
(setf (get apriltag_ros::GetAprilStatus :datatype-) "apriltag_ros/GetAprilStatus")
(setf (get apriltag_ros::GetAprilStatus :request) apriltag_ros::GetAprilStatusRequest)
(setf (get apriltag_ros::GetAprilStatus :response) apriltag_ros::GetAprilStatusResponse)

(defmethod apriltag_ros::GetAprilStatusRequest
  (:response () (instance apriltag_ros::GetAprilStatusResponse :init)))

(setf (get apriltag_ros::GetAprilStatusRequest :md5sum-) "8b56a804bb0c4311853fc2df1c9bfb2e")
(setf (get apriltag_ros::GetAprilStatusRequest :datatype-) "apriltag_ros/GetAprilStatusRequest")
(setf (get apriltag_ros::GetAprilStatusRequest :definition-)
      "
string mission_switch
string mission_type
---

apriltag_ros/AprilStatus april_status


================================================================================
MSG: apriltag_ros/AprilStatus
# Apriltag status
bool is_valid
string mission_type
geometry_msgs/Pose pose

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w
")

(setf (get apriltag_ros::GetAprilStatusResponse :md5sum-) "8b56a804bb0c4311853fc2df1c9bfb2e")
(setf (get apriltag_ros::GetAprilStatusResponse :datatype-) "apriltag_ros/GetAprilStatusResponse")
(setf (get apriltag_ros::GetAprilStatusResponse :definition-)
      "
string mission_switch
string mission_type
---

apriltag_ros/AprilStatus april_status


================================================================================
MSG: apriltag_ros/AprilStatus
# Apriltag status
bool is_valid
string mission_type
geometry_msgs/Pose pose

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w
")



(provide :apriltag_ros/GetAprilStatus "8b56a804bb0c4311853fc2df1c9bfb2e")


