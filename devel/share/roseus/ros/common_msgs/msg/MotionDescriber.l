;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::MotionDescriber)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'MotionDescriber (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::MOTIONDESCRIBER")
  (make-package "COMMON_MSGS::MOTIONDESCRIBER"))

(in-package "ROS")
;;//! \htmlinclude MotionDescriber.msg.html


(defclass common_msgs::MotionDescriber
  :super ros::object
  :slots (_planned_waypoint _max_dynamics ))

(defmethod common_msgs::MotionDescriber
  (:init
   (&key
    ((:planned_waypoint __planned_waypoint) (instance common_msgs::Waypoint :init))
    ((:max_dynamics __max_dynamics) (instance common_msgs::MaxDynamics :init))
    )
   (send-super :init)
   (setq _planned_waypoint __planned_waypoint)
   (setq _max_dynamics __max_dynamics)
   self)
  (:planned_waypoint
   (&rest __planned_waypoint)
   (if (keywordp (car __planned_waypoint))
       (send* _planned_waypoint __planned_waypoint)
     (progn
       (if __planned_waypoint (setq _planned_waypoint (car __planned_waypoint)))
       _planned_waypoint)))
  (:max_dynamics
   (&rest __max_dynamics)
   (if (keywordp (car __max_dynamics))
       (send* _max_dynamics __max_dynamics)
     (progn
       (if __max_dynamics (setq _max_dynamics (car __max_dynamics)))
       _max_dynamics)))
  (:serialization-length
   ()
   (+
    ;; common_msgs/Waypoint _planned_waypoint
    (send _planned_waypoint :serialization-length)
    ;; common_msgs/MaxDynamics _max_dynamics
    (send _max_dynamics :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; common_msgs/Waypoint _planned_waypoint
       (send _planned_waypoint :serialize s)
     ;; common_msgs/MaxDynamics _max_dynamics
       (send _max_dynamics :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; common_msgs/Waypoint _planned_waypoint
     (send _planned_waypoint :deserialize buf ptr-) (incf ptr- (send _planned_waypoint :serialization-length))
   ;; common_msgs/MaxDynamics _max_dynamics
     (send _max_dynamics :deserialize buf ptr-) (incf ptr- (send _max_dynamics :serialization-length))
   ;;
   self)
  )

(setf (get common_msgs::MotionDescriber :md5sum-) "bd2b714d2bfb859055e9c10c12ad6ca0")
(setf (get common_msgs::MotionDescriber :datatype-) "common_msgs/MotionDescriber")
(setf (get common_msgs::MotionDescriber :definition-)
      "common_msgs/Waypoint planned_waypoint
common_msgs/MaxDynamics max_dynamics

================================================================================
MSG: common_msgs/Waypoint
Header header
common_msgs/NavigationState navigation_state
string m_type
uint32 m_id

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: common_msgs/NavigationState
Header header
geometry_msgs/Pose pose
geometry_msgs/Twist velocity
geometry_msgs/Twist acceleration

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: common_msgs/MaxDynamics
Header header
geometry_msgs/Twist vel_max
geometry_msgs/Twist acc_max
geometry_msgs/Twist jerk_max

")



(provide :common_msgs/MotionDescriber "bd2b714d2bfb859055e9c10c12ad6ca0")


