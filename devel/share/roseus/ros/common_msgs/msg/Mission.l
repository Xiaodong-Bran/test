;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::Mission)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'Mission (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::MISSION")
  (make-package "COMMON_MSGS::MISSION"))

(in-package "ROS")
;;//! \htmlinclude Mission.msg.html
(if (not (find-package "SENSOR_MSGS"))
  (ros::roseus-add-msgs "sensor_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::Mission
  :super ros::object
  :slots (_header _m_type _m_id _waypoint _gps_waypoint _pixhawkCMD _pixhawkServo _redundancy ))

(defmethod common_msgs::Mission
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:m_type __m_type) "")
    ((:m_id __m_id) 0)
    ((:waypoint __waypoint) (instance common_msgs::Waypoint :init))
    ((:gps_waypoint __gps_waypoint) (instance sensor_msgs::NavSatFix :init))
    ((:pixhawkCMD __pixhawkCMD) (instance common_msgs::PixhawkCMD :init))
    ((:pixhawkServo __pixhawkServo) (instance common_msgs::PixhawkServo :init))
    ((:redundancy __redundancy) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _header __header)
   (setq _m_type (string __m_type))
   (setq _m_id (round __m_id))
   (setq _waypoint __waypoint)
   (setq _gps_waypoint __gps_waypoint)
   (setq _pixhawkCMD __pixhawkCMD)
   (setq _pixhawkServo __pixhawkServo)
   (setq _redundancy __redundancy)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:m_type
   (&optional __m_type)
   (if __m_type (setq _m_type __m_type)) _m_type)
  (:m_id
   (&optional __m_id)
   (if __m_id (setq _m_id __m_id)) _m_id)
  (:waypoint
   (&rest __waypoint)
   (if (keywordp (car __waypoint))
       (send* _waypoint __waypoint)
     (progn
       (if __waypoint (setq _waypoint (car __waypoint)))
       _waypoint)))
  (:gps_waypoint
   (&rest __gps_waypoint)
   (if (keywordp (car __gps_waypoint))
       (send* _gps_waypoint __gps_waypoint)
     (progn
       (if __gps_waypoint (setq _gps_waypoint (car __gps_waypoint)))
       _gps_waypoint)))
  (:pixhawkCMD
   (&rest __pixhawkCMD)
   (if (keywordp (car __pixhawkCMD))
       (send* _pixhawkCMD __pixhawkCMD)
     (progn
       (if __pixhawkCMD (setq _pixhawkCMD (car __pixhawkCMD)))
       _pixhawkCMD)))
  (:pixhawkServo
   (&rest __pixhawkServo)
   (if (keywordp (car __pixhawkServo))
       (send* _pixhawkServo __pixhawkServo)
     (progn
       (if __pixhawkServo (setq _pixhawkServo (car __pixhawkServo)))
       _pixhawkServo)))
  (:redundancy
   (&optional __redundancy)
   (if __redundancy (setq _redundancy __redundancy)) _redundancy)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; string _m_type
    4 (length _m_type)
    ;; uint32 _m_id
    4
    ;; common_msgs/Waypoint _waypoint
    (send _waypoint :serialization-length)
    ;; sensor_msgs/NavSatFix _gps_waypoint
    (send _gps_waypoint :serialization-length)
    ;; common_msgs/PixhawkCMD _pixhawkCMD
    (send _pixhawkCMD :serialization-length)
    ;; common_msgs/PixhawkServo _pixhawkServo
    (send _pixhawkServo :serialization-length)
    ;; float64[] _redundancy
    (* 8    (length _redundancy)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; string _m_type
       (write-long (length _m_type) s) (princ _m_type s)
     ;; uint32 _m_id
       (write-long _m_id s)
     ;; common_msgs/Waypoint _waypoint
       (send _waypoint :serialize s)
     ;; sensor_msgs/NavSatFix _gps_waypoint
       (send _gps_waypoint :serialize s)
     ;; common_msgs/PixhawkCMD _pixhawkCMD
       (send _pixhawkCMD :serialize s)
     ;; common_msgs/PixhawkServo _pixhawkServo
       (send _pixhawkServo :serialize s)
     ;; float64[] _redundancy
     (write-long (length _redundancy) s)
     (dotimes (i (length _redundancy))
       (sys::poke (elt _redundancy i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; string _m_type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _m_type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint32 _m_id
     (setq _m_id (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; common_msgs/Waypoint _waypoint
     (send _waypoint :deserialize buf ptr-) (incf ptr- (send _waypoint :serialization-length))
   ;; sensor_msgs/NavSatFix _gps_waypoint
     (send _gps_waypoint :deserialize buf ptr-) (incf ptr- (send _gps_waypoint :serialization-length))
   ;; common_msgs/PixhawkCMD _pixhawkCMD
     (send _pixhawkCMD :deserialize buf ptr-) (incf ptr- (send _pixhawkCMD :serialization-length))
   ;; common_msgs/PixhawkServo _pixhawkServo
     (send _pixhawkServo :deserialize buf ptr-) (incf ptr- (send _pixhawkServo :serialization-length))
   ;; float64[] _redundancy
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _redundancy (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _redundancy i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(setf (get common_msgs::Mission :md5sum-) "c9fcf876b16f31ef90c5d998cf1295bb")
(setf (get common_msgs::Mission :datatype-) "common_msgs/Mission")
(setf (get common_msgs::Mission :definition-)
      "Header header
string m_type
uint32 m_id
Waypoint waypoint
sensor_msgs/NavSatFix gps_waypoint
PixhawkCMD pixhawkCMD
PixhawkServo pixhawkServo
float64[] redundancy

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: common_msgs/Waypoint
Header header
common_msgs/NavigationState navigation_state
string m_type
uint32 m_id

================================================================================
MSG: common_msgs/NavigationState
Header header
geometry_msgs/Pose pose
geometry_msgs/Twist velocity
geometry_msgs/Twist acceleration

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: sensor_msgs/NavSatFix
# Navigation Satellite fix for any Global Navigation Satellite System
#
# Specified using the WGS 84 reference ellipsoid

# header.stamp specifies the ROS time for this measurement (the
#        corresponding satellite time may be reported using the
#        sensor_msgs/TimeReference message).
#
# header.frame_id is the frame of reference reported by the satellite
#        receiver, usually the location of the antenna.  This is a
#        Euclidean frame relative to the vehicle, not a reference
#        ellipsoid.
Header header

# satellite fix status information
NavSatStatus status

# Latitude [degrees]. Positive is north of equator; negative is south.
float64 latitude

# Longitude [degrees]. Positive is east of prime meridian; negative is west.
float64 longitude

# Altitude [m]. Positive is above the WGS 84 ellipsoid
# (quiet NaN if no altitude is available).
float64 altitude

# Position covariance [m^2] defined relative to a tangential plane
# through the reported position. The components are East, North, and
# Up (ENU), in row-major order.
#
# Beware: this coordinate system exhibits singularities at the poles.

float64[9] position_covariance

# If the covariance of the fix is known, fill it in completely. If the
# GPS receiver provides the variance of each measurement, put them
# along the diagonal. If only Dilution of Precision is available,
# estimate an approximate covariance from that.

uint8 COVARIANCE_TYPE_UNKNOWN = 0
uint8 COVARIANCE_TYPE_APPROXIMATED = 1
uint8 COVARIANCE_TYPE_DIAGONAL_KNOWN = 2
uint8 COVARIANCE_TYPE_KNOWN = 3

uint8 position_covariance_type

================================================================================
MSG: sensor_msgs/NavSatStatus
# Navigation Satellite fix status for any Global Navigation Satellite System

# Whether to output an augmented fix is determined by both the fix
# type and the last time differential corrections were received.  A
# fix is valid when status >= STATUS_FIX.

int8 STATUS_NO_FIX =  -1        # unable to fix position
int8 STATUS_FIX =      0        # unaugmented fix
int8 STATUS_SBAS_FIX = 1        # with satellite-based augmentation
int8 STATUS_GBAS_FIX = 2        # with ground-based augmentation

int8 status

# Bits defining which Global Navigation Satellite System signals were
# used by the receiver.

uint16 SERVICE_GPS =     1
uint16 SERVICE_GLONASS = 2
uint16 SERVICE_COMPASS = 4      # includes BeiDou.
uint16 SERVICE_GALILEO = 8

uint16 service

================================================================================
MSG: common_msgs/PixhawkCMD
Header header
uint8[3] cmd

================================================================================
MSG: common_msgs/PixhawkServo
Header header
float64[4] servo

")



(provide :common_msgs/Mission "c9fcf876b16f31ef90c5d998cf1295bb")


