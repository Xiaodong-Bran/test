;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::UWB_SendData)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'UWB_SendData (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::UWB_SENDDATA")
  (make-package "COMMON_MSGS::UWB_SENDDATA"))

(in-package "ROS")
;;//! \htmlinclude UWB_SendData.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::UWB_SendData
  :super ros::object
  :slots (_header _bytes _floats ))

(defmethod common_msgs::UWB_SendData
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:bytes __bytes) (make-array 0 :initial-element 0 :element-type :char))
    ((:floats __floats) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _header __header)
   (setq _bytes __bytes)
   (setq _floats __floats)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:bytes
   (&optional __bytes)
   (if __bytes (setq _bytes __bytes)) _bytes)
  (:floats
   (&optional __floats)
   (if __floats (setq _floats __floats)) _floats)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8[] _bytes
    (* 1    (length _bytes)) 4
    ;; float32[] _floats
    (* 4    (length _floats)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8[] _bytes
     (write-long (length _bytes) s)
     (princ _bytes s)
     ;; float32[] _floats
     (write-long (length _floats) s)
     (dotimes (i (length _floats))
       (sys::poke (elt _floats i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8[] _bytes
   (let ((n (sys::peek buf ptr- :integer))) (incf ptr- 4)
     (setq _bytes (make-array n :element-type :char))
     (replace _bytes buf :start2 ptr-) (incf ptr- n))
   ;; float32[] _floats
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _floats (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _floats i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     ))
   ;;
   self)
  )

(setf (get common_msgs::UWB_SendData :md5sum-) "52e2f7d765655ad53859ce3b275ab9ca")
(setf (get common_msgs::UWB_SendData :datatype-) "common_msgs/UWB_SendData")
(setf (get common_msgs::UWB_SendData :definition-)
      "Header header
uint8[] bytes
float32[] floats

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :common_msgs/UWB_SendData "52e2f7d765655ad53859ce3b275ab9ca")


