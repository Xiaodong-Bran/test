;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::Reference)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'Reference (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::REFERENCE")
  (make-package "COMMON_MSGS::REFERENCE"))

(in-package "ROS")
;;//! \htmlinclude Reference.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::Reference
  :super ros::object
  :slots (_header _status _m_id _navigation _pixhawkCMD _pixhawkServo ))

(defmethod common_msgs::Reference
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:status __status) 0)
    ((:m_id __m_id) 0)
    ((:navigation __navigation) (instance common_msgs::NavigationState :init))
    ((:pixhawkCMD __pixhawkCMD) (instance common_msgs::PixhawkCMD :init))
    ((:pixhawkServo __pixhawkServo) (instance common_msgs::PixhawkServo :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _status (round __status))
   (setq _m_id (round __m_id))
   (setq _navigation __navigation)
   (setq _pixhawkCMD __pixhawkCMD)
   (setq _pixhawkServo __pixhawkServo)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:m_id
   (&optional __m_id)
   (if __m_id (setq _m_id __m_id)) _m_id)
  (:navigation
   (&rest __navigation)
   (if (keywordp (car __navigation))
       (send* _navigation __navigation)
     (progn
       (if __navigation (setq _navigation (car __navigation)))
       _navigation)))
  (:pixhawkCMD
   (&rest __pixhawkCMD)
   (if (keywordp (car __pixhawkCMD))
       (send* _pixhawkCMD __pixhawkCMD)
     (progn
       (if __pixhawkCMD (setq _pixhawkCMD (car __pixhawkCMD)))
       _pixhawkCMD)))
  (:pixhawkServo
   (&rest __pixhawkServo)
   (if (keywordp (car __pixhawkServo))
       (send* _pixhawkServo __pixhawkServo)
     (progn
       (if __pixhawkServo (setq _pixhawkServo (car __pixhawkServo)))
       _pixhawkServo)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; int32 _status
    4
    ;; uint32 _m_id
    4
    ;; common_msgs/NavigationState _navigation
    (send _navigation :serialization-length)
    ;; common_msgs/PixhawkCMD _pixhawkCMD
    (send _pixhawkCMD :serialization-length)
    ;; common_msgs/PixhawkServo _pixhawkServo
    (send _pixhawkServo :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; int32 _status
       (write-long _status s)
     ;; uint32 _m_id
       (write-long _m_id s)
     ;; common_msgs/NavigationState _navigation
       (send _navigation :serialize s)
     ;; common_msgs/PixhawkCMD _pixhawkCMD
       (send _pixhawkCMD :serialize s)
     ;; common_msgs/PixhawkServo _pixhawkServo
       (send _pixhawkServo :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; int32 _status
     (setq _status (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _m_id
     (setq _m_id (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; common_msgs/NavigationState _navigation
     (send _navigation :deserialize buf ptr-) (incf ptr- (send _navigation :serialization-length))
   ;; common_msgs/PixhawkCMD _pixhawkCMD
     (send _pixhawkCMD :deserialize buf ptr-) (incf ptr- (send _pixhawkCMD :serialization-length))
   ;; common_msgs/PixhawkServo _pixhawkServo
     (send _pixhawkServo :deserialize buf ptr-) (incf ptr- (send _pixhawkServo :serialization-length))
   ;;
   self)
  )

(setf (get common_msgs::Reference :md5sum-) "9eb864d6aea142d4059c163d7c3b3db7")
(setf (get common_msgs::Reference :datatype-) "common_msgs/Reference")
(setf (get common_msgs::Reference :definition-)
      "Header header
int32 status
uint32 m_id
NavigationState navigation
PixhawkCMD pixhawkCMD
PixhawkServo pixhawkServo

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: common_msgs/NavigationState
Header header
geometry_msgs/Pose pose
geometry_msgs/Twist velocity
geometry_msgs/Twist acceleration

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: common_msgs/PixhawkCMD
Header header
uint8[3] cmd

================================================================================
MSG: common_msgs/PixhawkServo
Header header
float64[4] servo

")



(provide :common_msgs/Reference "9eb864d6aea142d4059c163d7c3b3db7")


