;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::UWB_FullNeighborDatabase)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'UWB_FullNeighborDatabase (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::UWB_FULLNEIGHBORDATABASE")
  (make-package "COMMON_MSGS::UWB_FULLNEIGHBORDATABASE"))

(in-package "ROS")
;;//! \htmlinclude UWB_FullNeighborDatabase.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::UWB_FullNeighborDatabase
  :super ros::object
  :slots (_header _nodeId _msgType _msgId _numNeighborEntries _sortType _reserved _timestamp _status _neighbors ))

(defmethod common_msgs::UWB_FullNeighborDatabase
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:nodeId __nodeId) 0)
    ((:msgType __msgType) 0)
    ((:msgId __msgId) 0)
    ((:numNeighborEntries __numNeighborEntries) 0)
    ((:sortType __sortType) 0)
    ((:reserved __reserved) 0)
    ((:timestamp __timestamp) 0)
    ((:status __status) 0)
    ((:neighbors __neighbors) (let (r) (dotimes (i 0) (push (instance common_msgs::UWB_FullNeighborDatabaseEntry :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _nodeId (round __nodeId))
   (setq _msgType (round __msgType))
   (setq _msgId (round __msgId))
   (setq _numNeighborEntries (round __numNeighborEntries))
   (setq _sortType (round __sortType))
   (setq _reserved (round __reserved))
   (setq _timestamp (round __timestamp))
   (setq _status (round __status))
   (setq _neighbors __neighbors)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:nodeId
   (&optional __nodeId)
   (if __nodeId (setq _nodeId __nodeId)) _nodeId)
  (:msgType
   (&optional __msgType)
   (if __msgType (setq _msgType __msgType)) _msgType)
  (:msgId
   (&optional __msgId)
   (if __msgId (setq _msgId __msgId)) _msgId)
  (:numNeighborEntries
   (&optional __numNeighborEntries)
   (if __numNeighborEntries (setq _numNeighborEntries __numNeighborEntries)) _numNeighborEntries)
  (:sortType
   (&optional __sortType)
   (if __sortType (setq _sortType __sortType)) _sortType)
  (:reserved
   (&optional __reserved)
   (if __reserved (setq _reserved __reserved)) _reserved)
  (:timestamp
   (&optional __timestamp)
   (if __timestamp (setq _timestamp __timestamp)) _timestamp)
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:neighbors
   (&rest __neighbors)
   (if (keywordp (car __neighbors))
       (send* _neighbors __neighbors)
     (progn
       (if __neighbors (setq _neighbors (car __neighbors)))
       _neighbors)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint32 _nodeId
    4
    ;; uint16 _msgType
    2
    ;; uint16 _msgId
    2
    ;; uint8 _numNeighborEntries
    1
    ;; uint8 _sortType
    1
    ;; uint16 _reserved
    2
    ;; uint32 _timestamp
    4
    ;; uint32 _status
    4
    ;; common_msgs/UWB_FullNeighborDatabaseEntry[] _neighbors
    (apply #'+ (send-all _neighbors :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint32 _nodeId
       (write-long _nodeId s)
     ;; uint16 _msgType
       (write-word _msgType s)
     ;; uint16 _msgId
       (write-word _msgId s)
     ;; uint8 _numNeighborEntries
       (write-byte _numNeighborEntries s)
     ;; uint8 _sortType
       (write-byte _sortType s)
     ;; uint16 _reserved
       (write-word _reserved s)
     ;; uint32 _timestamp
       (write-long _timestamp s)
     ;; uint32 _status
       (write-long _status s)
     ;; common_msgs/UWB_FullNeighborDatabaseEntry[] _neighbors
     (write-long (length _neighbors) s)
     (dolist (elem _neighbors)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint32 _nodeId
     (setq _nodeId (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint16 _msgType
     (setq _msgType (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _msgId
     (setq _msgId (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint8 _numNeighborEntries
     (setq _numNeighborEntries (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _sortType
     (setq _sortType (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _reserved
     (setq _reserved (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _timestamp
     (setq _timestamp (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _status
     (setq _status (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; common_msgs/UWB_FullNeighborDatabaseEntry[] _neighbors
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _neighbors (let (r) (dotimes (i n) (push (instance common_msgs::UWB_FullNeighborDatabaseEntry :init) r)) r))
     (dolist (elem- _neighbors)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get common_msgs::UWB_FullNeighborDatabase :md5sum-) "e5469a87bded28f455f13fbf487a001b")
(setf (get common_msgs::UWB_FullNeighborDatabase :datatype-) "common_msgs/UWB_FullNeighborDatabase")
(setf (get common_msgs::UWB_FullNeighborDatabase :definition-)
      "Header header

uint32 nodeId

uint16 msgType
uint16 msgId
uint8 numNeighborEntries
uint8 sortType
uint16 reserved
uint32 timestamp
uint32 status
UWB_FullNeighborDatabaseEntry[] neighbors

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: common_msgs/UWB_FullNeighborDatabaseEntry
Header header

uint32 nodeId
uint8 rangeStatus
uint8 antennaMode
uint16 stopwatchTime
uint32 rangeMm
uint16 rangeErrorEstimate
uint16 rangeVelocity
uint8 rangeMeasurementType
uint8 flags
uint16 ledFlags
uint16 noise
uint16 vPeak
uint16 statsNumRangeAttempts
uint16 statsNumRangeSuccesses
uint32 statsAgeMs
uint32 rangeUpdateTimestampMs
uint32 lastHeardTimestampMs
uint32 addedToNDBTimestampMs

")



(provide :common_msgs/UWB_FullNeighborDatabase "e5469a87bded28f455f13fbf487a001b")


