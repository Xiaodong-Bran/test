;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::GPSVelocity)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'GPSVelocity (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::GPSVELOCITY")
  (make-package "COMMON_MSGS::GPSVELOCITY"))

(in-package "ROS")
;;//! \htmlinclude GPSVelocity.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::GPSVelocity
  :super ros::object
  :slots (_header _lat_vel _lon_vel _alt_vel ))

(defmethod common_msgs::GPSVelocity
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:lat_vel __lat_vel) 0.0)
    ((:lon_vel __lon_vel) 0.0)
    ((:alt_vel __alt_vel) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _lat_vel (float __lat_vel))
   (setq _lon_vel (float __lon_vel))
   (setq _alt_vel (float __alt_vel))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:lat_vel
   (&optional __lat_vel)
   (if __lat_vel (setq _lat_vel __lat_vel)) _lat_vel)
  (:lon_vel
   (&optional __lon_vel)
   (if __lon_vel (setq _lon_vel __lon_vel)) _lon_vel)
  (:alt_vel
   (&optional __alt_vel)
   (if __alt_vel (setq _alt_vel __alt_vel)) _alt_vel)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float64 _lat_vel
    8
    ;; float64 _lon_vel
    8
    ;; float64 _alt_vel
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float64 _lat_vel
       (sys::poke _lat_vel (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _lon_vel
       (sys::poke _lon_vel (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _alt_vel
       (sys::poke _alt_vel (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float64 _lat_vel
     (setq _lat_vel (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _lon_vel
     (setq _lon_vel (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _alt_vel
     (setq _alt_vel (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get common_msgs::GPSVelocity :md5sum-) "cd07811c37397d55c764919d4433214e")
(setf (get common_msgs::GPSVelocity :datatype-) "common_msgs/GPSVelocity")
(setf (get common_msgs::GPSVelocity :definition-)
      "Header header
float64 lat_vel
float64 lon_vel
float64 alt_vel

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :common_msgs/GPSVelocity "cd07811c37397d55c764919d4433214e")


