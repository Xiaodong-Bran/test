;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::MissionStatus)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'MissionStatus (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::MISSIONSTATUS")
  (make-package "COMMON_MSGS::MISSIONSTATUS"))

(in-package "ROS")
;;//! \htmlinclude MissionStatus.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::MissionStatus
  :super ros::object
  :slots (_header _mission _pp_status _is_reference_reached _is_measurement_reached ))

(defmethod common_msgs::MissionStatus
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:mission __mission) (instance common_msgs::Mission :init))
    ((:pp_status __pp_status) (instance common_msgs::PathPlanningStatus :init))
    ((:is_reference_reached __is_reference_reached) nil)
    ((:is_measurement_reached __is_measurement_reached) nil)
    )
   (send-super :init)
   (setq _header __header)
   (setq _mission __mission)
   (setq _pp_status __pp_status)
   (setq _is_reference_reached __is_reference_reached)
   (setq _is_measurement_reached __is_measurement_reached)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:mission
   (&rest __mission)
   (if (keywordp (car __mission))
       (send* _mission __mission)
     (progn
       (if __mission (setq _mission (car __mission)))
       _mission)))
  (:pp_status
   (&rest __pp_status)
   (if (keywordp (car __pp_status))
       (send* _pp_status __pp_status)
     (progn
       (if __pp_status (setq _pp_status (car __pp_status)))
       _pp_status)))
  (:is_reference_reached
   (&optional __is_reference_reached)
   (if __is_reference_reached (setq _is_reference_reached __is_reference_reached)) _is_reference_reached)
  (:is_measurement_reached
   (&optional __is_measurement_reached)
   (if __is_measurement_reached (setq _is_measurement_reached __is_measurement_reached)) _is_measurement_reached)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; common_msgs/Mission _mission
    (send _mission :serialization-length)
    ;; common_msgs/PathPlanningStatus _pp_status
    (send _pp_status :serialization-length)
    ;; bool _is_reference_reached
    1
    ;; bool _is_measurement_reached
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; common_msgs/Mission _mission
       (send _mission :serialize s)
     ;; common_msgs/PathPlanningStatus _pp_status
       (send _pp_status :serialize s)
     ;; bool _is_reference_reached
       (if _is_reference_reached (write-byte -1 s) (write-byte 0 s))
     ;; bool _is_measurement_reached
       (if _is_measurement_reached (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; common_msgs/Mission _mission
     (send _mission :deserialize buf ptr-) (incf ptr- (send _mission :serialization-length))
   ;; common_msgs/PathPlanningStatus _pp_status
     (send _pp_status :deserialize buf ptr-) (incf ptr- (send _pp_status :serialization-length))
   ;; bool _is_reference_reached
     (setq _is_reference_reached (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _is_measurement_reached
     (setq _is_measurement_reached (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get common_msgs::MissionStatus :md5sum-) "52a8faff66c070afec63a12e44128dac")
(setf (get common_msgs::MissionStatus :datatype-) "common_msgs/MissionStatus")
(setf (get common_msgs::MissionStatus :definition-)
      "Header header
Mission mission
PathPlanningStatus pp_status
bool is_reference_reached
bool is_measurement_reached

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: common_msgs/Mission
Header header
string m_type
uint32 m_id
Waypoint waypoint
sensor_msgs/NavSatFix gps_waypoint
PixhawkCMD pixhawkCMD
PixhawkServo pixhawkServo
float64[] redundancy

================================================================================
MSG: common_msgs/Waypoint
Header header
common_msgs/NavigationState navigation_state
string m_type
uint32 m_id

================================================================================
MSG: common_msgs/NavigationState
Header header
geometry_msgs/Pose pose
geometry_msgs/Twist velocity
geometry_msgs/Twist acceleration

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: sensor_msgs/NavSatFix
# Navigation Satellite fix for any Global Navigation Satellite System
#
# Specified using the WGS 84 reference ellipsoid

# header.stamp specifies the ROS time for this measurement (the
#        corresponding satellite time may be reported using the
#        sensor_msgs/TimeReference message).
#
# header.frame_id is the frame of reference reported by the satellite
#        receiver, usually the location of the antenna.  This is a
#        Euclidean frame relative to the vehicle, not a reference
#        ellipsoid.
Header header

# satellite fix status information
NavSatStatus status

# Latitude [degrees]. Positive is north of equator; negative is south.
float64 latitude

# Longitude [degrees]. Positive is east of prime meridian; negative is west.
float64 longitude

# Altitude [m]. Positive is above the WGS 84 ellipsoid
# (quiet NaN if no altitude is available).
float64 altitude

# Position covariance [m^2] defined relative to a tangential plane
# through the reported position. The components are East, North, and
# Up (ENU), in row-major order.
#
# Beware: this coordinate system exhibits singularities at the poles.

float64[9] position_covariance

# If the covariance of the fix is known, fill it in completely. If the
# GPS receiver provides the variance of each measurement, put them
# along the diagonal. If only Dilution of Precision is available,
# estimate an approximate covariance from that.

uint8 COVARIANCE_TYPE_UNKNOWN = 0
uint8 COVARIANCE_TYPE_APPROXIMATED = 1
uint8 COVARIANCE_TYPE_DIAGONAL_KNOWN = 2
uint8 COVARIANCE_TYPE_KNOWN = 3

uint8 position_covariance_type

================================================================================
MSG: sensor_msgs/NavSatStatus
# Navigation Satellite fix status for any Global Navigation Satellite System

# Whether to output an augmented fix is determined by both the fix
# type and the last time differential corrections were received.  A
# fix is valid when status >= STATUS_FIX.

int8 STATUS_NO_FIX =  -1        # unable to fix position
int8 STATUS_FIX =      0        # unaugmented fix
int8 STATUS_SBAS_FIX = 1        # with satellite-based augmentation
int8 STATUS_GBAS_FIX = 2        # with ground-based augmentation

int8 status

# Bits defining which Global Navigation Satellite System signals were
# used by the receiver.

uint16 SERVICE_GPS =     1
uint16 SERVICE_GLONASS = 2
uint16 SERVICE_COMPASS = 4      # includes BeiDou.
uint16 SERVICE_GALILEO = 8

uint16 service

================================================================================
MSG: common_msgs/PixhawkCMD
Header header
uint8[3] cmd

================================================================================
MSG: common_msgs/PixhawkServo
Header header
float64[4] servo

================================================================================
MSG: common_msgs/PathPlanningStatus
Header header
bool is_reached
bool is_target_reachable
common_msgs/Waypoint waypoint
geometry_msgs/Pose alternative_target_position_NWU

")



(provide :common_msgs/MissionStatus "52a8faff66c070afec63a12e44128dac")


