;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::MeasurementPosVel)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'MeasurementPosVel (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::MEASUREMENTPOSVEL")
  (make-package "COMMON_MSGS::MEASUREMENTPOSVEL"))

(in-package "ROS")
;;//! \htmlinclude MeasurementPosVel.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::MeasurementPosVel
  :super ros::object
  :slots (_header _position _velocity ))

(defmethod common_msgs::MeasurementPosVel
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:position __position) (instance common_msgs::MeasurementPosition :init))
    ((:velocity __velocity) (instance common_msgs::MeasurementVelocity :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _position __position)
   (setq _velocity __velocity)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:position
   (&rest __position)
   (if (keywordp (car __position))
       (send* _position __position)
     (progn
       (if __position (setq _position (car __position)))
       _position)))
  (:velocity
   (&rest __velocity)
   (if (keywordp (car __velocity))
       (send* _velocity __velocity)
     (progn
       (if __velocity (setq _velocity (car __velocity)))
       _velocity)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; common_msgs/MeasurementPosition _position
    (send _position :serialization-length)
    ;; common_msgs/MeasurementVelocity _velocity
    (send _velocity :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; common_msgs/MeasurementPosition _position
       (send _position :serialize s)
     ;; common_msgs/MeasurementVelocity _velocity
       (send _velocity :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; common_msgs/MeasurementPosition _position
     (send _position :deserialize buf ptr-) (incf ptr- (send _position :serialization-length))
   ;; common_msgs/MeasurementVelocity _velocity
     (send _velocity :deserialize buf ptr-) (incf ptr- (send _velocity :serialization-length))
   ;;
   self)
  )

(setf (get common_msgs::MeasurementPosVel :md5sum-) "9357e8ec18526a1fb373c79a69f060dd")
(setf (get common_msgs::MeasurementPosVel :datatype-) "common_msgs/MeasurementPosVel")
(setf (get common_msgs::MeasurementPosVel :definition-)
      "Header header
MeasurementPosition position
MeasurementVelocity velocity

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: common_msgs/MeasurementPosition
Header header
geometry_msgs/Pose pose
bool is_xy_valid
bool is_z_valid
bool is_yaw_valid

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: common_msgs/MeasurementVelocity
Header header
geometry_msgs/Twist velocity
bool is_xy_valid
bool is_z_valid
bool is_yaw_valid

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
")



(provide :common_msgs/MeasurementPosVel "9357e8ec18526a1fb373c79a69f060dd")


