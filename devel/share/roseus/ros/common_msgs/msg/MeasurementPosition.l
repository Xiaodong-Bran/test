;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::MeasurementPosition)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'MeasurementPosition (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::MEASUREMENTPOSITION")
  (make-package "COMMON_MSGS::MEASUREMENTPOSITION"))

(in-package "ROS")
;;//! \htmlinclude MeasurementPosition.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::MeasurementPosition
  :super ros::object
  :slots (_header _pose _is_xy_valid _is_z_valid _is_yaw_valid ))

(defmethod common_msgs::MeasurementPosition
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:pose __pose) (instance geometry_msgs::Pose :init))
    ((:is_xy_valid __is_xy_valid) nil)
    ((:is_z_valid __is_z_valid) nil)
    ((:is_yaw_valid __is_yaw_valid) nil)
    )
   (send-super :init)
   (setq _header __header)
   (setq _pose __pose)
   (setq _is_xy_valid __is_xy_valid)
   (setq _is_z_valid __is_z_valid)
   (setq _is_yaw_valid __is_yaw_valid)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:pose
   (&rest __pose)
   (if (keywordp (car __pose))
       (send* _pose __pose)
     (progn
       (if __pose (setq _pose (car __pose)))
       _pose)))
  (:is_xy_valid
   (&optional __is_xy_valid)
   (if __is_xy_valid (setq _is_xy_valid __is_xy_valid)) _is_xy_valid)
  (:is_z_valid
   (&optional __is_z_valid)
   (if __is_z_valid (setq _is_z_valid __is_z_valid)) _is_z_valid)
  (:is_yaw_valid
   (&optional __is_yaw_valid)
   (if __is_yaw_valid (setq _is_yaw_valid __is_yaw_valid)) _is_yaw_valid)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Pose _pose
    (send _pose :serialization-length)
    ;; bool _is_xy_valid
    1
    ;; bool _is_z_valid
    1
    ;; bool _is_yaw_valid
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Pose _pose
       (send _pose :serialize s)
     ;; bool _is_xy_valid
       (if _is_xy_valid (write-byte -1 s) (write-byte 0 s))
     ;; bool _is_z_valid
       (if _is_z_valid (write-byte -1 s) (write-byte 0 s))
     ;; bool _is_yaw_valid
       (if _is_yaw_valid (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Pose _pose
     (send _pose :deserialize buf ptr-) (incf ptr- (send _pose :serialization-length))
   ;; bool _is_xy_valid
     (setq _is_xy_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _is_z_valid
     (setq _is_z_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _is_yaw_valid
     (setq _is_yaw_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get common_msgs::MeasurementPosition :md5sum-) "49fef8348ffe3fc1ef41fbe01ce739a3")
(setf (get common_msgs::MeasurementPosition :datatype-) "common_msgs/MeasurementPosition")
(setf (get common_msgs::MeasurementPosition :definition-)
      "Header header
geometry_msgs/Pose pose
bool is_xy_valid
bool is_z_valid
bool is_yaw_valid

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

")



(provide :common_msgs/MeasurementPosition "49fef8348ffe3fc1ef41fbe01ce739a3")


