;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::Waypoint)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'Waypoint (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::WAYPOINT")
  (make-package "COMMON_MSGS::WAYPOINT"))

(in-package "ROS")
;;//! \htmlinclude Waypoint.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::Waypoint
  :super ros::object
  :slots (_header _navigation_state _m_type _m_id ))

(defmethod common_msgs::Waypoint
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:navigation_state __navigation_state) (instance common_msgs::NavigationState :init))
    ((:m_type __m_type) "")
    ((:m_id __m_id) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _navigation_state __navigation_state)
   (setq _m_type (string __m_type))
   (setq _m_id (round __m_id))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:navigation_state
   (&rest __navigation_state)
   (if (keywordp (car __navigation_state))
       (send* _navigation_state __navigation_state)
     (progn
       (if __navigation_state (setq _navigation_state (car __navigation_state)))
       _navigation_state)))
  (:m_type
   (&optional __m_type)
   (if __m_type (setq _m_type __m_type)) _m_type)
  (:m_id
   (&optional __m_id)
   (if __m_id (setq _m_id __m_id)) _m_id)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; common_msgs/NavigationState _navigation_state
    (send _navigation_state :serialization-length)
    ;; string _m_type
    4 (length _m_type)
    ;; uint32 _m_id
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; common_msgs/NavigationState _navigation_state
       (send _navigation_state :serialize s)
     ;; string _m_type
       (write-long (length _m_type) s) (princ _m_type s)
     ;; uint32 _m_id
       (write-long _m_id s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; common_msgs/NavigationState _navigation_state
     (send _navigation_state :deserialize buf ptr-) (incf ptr- (send _navigation_state :serialization-length))
   ;; string _m_type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _m_type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint32 _m_id
     (setq _m_id (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get common_msgs::Waypoint :md5sum-) "cb8be93f3e3ff3f65f66e7f742783b2f")
(setf (get common_msgs::Waypoint :datatype-) "common_msgs/Waypoint")
(setf (get common_msgs::Waypoint :definition-)
      "Header header
common_msgs/NavigationState navigation_state
string m_type
uint32 m_id

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: common_msgs/NavigationState
Header header
geometry_msgs/Pose pose
geometry_msgs/Twist velocity
geometry_msgs/Twist acceleration

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
")



(provide :common_msgs/Waypoint "cb8be93f3e3ff3f65f66e7f742783b2f")


