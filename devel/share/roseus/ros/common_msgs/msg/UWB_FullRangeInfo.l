;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::UWB_FullRangeInfo)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'UWB_FullRangeInfo (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::UWB_FULLRANGEINFO")
  (make-package "COMMON_MSGS::UWB_FULLRANGEINFO"))

(in-package "ROS")
;;//! \htmlinclude UWB_FullRangeInfo.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::UWB_FullRangeInfo
  :super ros::object
  :slots (_header _nodeId _msgType _msgId _responderId _rangeStatus _antennaMode _stopwatchTime _precisionRangeMm _coarseRangeMm _filteredRangeMm _precisionRangeErrEst _coarseRangeErrEst _filteredRangeErrEst _filteredRangeVel _filteredRangeVelErrEst _rangeMeasurementType _reserved _reqLEDFlags _respLEDFlags _noise _vPeak _coarseTOFInBins _timestamp ))

(defmethod common_msgs::UWB_FullRangeInfo
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:nodeId __nodeId) 0)
    ((:msgType __msgType) 0)
    ((:msgId __msgId) 0)
    ((:responderId __responderId) 0)
    ((:rangeStatus __rangeStatus) 0)
    ((:antennaMode __antennaMode) 0)
    ((:stopwatchTime __stopwatchTime) 0)
    ((:precisionRangeMm __precisionRangeMm) 0)
    ((:coarseRangeMm __coarseRangeMm) 0)
    ((:filteredRangeMm __filteredRangeMm) 0)
    ((:precisionRangeErrEst __precisionRangeErrEst) 0)
    ((:coarseRangeErrEst __coarseRangeErrEst) 0)
    ((:filteredRangeErrEst __filteredRangeErrEst) 0)
    ((:filteredRangeVel __filteredRangeVel) 0)
    ((:filteredRangeVelErrEst __filteredRangeVelErrEst) 0)
    ((:rangeMeasurementType __rangeMeasurementType) 0)
    ((:reserved __reserved) 0)
    ((:reqLEDFlags __reqLEDFlags) 0)
    ((:respLEDFlags __respLEDFlags) 0)
    ((:noise __noise) 0)
    ((:vPeak __vPeak) 0)
    ((:coarseTOFInBins __coarseTOFInBins) 0)
    ((:timestamp __timestamp) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _nodeId (round __nodeId))
   (setq _msgType (round __msgType))
   (setq _msgId (round __msgId))
   (setq _responderId (round __responderId))
   (setq _rangeStatus (round __rangeStatus))
   (setq _antennaMode (round __antennaMode))
   (setq _stopwatchTime (round __stopwatchTime))
   (setq _precisionRangeMm (round __precisionRangeMm))
   (setq _coarseRangeMm (round __coarseRangeMm))
   (setq _filteredRangeMm (round __filteredRangeMm))
   (setq _precisionRangeErrEst (round __precisionRangeErrEst))
   (setq _coarseRangeErrEst (round __coarseRangeErrEst))
   (setq _filteredRangeErrEst (round __filteredRangeErrEst))
   (setq _filteredRangeVel (round __filteredRangeVel))
   (setq _filteredRangeVelErrEst (round __filteredRangeVelErrEst))
   (setq _rangeMeasurementType (round __rangeMeasurementType))
   (setq _reserved (round __reserved))
   (setq _reqLEDFlags (round __reqLEDFlags))
   (setq _respLEDFlags (round __respLEDFlags))
   (setq _noise (round __noise))
   (setq _vPeak (round __vPeak))
   (setq _coarseTOFInBins (round __coarseTOFInBins))
   (setq _timestamp (round __timestamp))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:nodeId
   (&optional __nodeId)
   (if __nodeId (setq _nodeId __nodeId)) _nodeId)
  (:msgType
   (&optional __msgType)
   (if __msgType (setq _msgType __msgType)) _msgType)
  (:msgId
   (&optional __msgId)
   (if __msgId (setq _msgId __msgId)) _msgId)
  (:responderId
   (&optional __responderId)
   (if __responderId (setq _responderId __responderId)) _responderId)
  (:rangeStatus
   (&optional __rangeStatus)
   (if __rangeStatus (setq _rangeStatus __rangeStatus)) _rangeStatus)
  (:antennaMode
   (&optional __antennaMode)
   (if __antennaMode (setq _antennaMode __antennaMode)) _antennaMode)
  (:stopwatchTime
   (&optional __stopwatchTime)
   (if __stopwatchTime (setq _stopwatchTime __stopwatchTime)) _stopwatchTime)
  (:precisionRangeMm
   (&optional __precisionRangeMm)
   (if __precisionRangeMm (setq _precisionRangeMm __precisionRangeMm)) _precisionRangeMm)
  (:coarseRangeMm
   (&optional __coarseRangeMm)
   (if __coarseRangeMm (setq _coarseRangeMm __coarseRangeMm)) _coarseRangeMm)
  (:filteredRangeMm
   (&optional __filteredRangeMm)
   (if __filteredRangeMm (setq _filteredRangeMm __filteredRangeMm)) _filteredRangeMm)
  (:precisionRangeErrEst
   (&optional __precisionRangeErrEst)
   (if __precisionRangeErrEst (setq _precisionRangeErrEst __precisionRangeErrEst)) _precisionRangeErrEst)
  (:coarseRangeErrEst
   (&optional __coarseRangeErrEst)
   (if __coarseRangeErrEst (setq _coarseRangeErrEst __coarseRangeErrEst)) _coarseRangeErrEst)
  (:filteredRangeErrEst
   (&optional __filteredRangeErrEst)
   (if __filteredRangeErrEst (setq _filteredRangeErrEst __filteredRangeErrEst)) _filteredRangeErrEst)
  (:filteredRangeVel
   (&optional __filteredRangeVel)
   (if __filteredRangeVel (setq _filteredRangeVel __filteredRangeVel)) _filteredRangeVel)
  (:filteredRangeVelErrEst
   (&optional __filteredRangeVelErrEst)
   (if __filteredRangeVelErrEst (setq _filteredRangeVelErrEst __filteredRangeVelErrEst)) _filteredRangeVelErrEst)
  (:rangeMeasurementType
   (&optional __rangeMeasurementType)
   (if __rangeMeasurementType (setq _rangeMeasurementType __rangeMeasurementType)) _rangeMeasurementType)
  (:reserved
   (&optional __reserved)
   (if __reserved (setq _reserved __reserved)) _reserved)
  (:reqLEDFlags
   (&optional __reqLEDFlags)
   (if __reqLEDFlags (setq _reqLEDFlags __reqLEDFlags)) _reqLEDFlags)
  (:respLEDFlags
   (&optional __respLEDFlags)
   (if __respLEDFlags (setq _respLEDFlags __respLEDFlags)) _respLEDFlags)
  (:noise
   (&optional __noise)
   (if __noise (setq _noise __noise)) _noise)
  (:vPeak
   (&optional __vPeak)
   (if __vPeak (setq _vPeak __vPeak)) _vPeak)
  (:coarseTOFInBins
   (&optional __coarseTOFInBins)
   (if __coarseTOFInBins (setq _coarseTOFInBins __coarseTOFInBins)) _coarseTOFInBins)
  (:timestamp
   (&optional __timestamp)
   (if __timestamp (setq _timestamp __timestamp)) _timestamp)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint32 _nodeId
    4
    ;; uint16 _msgType
    2
    ;; uint16 _msgId
    2
    ;; uint32 _responderId
    4
    ;; uint8 _rangeStatus
    1
    ;; uint8 _antennaMode
    1
    ;; uint16 _stopwatchTime
    2
    ;; uint32 _precisionRangeMm
    4
    ;; uint32 _coarseRangeMm
    4
    ;; uint32 _filteredRangeMm
    4
    ;; uint16 _precisionRangeErrEst
    2
    ;; uint16 _coarseRangeErrEst
    2
    ;; uint16 _filteredRangeErrEst
    2
    ;; uint16 _filteredRangeVel
    2
    ;; uint16 _filteredRangeVelErrEst
    2
    ;; uint8 _rangeMeasurementType
    1
    ;; uint8 _reserved
    1
    ;; uint16 _reqLEDFlags
    2
    ;; uint16 _respLEDFlags
    2
    ;; uint16 _noise
    2
    ;; uint16 _vPeak
    2
    ;; uint32 _coarseTOFInBins
    4
    ;; uint32 _timestamp
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint32 _nodeId
       (write-long _nodeId s)
     ;; uint16 _msgType
       (write-word _msgType s)
     ;; uint16 _msgId
       (write-word _msgId s)
     ;; uint32 _responderId
       (write-long _responderId s)
     ;; uint8 _rangeStatus
       (write-byte _rangeStatus s)
     ;; uint8 _antennaMode
       (write-byte _antennaMode s)
     ;; uint16 _stopwatchTime
       (write-word _stopwatchTime s)
     ;; uint32 _precisionRangeMm
       (write-long _precisionRangeMm s)
     ;; uint32 _coarseRangeMm
       (write-long _coarseRangeMm s)
     ;; uint32 _filteredRangeMm
       (write-long _filteredRangeMm s)
     ;; uint16 _precisionRangeErrEst
       (write-word _precisionRangeErrEst s)
     ;; uint16 _coarseRangeErrEst
       (write-word _coarseRangeErrEst s)
     ;; uint16 _filteredRangeErrEst
       (write-word _filteredRangeErrEst s)
     ;; uint16 _filteredRangeVel
       (write-word _filteredRangeVel s)
     ;; uint16 _filteredRangeVelErrEst
       (write-word _filteredRangeVelErrEst s)
     ;; uint8 _rangeMeasurementType
       (write-byte _rangeMeasurementType s)
     ;; uint8 _reserved
       (write-byte _reserved s)
     ;; uint16 _reqLEDFlags
       (write-word _reqLEDFlags s)
     ;; uint16 _respLEDFlags
       (write-word _respLEDFlags s)
     ;; uint16 _noise
       (write-word _noise s)
     ;; uint16 _vPeak
       (write-word _vPeak s)
     ;; uint32 _coarseTOFInBins
       (write-long _coarseTOFInBins s)
     ;; uint32 _timestamp
       (write-long _timestamp s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint32 _nodeId
     (setq _nodeId (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint16 _msgType
     (setq _msgType (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _msgId
     (setq _msgId (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _responderId
     (setq _responderId (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint8 _rangeStatus
     (setq _rangeStatus (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _antennaMode
     (setq _antennaMode (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _stopwatchTime
     (setq _stopwatchTime (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _precisionRangeMm
     (setq _precisionRangeMm (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _coarseRangeMm
     (setq _coarseRangeMm (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _filteredRangeMm
     (setq _filteredRangeMm (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint16 _precisionRangeErrEst
     (setq _precisionRangeErrEst (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _coarseRangeErrEst
     (setq _coarseRangeErrEst (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _filteredRangeErrEst
     (setq _filteredRangeErrEst (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _filteredRangeVel
     (setq _filteredRangeVel (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _filteredRangeVelErrEst
     (setq _filteredRangeVelErrEst (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint8 _rangeMeasurementType
     (setq _rangeMeasurementType (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _reserved
     (setq _reserved (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _reqLEDFlags
     (setq _reqLEDFlags (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _respLEDFlags
     (setq _respLEDFlags (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _noise
     (setq _noise (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _vPeak
     (setq _vPeak (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _coarseTOFInBins
     (setq _coarseTOFInBins (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _timestamp
     (setq _timestamp (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get common_msgs::UWB_FullRangeInfo :md5sum-) "6eed76e0c8b91a8a7fb2c268438b77be")
(setf (get common_msgs::UWB_FullRangeInfo :datatype-) "common_msgs/UWB_FullRangeInfo")
(setf (get common_msgs::UWB_FullRangeInfo :definition-)
      "Header header

uint32 nodeId

uint16 msgType
uint16 msgId

uint32 responderId

uint8 rangeStatus
uint8 antennaMode
uint16 stopwatchTime

uint32 precisionRangeMm
uint32 coarseRangeMm
uint32 filteredRangeMm

uint16 precisionRangeErrEst
uint16 coarseRangeErrEst
uint16 filteredRangeErrEst

uint16 filteredRangeVel
uint16 filteredRangeVelErrEst

uint8 rangeMeasurementType
uint8 reserved

uint16 reqLEDFlags
uint16 respLEDFlags

uint16 noise
uint16 vPeak

uint32 coarseTOFInBins

uint32 timestamp

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :common_msgs/UWB_FullRangeInfo "6eed76e0c8b91a8a7fb2c268438b77be")


