;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::PixhawkCMD)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'PixhawkCMD (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::PIXHAWKCMD")
  (make-package "COMMON_MSGS::PIXHAWKCMD"))

(in-package "ROS")
;;//! \htmlinclude PixhawkCMD.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::PixhawkCMD
  :super ros::object
  :slots (_header _cmd ))

(defmethod common_msgs::PixhawkCMD
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:cmd __cmd) (make-array 3 :initial-element 0 :element-type :char))
    )
   (send-super :init)
   (setq _header __header)
   (setq _cmd __cmd)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:cmd
   (&optional __cmd)
   (if __cmd (setq _cmd __cmd)) _cmd)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8[3] _cmd
    (* 1    3)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8[3] _cmd
     (princ _cmd s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8[3] _cmd
   (setq _cmd (make-array 3 :element-type :char))
   (replace _cmd buf :start2 ptr-) (incf ptr- 3)
   ;;
   self)
  )

(setf (get common_msgs::PixhawkCMD :md5sum-) "1c7846cd95a44bc1022bfecb4d174ebc")
(setf (get common_msgs::PixhawkCMD :datatype-) "common_msgs/PixhawkCMD")
(setf (get common_msgs::PixhawkCMD :definition-)
      "Header header
uint8[3] cmd

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :common_msgs/PixhawkCMD "1c7846cd95a44bc1022bfecb4d174ebc")


