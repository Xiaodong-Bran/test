;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::PixhawkServo)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'PixhawkServo (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::PIXHAWKSERVO")
  (make-package "COMMON_MSGS::PIXHAWKSERVO"))

(in-package "ROS")
;;//! \htmlinclude PixhawkServo.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::PixhawkServo
  :super ros::object
  :slots (_header _servo ))

(defmethod common_msgs::PixhawkServo
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:servo __servo) (make-array 4 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _header __header)
   (setq _servo __servo)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:servo
   (&optional __servo)
   (if __servo (setq _servo __servo)) _servo)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float64[4] _servo
    (* 8    4)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float64[4] _servo
     (dotimes (i 4)
       (sys::poke (elt _servo i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float64[4] _servo
   (dotimes (i (length _servo))
     (setf (elt _servo i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;;
   self)
  )

(setf (get common_msgs::PixhawkServo :md5sum-) "345334d3f0191b67a904ea14a08655e8")
(setf (get common_msgs::PixhawkServo :datatype-) "common_msgs/PixhawkServo")
(setf (get common_msgs::PixhawkServo :definition-)
      "Header header
float64[4] servo

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :common_msgs/PixhawkServo "345334d3f0191b67a904ea14a08655e8")


