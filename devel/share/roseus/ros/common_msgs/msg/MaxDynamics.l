;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::MaxDynamics)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'MaxDynamics (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::MAXDYNAMICS")
  (make-package "COMMON_MSGS::MAXDYNAMICS"))

(in-package "ROS")
;;//! \htmlinclude MaxDynamics.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::MaxDynamics
  :super ros::object
  :slots (_header _vel_max _acc_max _jerk_max ))

(defmethod common_msgs::MaxDynamics
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:vel_max __vel_max) (instance geometry_msgs::Twist :init))
    ((:acc_max __acc_max) (instance geometry_msgs::Twist :init))
    ((:jerk_max __jerk_max) (instance geometry_msgs::Twist :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _vel_max __vel_max)
   (setq _acc_max __acc_max)
   (setq _jerk_max __jerk_max)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:vel_max
   (&rest __vel_max)
   (if (keywordp (car __vel_max))
       (send* _vel_max __vel_max)
     (progn
       (if __vel_max (setq _vel_max (car __vel_max)))
       _vel_max)))
  (:acc_max
   (&rest __acc_max)
   (if (keywordp (car __acc_max))
       (send* _acc_max __acc_max)
     (progn
       (if __acc_max (setq _acc_max (car __acc_max)))
       _acc_max)))
  (:jerk_max
   (&rest __jerk_max)
   (if (keywordp (car __jerk_max))
       (send* _jerk_max __jerk_max)
     (progn
       (if __jerk_max (setq _jerk_max (car __jerk_max)))
       _jerk_max)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Twist _vel_max
    (send _vel_max :serialization-length)
    ;; geometry_msgs/Twist _acc_max
    (send _acc_max :serialization-length)
    ;; geometry_msgs/Twist _jerk_max
    (send _jerk_max :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Twist _vel_max
       (send _vel_max :serialize s)
     ;; geometry_msgs/Twist _acc_max
       (send _acc_max :serialize s)
     ;; geometry_msgs/Twist _jerk_max
       (send _jerk_max :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Twist _vel_max
     (send _vel_max :deserialize buf ptr-) (incf ptr- (send _vel_max :serialization-length))
   ;; geometry_msgs/Twist _acc_max
     (send _acc_max :deserialize buf ptr-) (incf ptr- (send _acc_max :serialization-length))
   ;; geometry_msgs/Twist _jerk_max
     (send _jerk_max :deserialize buf ptr-) (incf ptr- (send _jerk_max :serialization-length))
   ;;
   self)
  )

(setf (get common_msgs::MaxDynamics :md5sum-) "c3877f5e318c0ad8b9786835e1cba30d")
(setf (get common_msgs::MaxDynamics :datatype-) "common_msgs/MaxDynamics")
(setf (get common_msgs::MaxDynamics :definition-)
      "Header header
geometry_msgs/Twist vel_max
geometry_msgs/Twist acc_max
geometry_msgs/Twist jerk_max

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
")



(provide :common_msgs/MaxDynamics "c3877f5e318c0ad8b9786835e1cba30d")


