;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::MeasurementVelocity)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'MeasurementVelocity (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::MEASUREMENTVELOCITY")
  (make-package "COMMON_MSGS::MEASUREMENTVELOCITY"))

(in-package "ROS")
;;//! \htmlinclude MeasurementVelocity.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::MeasurementVelocity
  :super ros::object
  :slots (_header _velocity _is_xy_valid _is_z_valid _is_yaw_valid ))

(defmethod common_msgs::MeasurementVelocity
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:velocity __velocity) (instance geometry_msgs::Twist :init))
    ((:is_xy_valid __is_xy_valid) nil)
    ((:is_z_valid __is_z_valid) nil)
    ((:is_yaw_valid __is_yaw_valid) nil)
    )
   (send-super :init)
   (setq _header __header)
   (setq _velocity __velocity)
   (setq _is_xy_valid __is_xy_valid)
   (setq _is_z_valid __is_z_valid)
   (setq _is_yaw_valid __is_yaw_valid)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:velocity
   (&rest __velocity)
   (if (keywordp (car __velocity))
       (send* _velocity __velocity)
     (progn
       (if __velocity (setq _velocity (car __velocity)))
       _velocity)))
  (:is_xy_valid
   (&optional __is_xy_valid)
   (if __is_xy_valid (setq _is_xy_valid __is_xy_valid)) _is_xy_valid)
  (:is_z_valid
   (&optional __is_z_valid)
   (if __is_z_valid (setq _is_z_valid __is_z_valid)) _is_z_valid)
  (:is_yaw_valid
   (&optional __is_yaw_valid)
   (if __is_yaw_valid (setq _is_yaw_valid __is_yaw_valid)) _is_yaw_valid)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Twist _velocity
    (send _velocity :serialization-length)
    ;; bool _is_xy_valid
    1
    ;; bool _is_z_valid
    1
    ;; bool _is_yaw_valid
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Twist _velocity
       (send _velocity :serialize s)
     ;; bool _is_xy_valid
       (if _is_xy_valid (write-byte -1 s) (write-byte 0 s))
     ;; bool _is_z_valid
       (if _is_z_valid (write-byte -1 s) (write-byte 0 s))
     ;; bool _is_yaw_valid
       (if _is_yaw_valid (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Twist _velocity
     (send _velocity :deserialize buf ptr-) (incf ptr- (send _velocity :serialization-length))
   ;; bool _is_xy_valid
     (setq _is_xy_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _is_z_valid
     (setq _is_z_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _is_yaw_valid
     (setq _is_yaw_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get common_msgs::MeasurementVelocity :md5sum-) "1764c5d2ae75808f9fa4aeb5c6990809")
(setf (get common_msgs::MeasurementVelocity :datatype-) "common_msgs/MeasurementVelocity")
(setf (get common_msgs::MeasurementVelocity :definition-)
      "Header header
geometry_msgs/Twist velocity
bool is_xy_valid
bool is_z_valid
bool is_yaw_valid

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
")



(provide :common_msgs/MeasurementVelocity "1764c5d2ae75808f9fa4aeb5c6990809")


