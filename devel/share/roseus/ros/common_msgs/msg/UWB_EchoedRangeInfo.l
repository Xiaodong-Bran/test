;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::UWB_EchoedRangeInfo)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'UWB_EchoedRangeInfo (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::UWB_ECHOEDRANGEINFO")
  (make-package "COMMON_MSGS::UWB_ECHOEDRANGEINFO"))

(in-package "ROS")
;;//! \htmlinclude UWB_EchoedRangeInfo.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::UWB_EchoedRangeInfo
  :super ros::object
  :slots (_header _msgType _msgId _requesterId _responderId _precisionRangeMm _precisionRangeErrEst _ledFlags _timestamp ))

(defmethod common_msgs::UWB_EchoedRangeInfo
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:msgType __msgType) 0)
    ((:msgId __msgId) 0)
    ((:requesterId __requesterId) 0)
    ((:responderId __responderId) 0)
    ((:precisionRangeMm __precisionRangeMm) 0)
    ((:precisionRangeErrEst __precisionRangeErrEst) 0)
    ((:ledFlags __ledFlags) 0)
    ((:timestamp __timestamp) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _msgType (round __msgType))
   (setq _msgId (round __msgId))
   (setq _requesterId (round __requesterId))
   (setq _responderId (round __responderId))
   (setq _precisionRangeMm (round __precisionRangeMm))
   (setq _precisionRangeErrEst (round __precisionRangeErrEst))
   (setq _ledFlags (round __ledFlags))
   (setq _timestamp (round __timestamp))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:msgType
   (&optional __msgType)
   (if __msgType (setq _msgType __msgType)) _msgType)
  (:msgId
   (&optional __msgId)
   (if __msgId (setq _msgId __msgId)) _msgId)
  (:requesterId
   (&optional __requesterId)
   (if __requesterId (setq _requesterId __requesterId)) _requesterId)
  (:responderId
   (&optional __responderId)
   (if __responderId (setq _responderId __responderId)) _responderId)
  (:precisionRangeMm
   (&optional __precisionRangeMm)
   (if __precisionRangeMm (setq _precisionRangeMm __precisionRangeMm)) _precisionRangeMm)
  (:precisionRangeErrEst
   (&optional __precisionRangeErrEst)
   (if __precisionRangeErrEst (setq _precisionRangeErrEst __precisionRangeErrEst)) _precisionRangeErrEst)
  (:ledFlags
   (&optional __ledFlags)
   (if __ledFlags (setq _ledFlags __ledFlags)) _ledFlags)
  (:timestamp
   (&optional __timestamp)
   (if __timestamp (setq _timestamp __timestamp)) _timestamp)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint16 _msgType
    2
    ;; uint16 _msgId
    2
    ;; uint32 _requesterId
    4
    ;; uint32 _responderId
    4
    ;; uint32 _precisionRangeMm
    4
    ;; uint16 _precisionRangeErrEst
    2
    ;; uint16 _ledFlags
    2
    ;; uint32 _timestamp
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint16 _msgType
       (write-word _msgType s)
     ;; uint16 _msgId
       (write-word _msgId s)
     ;; uint32 _requesterId
       (write-long _requesterId s)
     ;; uint32 _responderId
       (write-long _responderId s)
     ;; uint32 _precisionRangeMm
       (write-long _precisionRangeMm s)
     ;; uint16 _precisionRangeErrEst
       (write-word _precisionRangeErrEst s)
     ;; uint16 _ledFlags
       (write-word _ledFlags s)
     ;; uint32 _timestamp
       (write-long _timestamp s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint16 _msgType
     (setq _msgType (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _msgId
     (setq _msgId (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _requesterId
     (setq _requesterId (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _responderId
     (setq _responderId (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _precisionRangeMm
     (setq _precisionRangeMm (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint16 _precisionRangeErrEst
     (setq _precisionRangeErrEst (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _ledFlags
     (setq _ledFlags (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _timestamp
     (setq _timestamp (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get common_msgs::UWB_EchoedRangeInfo :md5sum-) "e88801ba6b3d031989091b69fd886607")
(setf (get common_msgs::UWB_EchoedRangeInfo :datatype-) "common_msgs/UWB_EchoedRangeInfo")
(setf (get common_msgs::UWB_EchoedRangeInfo :definition-)
      "Header header

uint16 msgType
uint16 msgId

uint32 requesterId
uint32 responderId

uint32 precisionRangeMm
uint16 precisionRangeErrEst

uint16 ledFlags
uint32 timestamp

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :common_msgs/UWB_EchoedRangeInfo "e88801ba6b3d031989091b69fd886607")


