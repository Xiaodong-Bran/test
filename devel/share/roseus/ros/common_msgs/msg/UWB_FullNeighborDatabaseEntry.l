;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::UWB_FullNeighborDatabaseEntry)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'UWB_FullNeighborDatabaseEntry (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::UWB_FULLNEIGHBORDATABASEENTRY")
  (make-package "COMMON_MSGS::UWB_FULLNEIGHBORDATABASEENTRY"))

(in-package "ROS")
;;//! \htmlinclude UWB_FullNeighborDatabaseEntry.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::UWB_FullNeighborDatabaseEntry
  :super ros::object
  :slots (_header _nodeId _rangeStatus _antennaMode _stopwatchTime _rangeMm _rangeErrorEstimate _rangeVelocity _rangeMeasurementType _flags _ledFlags _noise _vPeak _statsNumRangeAttempts _statsNumRangeSuccesses _statsAgeMs _rangeUpdateTimestampMs _lastHeardTimestampMs _addedToNDBTimestampMs ))

(defmethod common_msgs::UWB_FullNeighborDatabaseEntry
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:nodeId __nodeId) 0)
    ((:rangeStatus __rangeStatus) 0)
    ((:antennaMode __antennaMode) 0)
    ((:stopwatchTime __stopwatchTime) 0)
    ((:rangeMm __rangeMm) 0)
    ((:rangeErrorEstimate __rangeErrorEstimate) 0)
    ((:rangeVelocity __rangeVelocity) 0)
    ((:rangeMeasurementType __rangeMeasurementType) 0)
    ((:flags __flags) 0)
    ((:ledFlags __ledFlags) 0)
    ((:noise __noise) 0)
    ((:vPeak __vPeak) 0)
    ((:statsNumRangeAttempts __statsNumRangeAttempts) 0)
    ((:statsNumRangeSuccesses __statsNumRangeSuccesses) 0)
    ((:statsAgeMs __statsAgeMs) 0)
    ((:rangeUpdateTimestampMs __rangeUpdateTimestampMs) 0)
    ((:lastHeardTimestampMs __lastHeardTimestampMs) 0)
    ((:addedToNDBTimestampMs __addedToNDBTimestampMs) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _nodeId (round __nodeId))
   (setq _rangeStatus (round __rangeStatus))
   (setq _antennaMode (round __antennaMode))
   (setq _stopwatchTime (round __stopwatchTime))
   (setq _rangeMm (round __rangeMm))
   (setq _rangeErrorEstimate (round __rangeErrorEstimate))
   (setq _rangeVelocity (round __rangeVelocity))
   (setq _rangeMeasurementType (round __rangeMeasurementType))
   (setq _flags (round __flags))
   (setq _ledFlags (round __ledFlags))
   (setq _noise (round __noise))
   (setq _vPeak (round __vPeak))
   (setq _statsNumRangeAttempts (round __statsNumRangeAttempts))
   (setq _statsNumRangeSuccesses (round __statsNumRangeSuccesses))
   (setq _statsAgeMs (round __statsAgeMs))
   (setq _rangeUpdateTimestampMs (round __rangeUpdateTimestampMs))
   (setq _lastHeardTimestampMs (round __lastHeardTimestampMs))
   (setq _addedToNDBTimestampMs (round __addedToNDBTimestampMs))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:nodeId
   (&optional __nodeId)
   (if __nodeId (setq _nodeId __nodeId)) _nodeId)
  (:rangeStatus
   (&optional __rangeStatus)
   (if __rangeStatus (setq _rangeStatus __rangeStatus)) _rangeStatus)
  (:antennaMode
   (&optional __antennaMode)
   (if __antennaMode (setq _antennaMode __antennaMode)) _antennaMode)
  (:stopwatchTime
   (&optional __stopwatchTime)
   (if __stopwatchTime (setq _stopwatchTime __stopwatchTime)) _stopwatchTime)
  (:rangeMm
   (&optional __rangeMm)
   (if __rangeMm (setq _rangeMm __rangeMm)) _rangeMm)
  (:rangeErrorEstimate
   (&optional __rangeErrorEstimate)
   (if __rangeErrorEstimate (setq _rangeErrorEstimate __rangeErrorEstimate)) _rangeErrorEstimate)
  (:rangeVelocity
   (&optional __rangeVelocity)
   (if __rangeVelocity (setq _rangeVelocity __rangeVelocity)) _rangeVelocity)
  (:rangeMeasurementType
   (&optional __rangeMeasurementType)
   (if __rangeMeasurementType (setq _rangeMeasurementType __rangeMeasurementType)) _rangeMeasurementType)
  (:flags
   (&optional __flags)
   (if __flags (setq _flags __flags)) _flags)
  (:ledFlags
   (&optional __ledFlags)
   (if __ledFlags (setq _ledFlags __ledFlags)) _ledFlags)
  (:noise
   (&optional __noise)
   (if __noise (setq _noise __noise)) _noise)
  (:vPeak
   (&optional __vPeak)
   (if __vPeak (setq _vPeak __vPeak)) _vPeak)
  (:statsNumRangeAttempts
   (&optional __statsNumRangeAttempts)
   (if __statsNumRangeAttempts (setq _statsNumRangeAttempts __statsNumRangeAttempts)) _statsNumRangeAttempts)
  (:statsNumRangeSuccesses
   (&optional __statsNumRangeSuccesses)
   (if __statsNumRangeSuccesses (setq _statsNumRangeSuccesses __statsNumRangeSuccesses)) _statsNumRangeSuccesses)
  (:statsAgeMs
   (&optional __statsAgeMs)
   (if __statsAgeMs (setq _statsAgeMs __statsAgeMs)) _statsAgeMs)
  (:rangeUpdateTimestampMs
   (&optional __rangeUpdateTimestampMs)
   (if __rangeUpdateTimestampMs (setq _rangeUpdateTimestampMs __rangeUpdateTimestampMs)) _rangeUpdateTimestampMs)
  (:lastHeardTimestampMs
   (&optional __lastHeardTimestampMs)
   (if __lastHeardTimestampMs (setq _lastHeardTimestampMs __lastHeardTimestampMs)) _lastHeardTimestampMs)
  (:addedToNDBTimestampMs
   (&optional __addedToNDBTimestampMs)
   (if __addedToNDBTimestampMs (setq _addedToNDBTimestampMs __addedToNDBTimestampMs)) _addedToNDBTimestampMs)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint32 _nodeId
    4
    ;; uint8 _rangeStatus
    1
    ;; uint8 _antennaMode
    1
    ;; uint16 _stopwatchTime
    2
    ;; uint32 _rangeMm
    4
    ;; uint16 _rangeErrorEstimate
    2
    ;; uint16 _rangeVelocity
    2
    ;; uint8 _rangeMeasurementType
    1
    ;; uint8 _flags
    1
    ;; uint16 _ledFlags
    2
    ;; uint16 _noise
    2
    ;; uint16 _vPeak
    2
    ;; uint16 _statsNumRangeAttempts
    2
    ;; uint16 _statsNumRangeSuccesses
    2
    ;; uint32 _statsAgeMs
    4
    ;; uint32 _rangeUpdateTimestampMs
    4
    ;; uint32 _lastHeardTimestampMs
    4
    ;; uint32 _addedToNDBTimestampMs
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint32 _nodeId
       (write-long _nodeId s)
     ;; uint8 _rangeStatus
       (write-byte _rangeStatus s)
     ;; uint8 _antennaMode
       (write-byte _antennaMode s)
     ;; uint16 _stopwatchTime
       (write-word _stopwatchTime s)
     ;; uint32 _rangeMm
       (write-long _rangeMm s)
     ;; uint16 _rangeErrorEstimate
       (write-word _rangeErrorEstimate s)
     ;; uint16 _rangeVelocity
       (write-word _rangeVelocity s)
     ;; uint8 _rangeMeasurementType
       (write-byte _rangeMeasurementType s)
     ;; uint8 _flags
       (write-byte _flags s)
     ;; uint16 _ledFlags
       (write-word _ledFlags s)
     ;; uint16 _noise
       (write-word _noise s)
     ;; uint16 _vPeak
       (write-word _vPeak s)
     ;; uint16 _statsNumRangeAttempts
       (write-word _statsNumRangeAttempts s)
     ;; uint16 _statsNumRangeSuccesses
       (write-word _statsNumRangeSuccesses s)
     ;; uint32 _statsAgeMs
       (write-long _statsAgeMs s)
     ;; uint32 _rangeUpdateTimestampMs
       (write-long _rangeUpdateTimestampMs s)
     ;; uint32 _lastHeardTimestampMs
       (write-long _lastHeardTimestampMs s)
     ;; uint32 _addedToNDBTimestampMs
       (write-long _addedToNDBTimestampMs s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint32 _nodeId
     (setq _nodeId (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint8 _rangeStatus
     (setq _rangeStatus (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _antennaMode
     (setq _antennaMode (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _stopwatchTime
     (setq _stopwatchTime (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _rangeMm
     (setq _rangeMm (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint16 _rangeErrorEstimate
     (setq _rangeErrorEstimate (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _rangeVelocity
     (setq _rangeVelocity (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint8 _rangeMeasurementType
     (setq _rangeMeasurementType (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _flags
     (setq _flags (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _ledFlags
     (setq _ledFlags (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _noise
     (setq _noise (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _vPeak
     (setq _vPeak (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _statsNumRangeAttempts
     (setq _statsNumRangeAttempts (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _statsNumRangeSuccesses
     (setq _statsNumRangeSuccesses (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _statsAgeMs
     (setq _statsAgeMs (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _rangeUpdateTimestampMs
     (setq _rangeUpdateTimestampMs (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _lastHeardTimestampMs
     (setq _lastHeardTimestampMs (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _addedToNDBTimestampMs
     (setq _addedToNDBTimestampMs (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get common_msgs::UWB_FullNeighborDatabaseEntry :md5sum-) "b9108662cebcb5984d0800a52513d126")
(setf (get common_msgs::UWB_FullNeighborDatabaseEntry :datatype-) "common_msgs/UWB_FullNeighborDatabaseEntry")
(setf (get common_msgs::UWB_FullNeighborDatabaseEntry :definition-)
      "Header header

uint32 nodeId
uint8 rangeStatus
uint8 antennaMode
uint16 stopwatchTime
uint32 rangeMm
uint16 rangeErrorEstimate
uint16 rangeVelocity
uint8 rangeMeasurementType
uint8 flags
uint16 ledFlags
uint16 noise
uint16 vPeak
uint16 statsNumRangeAttempts
uint16 statsNumRangeSuccesses
uint32 statsAgeMs
uint32 rangeUpdateTimestampMs
uint32 lastHeardTimestampMs
uint32 addedToNDBTimestampMs

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :common_msgs/UWB_FullNeighborDatabaseEntry "b9108662cebcb5984d0800a52513d126")


