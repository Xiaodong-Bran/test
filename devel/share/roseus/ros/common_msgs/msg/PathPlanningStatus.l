;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::PathPlanningStatus)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'PathPlanningStatus (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::PATHPLANNINGSTATUS")
  (make-package "COMMON_MSGS::PATHPLANNINGSTATUS"))

(in-package "ROS")
;;//! \htmlinclude PathPlanningStatus.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::PathPlanningStatus
  :super ros::object
  :slots (_header _is_reached _is_target_reachable _waypoint _alternative_target_position_NWU ))

(defmethod common_msgs::PathPlanningStatus
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:is_reached __is_reached) nil)
    ((:is_target_reachable __is_target_reachable) nil)
    ((:waypoint __waypoint) (instance common_msgs::Waypoint :init))
    ((:alternative_target_position_NWU __alternative_target_position_NWU) (instance geometry_msgs::Pose :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _is_reached __is_reached)
   (setq _is_target_reachable __is_target_reachable)
   (setq _waypoint __waypoint)
   (setq _alternative_target_position_NWU __alternative_target_position_NWU)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:is_reached
   (&optional __is_reached)
   (if __is_reached (setq _is_reached __is_reached)) _is_reached)
  (:is_target_reachable
   (&optional __is_target_reachable)
   (if __is_target_reachable (setq _is_target_reachable __is_target_reachable)) _is_target_reachable)
  (:waypoint
   (&rest __waypoint)
   (if (keywordp (car __waypoint))
       (send* _waypoint __waypoint)
     (progn
       (if __waypoint (setq _waypoint (car __waypoint)))
       _waypoint)))
  (:alternative_target_position_NWU
   (&rest __alternative_target_position_NWU)
   (if (keywordp (car __alternative_target_position_NWU))
       (send* _alternative_target_position_NWU __alternative_target_position_NWU)
     (progn
       (if __alternative_target_position_NWU (setq _alternative_target_position_NWU (car __alternative_target_position_NWU)))
       _alternative_target_position_NWU)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _is_reached
    1
    ;; bool _is_target_reachable
    1
    ;; common_msgs/Waypoint _waypoint
    (send _waypoint :serialization-length)
    ;; geometry_msgs/Pose _alternative_target_position_NWU
    (send _alternative_target_position_NWU :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _is_reached
       (if _is_reached (write-byte -1 s) (write-byte 0 s))
     ;; bool _is_target_reachable
       (if _is_target_reachable (write-byte -1 s) (write-byte 0 s))
     ;; common_msgs/Waypoint _waypoint
       (send _waypoint :serialize s)
     ;; geometry_msgs/Pose _alternative_target_position_NWU
       (send _alternative_target_position_NWU :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _is_reached
     (setq _is_reached (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _is_target_reachable
     (setq _is_target_reachable (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; common_msgs/Waypoint _waypoint
     (send _waypoint :deserialize buf ptr-) (incf ptr- (send _waypoint :serialization-length))
   ;; geometry_msgs/Pose _alternative_target_position_NWU
     (send _alternative_target_position_NWU :deserialize buf ptr-) (incf ptr- (send _alternative_target_position_NWU :serialization-length))
   ;;
   self)
  )

(setf (get common_msgs::PathPlanningStatus :md5sum-) "45bec79eaf000e465a864699c7374601")
(setf (get common_msgs::PathPlanningStatus :datatype-) "common_msgs/PathPlanningStatus")
(setf (get common_msgs::PathPlanningStatus :definition-)
      "Header header
bool is_reached
bool is_target_reachable
common_msgs/Waypoint waypoint
geometry_msgs/Pose alternative_target_position_NWU

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: common_msgs/Waypoint
Header header
common_msgs/NavigationState navigation_state
string m_type
uint32 m_id

================================================================================
MSG: common_msgs/NavigationState
Header header
geometry_msgs/Pose pose
geometry_msgs/Twist velocity
geometry_msgs/Twist acceleration

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
")



(provide :common_msgs/PathPlanningStatus "45bec79eaf000e465a864699c7374601")


