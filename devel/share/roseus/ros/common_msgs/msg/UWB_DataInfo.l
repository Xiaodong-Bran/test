;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::UWB_DataInfo)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'UWB_DataInfo (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::UWB_DATAINFO")
  (make-package "COMMON_MSGS::UWB_DATAINFO"))

(in-package "ROS")
;;//! \htmlinclude UWB_DataInfo.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::UWB_DataInfo
  :super ros::object
  :slots (_header _nodeId _msgType _msgId _sourceId _noise _vPeak _timestamp _antennaId _reserved _dataSize _data ))

(defmethod common_msgs::UWB_DataInfo
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:nodeId __nodeId) 0)
    ((:msgType __msgType) 0)
    ((:msgId __msgId) 0)
    ((:sourceId __sourceId) 0)
    ((:noise __noise) 0)
    ((:vPeak __vPeak) 0)
    ((:timestamp __timestamp) 0)
    ((:antennaId __antennaId) 0)
    ((:reserved __reserved) 0)
    ((:dataSize __dataSize) 0)
    ((:data __data) (make-array 0 :initial-element 0 :element-type :char))
    )
   (send-super :init)
   (setq _header __header)
   (setq _nodeId (round __nodeId))
   (setq _msgType (round __msgType))
   (setq _msgId (round __msgId))
   (setq _sourceId (round __sourceId))
   (setq _noise (round __noise))
   (setq _vPeak (round __vPeak))
   (setq _timestamp (round __timestamp))
   (setq _antennaId (round __antennaId))
   (setq _reserved (round __reserved))
   (setq _dataSize (round __dataSize))
   (setq _data __data)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:nodeId
   (&optional __nodeId)
   (if __nodeId (setq _nodeId __nodeId)) _nodeId)
  (:msgType
   (&optional __msgType)
   (if __msgType (setq _msgType __msgType)) _msgType)
  (:msgId
   (&optional __msgId)
   (if __msgId (setq _msgId __msgId)) _msgId)
  (:sourceId
   (&optional __sourceId)
   (if __sourceId (setq _sourceId __sourceId)) _sourceId)
  (:noise
   (&optional __noise)
   (if __noise (setq _noise __noise)) _noise)
  (:vPeak
   (&optional __vPeak)
   (if __vPeak (setq _vPeak __vPeak)) _vPeak)
  (:timestamp
   (&optional __timestamp)
   (if __timestamp (setq _timestamp __timestamp)) _timestamp)
  (:antennaId
   (&optional __antennaId)
   (if __antennaId (setq _antennaId __antennaId)) _antennaId)
  (:reserved
   (&optional __reserved)
   (if __reserved (setq _reserved __reserved)) _reserved)
  (:dataSize
   (&optional __dataSize)
   (if __dataSize (setq _dataSize __dataSize)) _dataSize)
  (:data
   (&optional __data)
   (if __data (setq _data __data)) _data)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint32 _nodeId
    4
    ;; uint16 _msgType
    2
    ;; uint16 _msgId
    2
    ;; uint32 _sourceId
    4
    ;; uint16 _noise
    2
    ;; uint16 _vPeak
    2
    ;; uint32 _timestamp
    4
    ;; uint8 _antennaId
    1
    ;; uint8 _reserved
    1
    ;; uint16 _dataSize
    2
    ;; uint8[] _data
    (* 1    (length _data)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint32 _nodeId
       (write-long _nodeId s)
     ;; uint16 _msgType
       (write-word _msgType s)
     ;; uint16 _msgId
       (write-word _msgId s)
     ;; uint32 _sourceId
       (write-long _sourceId s)
     ;; uint16 _noise
       (write-word _noise s)
     ;; uint16 _vPeak
       (write-word _vPeak s)
     ;; uint32 _timestamp
       (write-long _timestamp s)
     ;; uint8 _antennaId
       (write-byte _antennaId s)
     ;; uint8 _reserved
       (write-byte _reserved s)
     ;; uint16 _dataSize
       (write-word _dataSize s)
     ;; uint8[] _data
     (write-long (length _data) s)
     (princ _data s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint32 _nodeId
     (setq _nodeId (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint16 _msgType
     (setq _msgType (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _msgId
     (setq _msgId (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _sourceId
     (setq _sourceId (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint16 _noise
     (setq _noise (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _vPeak
     (setq _vPeak (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint32 _timestamp
     (setq _timestamp (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint8 _antennaId
     (setq _antennaId (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _reserved
     (setq _reserved (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _dataSize
     (setq _dataSize (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint8[] _data
   (let ((n (sys::peek buf ptr- :integer))) (incf ptr- 4)
     (setq _data (make-array n :element-type :char))
     (replace _data buf :start2 ptr-) (incf ptr- n))
   ;;
   self)
  )

(setf (get common_msgs::UWB_DataInfo :md5sum-) "5f00cf25f5c037224b4f3c82fda10bf0")
(setf (get common_msgs::UWB_DataInfo :datatype-) "common_msgs/UWB_DataInfo")
(setf (get common_msgs::UWB_DataInfo :definition-)
      "Header header

uint32 nodeId

uint16 msgType
uint16 msgId

uint32 sourceId

uint16 noise
uint16 vPeak

uint32 timestamp

uint8 antennaId
uint8 reserved

uint16 dataSize
uint8[] data

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :common_msgs/UWB_DataInfo "5f00cf25f5c037224b4f3c82fda10bf0")


