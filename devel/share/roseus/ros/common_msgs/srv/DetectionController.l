;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::DetectionController)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'DetectionController (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::DETECTIONCONTROLLER")
  (make-package "COMMON_MSGS::DETECTIONCONTROLLER"))
(unless (find-package "COMMON_MSGS::DETECTIONCONTROLLERREQUEST")
  (make-package "COMMON_MSGS::DETECTIONCONTROLLERREQUEST"))
(unless (find-package "COMMON_MSGS::DETECTIONCONTROLLERRESPONSE")
  (make-package "COMMON_MSGS::DETECTIONCONTROLLERRESPONSE"))

(in-package "ROS")



(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))


(defclass common_msgs::DetectionControllerRequest
  :super ros::object
  :slots (_type _switch_cmd ))

(defmethod common_msgs::DetectionControllerRequest
  (:init
   (&key
    ((:type __type) "")
    ((:switch_cmd __switch_cmd) nil)
    )
   (send-super :init)
   (setq _type (string __type))
   (setq _switch_cmd __switch_cmd)
   self)
  (:type
   (&optional __type)
   (if __type (setq _type __type)) _type)
  (:switch_cmd
   (&optional __switch_cmd)
   (if __switch_cmd (setq _switch_cmd __switch_cmd)) _switch_cmd)
  (:serialization-length
   ()
   (+
    ;; string _type
    4 (length _type)
    ;; bool _switch_cmd
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _type
       (write-long (length _type) s) (princ _type s)
     ;; bool _switch_cmd
       (if _switch_cmd (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _switch_cmd
     (setq _switch_cmd (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass common_msgs::DetectionControllerResponse
  :super ros::object
  :slots (_detect_status _pose _center _corners ))

(defmethod common_msgs::DetectionControllerResponse
  (:init
   (&key
    ((:detect_status __detect_status) 0)
    ((:pose __pose) (instance geometry_msgs::Pose :init))
    ((:center __center) (instance geometry_msgs::Point :init))
    ((:corners __corners) (let (r) (dotimes (i 0) (push (instance geometry_msgs::Point :init) r)) r))
    )
   (send-super :init)
   (setq _detect_status (round __detect_status))
   (setq _pose __pose)
   (setq _center __center)
   (setq _corners __corners)
   self)
  (:detect_status
   (&optional __detect_status)
   (if __detect_status (setq _detect_status __detect_status)) _detect_status)
  (:pose
   (&rest __pose)
   (if (keywordp (car __pose))
       (send* _pose __pose)
     (progn
       (if __pose (setq _pose (car __pose)))
       _pose)))
  (:center
   (&rest __center)
   (if (keywordp (car __center))
       (send* _center __center)
     (progn
       (if __center (setq _center (car __center)))
       _center)))
  (:corners
   (&rest __corners)
   (if (keywordp (car __corners))
       (send* _corners __corners)
     (progn
       (if __corners (setq _corners (car __corners)))
       _corners)))
  (:serialization-length
   ()
   (+
    ;; int32 _detect_status
    4
    ;; geometry_msgs/Pose _pose
    (send _pose :serialization-length)
    ;; geometry_msgs/Point _center
    (send _center :serialization-length)
    ;; geometry_msgs/Point[] _corners
    (apply #'+ (send-all _corners :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _detect_status
       (write-long _detect_status s)
     ;; geometry_msgs/Pose _pose
       (send _pose :serialize s)
     ;; geometry_msgs/Point _center
       (send _center :serialize s)
     ;; geometry_msgs/Point[] _corners
     (write-long (length _corners) s)
     (dolist (elem _corners)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _detect_status
     (setq _detect_status (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; geometry_msgs/Pose _pose
     (send _pose :deserialize buf ptr-) (incf ptr- (send _pose :serialization-length))
   ;; geometry_msgs/Point _center
     (send _center :deserialize buf ptr-) (incf ptr- (send _center :serialization-length))
   ;; geometry_msgs/Point[] _corners
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _corners (let (r) (dotimes (i n) (push (instance geometry_msgs::Point :init) r)) r))
     (dolist (elem- _corners)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(defclass common_msgs::DetectionController
  :super ros::object
  :slots ())

(setf (get common_msgs::DetectionController :md5sum-) "b9a7e1d4bca5e0973c393d8be82dc739")
(setf (get common_msgs::DetectionController :datatype-) "common_msgs/DetectionController")
(setf (get common_msgs::DetectionController :request) common_msgs::DetectionControllerRequest)
(setf (get common_msgs::DetectionController :response) common_msgs::DetectionControllerResponse)

(defmethod common_msgs::DetectionControllerRequest
  (:response () (instance common_msgs::DetectionControllerResponse :init)))

(setf (get common_msgs::DetectionControllerRequest :md5sum-) "b9a7e1d4bca5e0973c393d8be82dc739")
(setf (get common_msgs::DetectionControllerRequest :datatype-) "common_msgs/DetectionControllerRequest")
(setf (get common_msgs::DetectionControllerRequest :definition-)
      "
string type
bool switch_cmd
---

int32 detect_status
geometry_msgs/Pose pose
geometry_msgs/Point center
geometry_msgs/Point[] corners


================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w
")

(setf (get common_msgs::DetectionControllerResponse :md5sum-) "b9a7e1d4bca5e0973c393d8be82dc739")
(setf (get common_msgs::DetectionControllerResponse :datatype-) "common_msgs/DetectionControllerResponse")
(setf (get common_msgs::DetectionControllerResponse :definition-)
      "
string type
bool switch_cmd
---

int32 detect_status
geometry_msgs/Pose pose
geometry_msgs/Point center
geometry_msgs/Point[] corners


================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w
")



(provide :common_msgs/DetectionController "b9a7e1d4bca5e0973c393d8be82dc739")


