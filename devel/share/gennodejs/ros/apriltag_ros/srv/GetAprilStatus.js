// Auto-generated. Do not edit!

// (in-package apriltag_ros.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

let AprilStatus = require('../msg/AprilStatus.js');

//-----------------------------------------------------------

class GetAprilStatusRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.mission_switch = null;
      this.mission_type = null;
    }
    else {
      if (initObj.hasOwnProperty('mission_switch')) {
        this.mission_switch = initObj.mission_switch
      }
      else {
        this.mission_switch = '';
      }
      if (initObj.hasOwnProperty('mission_type')) {
        this.mission_type = initObj.mission_type
      }
      else {
        this.mission_type = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GetAprilStatusRequest
    // Serialize message field [mission_switch]
    bufferOffset = _serializer.string(obj.mission_switch, buffer, bufferOffset);
    // Serialize message field [mission_type]
    bufferOffset = _serializer.string(obj.mission_type, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GetAprilStatusRequest
    let len;
    let data = new GetAprilStatusRequest(null);
    // Deserialize message field [mission_switch]
    data.mission_switch = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [mission_type]
    data.mission_type = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.mission_switch.length;
    length += object.mission_type.length;
    return length + 8;
  }

  static datatype() {
    // Returns string type for a service object
    return 'apriltag_ros/GetAprilStatusRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '75d1d14cccdc95510373e43b6287a0d6';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    string mission_switch
    string mission_type
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GetAprilStatusRequest(null);
    if (msg.mission_switch !== undefined) {
      resolved.mission_switch = msg.mission_switch;
    }
    else {
      resolved.mission_switch = ''
    }

    if (msg.mission_type !== undefined) {
      resolved.mission_type = msg.mission_type;
    }
    else {
      resolved.mission_type = ''
    }

    return resolved;
    }
};

class GetAprilStatusResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.april_status = null;
    }
    else {
      if (initObj.hasOwnProperty('april_status')) {
        this.april_status = initObj.april_status
      }
      else {
        this.april_status = new AprilStatus();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GetAprilStatusResponse
    // Serialize message field [april_status]
    bufferOffset = AprilStatus.serialize(obj.april_status, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GetAprilStatusResponse
    let len;
    let data = new GetAprilStatusResponse(null);
    // Deserialize message field [april_status]
    data.april_status = AprilStatus.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += AprilStatus.getMessageSize(object.april_status);
    return length;
  }

  static datatype() {
    // Returns string type for a service object
    return 'apriltag_ros/GetAprilStatusResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '02feb5a1c95e57de6d58df28e92ffdd7';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    apriltag_ros/AprilStatus april_status
    
    
    ================================================================================
    MSG: apriltag_ros/AprilStatus
    # Apriltag status
    bool is_valid
    string mission_type
    geometry_msgs/Pose pose
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GetAprilStatusResponse(null);
    if (msg.april_status !== undefined) {
      resolved.april_status = AprilStatus.Resolve(msg.april_status)
    }
    else {
      resolved.april_status = new AprilStatus()
    }

    return resolved;
    }
};

module.exports = {
  Request: GetAprilStatusRequest,
  Response: GetAprilStatusResponse,
  md5sum() { return '8b56a804bb0c4311853fc2df1c9bfb2e'; },
  datatype() { return 'apriltag_ros/GetAprilStatus'; }
};
