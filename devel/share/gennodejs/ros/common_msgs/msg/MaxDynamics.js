// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let geometry_msgs = _finder('geometry_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class MaxDynamics {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.vel_max = null;
      this.acc_max = null;
      this.jerk_max = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('vel_max')) {
        this.vel_max = initObj.vel_max
      }
      else {
        this.vel_max = new geometry_msgs.msg.Twist();
      }
      if (initObj.hasOwnProperty('acc_max')) {
        this.acc_max = initObj.acc_max
      }
      else {
        this.acc_max = new geometry_msgs.msg.Twist();
      }
      if (initObj.hasOwnProperty('jerk_max')) {
        this.jerk_max = initObj.jerk_max
      }
      else {
        this.jerk_max = new geometry_msgs.msg.Twist();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MaxDynamics
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [vel_max]
    bufferOffset = geometry_msgs.msg.Twist.serialize(obj.vel_max, buffer, bufferOffset);
    // Serialize message field [acc_max]
    bufferOffset = geometry_msgs.msg.Twist.serialize(obj.acc_max, buffer, bufferOffset);
    // Serialize message field [jerk_max]
    bufferOffset = geometry_msgs.msg.Twist.serialize(obj.jerk_max, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MaxDynamics
    let len;
    let data = new MaxDynamics(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [vel_max]
    data.vel_max = geometry_msgs.msg.Twist.deserialize(buffer, bufferOffset);
    // Deserialize message field [acc_max]
    data.acc_max = geometry_msgs.msg.Twist.deserialize(buffer, bufferOffset);
    // Deserialize message field [jerk_max]
    data.jerk_max = geometry_msgs.msg.Twist.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 144;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/MaxDynamics';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'c3877f5e318c0ad8b9786835e1cba30d';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    geometry_msgs/Twist vel_max
    geometry_msgs/Twist acc_max
    geometry_msgs/Twist jerk_max
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MaxDynamics(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.vel_max !== undefined) {
      resolved.vel_max = geometry_msgs.msg.Twist.Resolve(msg.vel_max)
    }
    else {
      resolved.vel_max = new geometry_msgs.msg.Twist()
    }

    if (msg.acc_max !== undefined) {
      resolved.acc_max = geometry_msgs.msg.Twist.Resolve(msg.acc_max)
    }
    else {
      resolved.acc_max = new geometry_msgs.msg.Twist()
    }

    if (msg.jerk_max !== undefined) {
      resolved.jerk_max = geometry_msgs.msg.Twist.Resolve(msg.jerk_max)
    }
    else {
      resolved.jerk_max = new geometry_msgs.msg.Twist()
    }

    return resolved;
    }
};

module.exports = MaxDynamics;
