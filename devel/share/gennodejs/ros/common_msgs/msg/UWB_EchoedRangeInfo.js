// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class UWB_EchoedRangeInfo {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.msgType = null;
      this.msgId = null;
      this.requesterId = null;
      this.responderId = null;
      this.precisionRangeMm = null;
      this.precisionRangeErrEst = null;
      this.ledFlags = null;
      this.timestamp = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('msgType')) {
        this.msgType = initObj.msgType
      }
      else {
        this.msgType = 0;
      }
      if (initObj.hasOwnProperty('msgId')) {
        this.msgId = initObj.msgId
      }
      else {
        this.msgId = 0;
      }
      if (initObj.hasOwnProperty('requesterId')) {
        this.requesterId = initObj.requesterId
      }
      else {
        this.requesterId = 0;
      }
      if (initObj.hasOwnProperty('responderId')) {
        this.responderId = initObj.responderId
      }
      else {
        this.responderId = 0;
      }
      if (initObj.hasOwnProperty('precisionRangeMm')) {
        this.precisionRangeMm = initObj.precisionRangeMm
      }
      else {
        this.precisionRangeMm = 0;
      }
      if (initObj.hasOwnProperty('precisionRangeErrEst')) {
        this.precisionRangeErrEst = initObj.precisionRangeErrEst
      }
      else {
        this.precisionRangeErrEst = 0;
      }
      if (initObj.hasOwnProperty('ledFlags')) {
        this.ledFlags = initObj.ledFlags
      }
      else {
        this.ledFlags = 0;
      }
      if (initObj.hasOwnProperty('timestamp')) {
        this.timestamp = initObj.timestamp
      }
      else {
        this.timestamp = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type UWB_EchoedRangeInfo
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [msgType]
    bufferOffset = _serializer.uint16(obj.msgType, buffer, bufferOffset);
    // Serialize message field [msgId]
    bufferOffset = _serializer.uint16(obj.msgId, buffer, bufferOffset);
    // Serialize message field [requesterId]
    bufferOffset = _serializer.uint32(obj.requesterId, buffer, bufferOffset);
    // Serialize message field [responderId]
    bufferOffset = _serializer.uint32(obj.responderId, buffer, bufferOffset);
    // Serialize message field [precisionRangeMm]
    bufferOffset = _serializer.uint32(obj.precisionRangeMm, buffer, bufferOffset);
    // Serialize message field [precisionRangeErrEst]
    bufferOffset = _serializer.uint16(obj.precisionRangeErrEst, buffer, bufferOffset);
    // Serialize message field [ledFlags]
    bufferOffset = _serializer.uint16(obj.ledFlags, buffer, bufferOffset);
    // Serialize message field [timestamp]
    bufferOffset = _serializer.uint32(obj.timestamp, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type UWB_EchoedRangeInfo
    let len;
    let data = new UWB_EchoedRangeInfo(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [msgType]
    data.msgType = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [msgId]
    data.msgId = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [requesterId]
    data.requesterId = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [responderId]
    data.responderId = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [precisionRangeMm]
    data.precisionRangeMm = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [precisionRangeErrEst]
    data.precisionRangeErrEst = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [ledFlags]
    data.ledFlags = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [timestamp]
    data.timestamp = _deserializer.uint32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 24;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/UWB_EchoedRangeInfo';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'e88801ba6b3d031989091b69fd886607';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    uint16 msgType
    uint16 msgId
    
    uint32 requesterId
    uint32 responderId
    
    uint32 precisionRangeMm
    uint16 precisionRangeErrEst
    
    uint16 ledFlags
    uint32 timestamp
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new UWB_EchoedRangeInfo(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.msgType !== undefined) {
      resolved.msgType = msg.msgType;
    }
    else {
      resolved.msgType = 0
    }

    if (msg.msgId !== undefined) {
      resolved.msgId = msg.msgId;
    }
    else {
      resolved.msgId = 0
    }

    if (msg.requesterId !== undefined) {
      resolved.requesterId = msg.requesterId;
    }
    else {
      resolved.requesterId = 0
    }

    if (msg.responderId !== undefined) {
      resolved.responderId = msg.responderId;
    }
    else {
      resolved.responderId = 0
    }

    if (msg.precisionRangeMm !== undefined) {
      resolved.precisionRangeMm = msg.precisionRangeMm;
    }
    else {
      resolved.precisionRangeMm = 0
    }

    if (msg.precisionRangeErrEst !== undefined) {
      resolved.precisionRangeErrEst = msg.precisionRangeErrEst;
    }
    else {
      resolved.precisionRangeErrEst = 0
    }

    if (msg.ledFlags !== undefined) {
      resolved.ledFlags = msg.ledFlags;
    }
    else {
      resolved.ledFlags = 0
    }

    if (msg.timestamp !== undefined) {
      resolved.timestamp = msg.timestamp;
    }
    else {
      resolved.timestamp = 0
    }

    return resolved;
    }
};

module.exports = UWB_EchoedRangeInfo;
