// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let NavigationState = require('./NavigationState.js');
let PixhawkCMD = require('./PixhawkCMD.js');
let PixhawkServo = require('./PixhawkServo.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class Reference {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.status = null;
      this.m_id = null;
      this.navigation = null;
      this.pixhawkCMD = null;
      this.pixhawkServo = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('status')) {
        this.status = initObj.status
      }
      else {
        this.status = 0;
      }
      if (initObj.hasOwnProperty('m_id')) {
        this.m_id = initObj.m_id
      }
      else {
        this.m_id = 0;
      }
      if (initObj.hasOwnProperty('navigation')) {
        this.navigation = initObj.navigation
      }
      else {
        this.navigation = new NavigationState();
      }
      if (initObj.hasOwnProperty('pixhawkCMD')) {
        this.pixhawkCMD = initObj.pixhawkCMD
      }
      else {
        this.pixhawkCMD = new PixhawkCMD();
      }
      if (initObj.hasOwnProperty('pixhawkServo')) {
        this.pixhawkServo = initObj.pixhawkServo
      }
      else {
        this.pixhawkServo = new PixhawkServo();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Reference
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [status]
    bufferOffset = _serializer.int32(obj.status, buffer, bufferOffset);
    // Serialize message field [m_id]
    bufferOffset = _serializer.uint32(obj.m_id, buffer, bufferOffset);
    // Serialize message field [navigation]
    bufferOffset = NavigationState.serialize(obj.navigation, buffer, bufferOffset);
    // Serialize message field [pixhawkCMD]
    bufferOffset = PixhawkCMD.serialize(obj.pixhawkCMD, buffer, bufferOffset);
    // Serialize message field [pixhawkServo]
    bufferOffset = PixhawkServo.serialize(obj.pixhawkServo, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Reference
    let len;
    let data = new Reference(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [status]
    data.status = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [m_id]
    data.m_id = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [navigation]
    data.navigation = NavigationState.deserialize(buffer, bufferOffset);
    // Deserialize message field [pixhawkCMD]
    data.pixhawkCMD = PixhawkCMD.deserialize(buffer, bufferOffset);
    // Deserialize message field [pixhawkServo]
    data.pixhawkServo = PixhawkServo.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += NavigationState.getMessageSize(object.navigation);
    length += PixhawkCMD.getMessageSize(object.pixhawkCMD);
    length += PixhawkServo.getMessageSize(object.pixhawkServo);
    return length + 8;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/Reference';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '9eb864d6aea142d4059c163d7c3b3db7';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    int32 status
    uint32 m_id
    NavigationState navigation
    PixhawkCMD pixhawkCMD
    PixhawkServo pixhawkServo
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: common_msgs/NavigationState
    Header header
    geometry_msgs/Pose pose
    geometry_msgs/Twist velocity
    geometry_msgs/Twist acceleration
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    ================================================================================
    MSG: common_msgs/PixhawkCMD
    Header header
    uint8[3] cmd
    
    ================================================================================
    MSG: common_msgs/PixhawkServo
    Header header
    float64[4] servo
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Reference(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.status !== undefined) {
      resolved.status = msg.status;
    }
    else {
      resolved.status = 0
    }

    if (msg.m_id !== undefined) {
      resolved.m_id = msg.m_id;
    }
    else {
      resolved.m_id = 0
    }

    if (msg.navigation !== undefined) {
      resolved.navigation = NavigationState.Resolve(msg.navigation)
    }
    else {
      resolved.navigation = new NavigationState()
    }

    if (msg.pixhawkCMD !== undefined) {
      resolved.pixhawkCMD = PixhawkCMD.Resolve(msg.pixhawkCMD)
    }
    else {
      resolved.pixhawkCMD = new PixhawkCMD()
    }

    if (msg.pixhawkServo !== undefined) {
      resolved.pixhawkServo = PixhawkServo.Resolve(msg.pixhawkServo)
    }
    else {
      resolved.pixhawkServo = new PixhawkServo()
    }

    return resolved;
    }
};

module.exports = Reference;
