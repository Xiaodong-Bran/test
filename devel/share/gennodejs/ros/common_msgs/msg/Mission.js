// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let Waypoint = require('./Waypoint.js');
let PixhawkCMD = require('./PixhawkCMD.js');
let PixhawkServo = require('./PixhawkServo.js');
let sensor_msgs = _finder('sensor_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class Mission {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.m_type = null;
      this.m_id = null;
      this.waypoint = null;
      this.gps_waypoint = null;
      this.pixhawkCMD = null;
      this.pixhawkServo = null;
      this.redundancy = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('m_type')) {
        this.m_type = initObj.m_type
      }
      else {
        this.m_type = '';
      }
      if (initObj.hasOwnProperty('m_id')) {
        this.m_id = initObj.m_id
      }
      else {
        this.m_id = 0;
      }
      if (initObj.hasOwnProperty('waypoint')) {
        this.waypoint = initObj.waypoint
      }
      else {
        this.waypoint = new Waypoint();
      }
      if (initObj.hasOwnProperty('gps_waypoint')) {
        this.gps_waypoint = initObj.gps_waypoint
      }
      else {
        this.gps_waypoint = new sensor_msgs.msg.NavSatFix();
      }
      if (initObj.hasOwnProperty('pixhawkCMD')) {
        this.pixhawkCMD = initObj.pixhawkCMD
      }
      else {
        this.pixhawkCMD = new PixhawkCMD();
      }
      if (initObj.hasOwnProperty('pixhawkServo')) {
        this.pixhawkServo = initObj.pixhawkServo
      }
      else {
        this.pixhawkServo = new PixhawkServo();
      }
      if (initObj.hasOwnProperty('redundancy')) {
        this.redundancy = initObj.redundancy
      }
      else {
        this.redundancy = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Mission
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [m_type]
    bufferOffset = _serializer.string(obj.m_type, buffer, bufferOffset);
    // Serialize message field [m_id]
    bufferOffset = _serializer.uint32(obj.m_id, buffer, bufferOffset);
    // Serialize message field [waypoint]
    bufferOffset = Waypoint.serialize(obj.waypoint, buffer, bufferOffset);
    // Serialize message field [gps_waypoint]
    bufferOffset = sensor_msgs.msg.NavSatFix.serialize(obj.gps_waypoint, buffer, bufferOffset);
    // Serialize message field [pixhawkCMD]
    bufferOffset = PixhawkCMD.serialize(obj.pixhawkCMD, buffer, bufferOffset);
    // Serialize message field [pixhawkServo]
    bufferOffset = PixhawkServo.serialize(obj.pixhawkServo, buffer, bufferOffset);
    // Serialize message field [redundancy]
    bufferOffset = _arraySerializer.float64(obj.redundancy, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Mission
    let len;
    let data = new Mission(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [m_type]
    data.m_type = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [m_id]
    data.m_id = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [waypoint]
    data.waypoint = Waypoint.deserialize(buffer, bufferOffset);
    // Deserialize message field [gps_waypoint]
    data.gps_waypoint = sensor_msgs.msg.NavSatFix.deserialize(buffer, bufferOffset);
    // Deserialize message field [pixhawkCMD]
    data.pixhawkCMD = PixhawkCMD.deserialize(buffer, bufferOffset);
    // Deserialize message field [pixhawkServo]
    data.pixhawkServo = PixhawkServo.deserialize(buffer, bufferOffset);
    // Deserialize message field [redundancy]
    data.redundancy = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += object.m_type.length;
    length += Waypoint.getMessageSize(object.waypoint);
    length += sensor_msgs.msg.NavSatFix.getMessageSize(object.gps_waypoint);
    length += PixhawkCMD.getMessageSize(object.pixhawkCMD);
    length += PixhawkServo.getMessageSize(object.pixhawkServo);
    length += 8 * object.redundancy.length;
    return length + 12;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/Mission';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'c9fcf876b16f31ef90c5d998cf1295bb';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    string m_type
    uint32 m_id
    Waypoint waypoint
    sensor_msgs/NavSatFix gps_waypoint
    PixhawkCMD pixhawkCMD
    PixhawkServo pixhawkServo
    float64[] redundancy
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: common_msgs/Waypoint
    Header header
    common_msgs/NavigationState navigation_state
    string m_type
    uint32 m_id
    
    ================================================================================
    MSG: common_msgs/NavigationState
    Header header
    geometry_msgs/Pose pose
    geometry_msgs/Twist velocity
    geometry_msgs/Twist acceleration
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    ================================================================================
    MSG: sensor_msgs/NavSatFix
    # Navigation Satellite fix for any Global Navigation Satellite System
    #
    # Specified using the WGS 84 reference ellipsoid
    
    # header.stamp specifies the ROS time for this measurement (the
    #        corresponding satellite time may be reported using the
    #        sensor_msgs/TimeReference message).
    #
    # header.frame_id is the frame of reference reported by the satellite
    #        receiver, usually the location of the antenna.  This is a
    #        Euclidean frame relative to the vehicle, not a reference
    #        ellipsoid.
    Header header
    
    # satellite fix status information
    NavSatStatus status
    
    # Latitude [degrees]. Positive is north of equator; negative is south.
    float64 latitude
    
    # Longitude [degrees]. Positive is east of prime meridian; negative is west.
    float64 longitude
    
    # Altitude [m]. Positive is above the WGS 84 ellipsoid
    # (quiet NaN if no altitude is available).
    float64 altitude
    
    # Position covariance [m^2] defined relative to a tangential plane
    # through the reported position. The components are East, North, and
    # Up (ENU), in row-major order.
    #
    # Beware: this coordinate system exhibits singularities at the poles.
    
    float64[9] position_covariance
    
    # If the covariance of the fix is known, fill it in completely. If the
    # GPS receiver provides the variance of each measurement, put them
    # along the diagonal. If only Dilution of Precision is available,
    # estimate an approximate covariance from that.
    
    uint8 COVARIANCE_TYPE_UNKNOWN = 0
    uint8 COVARIANCE_TYPE_APPROXIMATED = 1
    uint8 COVARIANCE_TYPE_DIAGONAL_KNOWN = 2
    uint8 COVARIANCE_TYPE_KNOWN = 3
    
    uint8 position_covariance_type
    
    ================================================================================
    MSG: sensor_msgs/NavSatStatus
    # Navigation Satellite fix status for any Global Navigation Satellite System
    
    # Whether to output an augmented fix is determined by both the fix
    # type and the last time differential corrections were received.  A
    # fix is valid when status >= STATUS_FIX.
    
    int8 STATUS_NO_FIX =  -1        # unable to fix position
    int8 STATUS_FIX =      0        # unaugmented fix
    int8 STATUS_SBAS_FIX = 1        # with satellite-based augmentation
    int8 STATUS_GBAS_FIX = 2        # with ground-based augmentation
    
    int8 status
    
    # Bits defining which Global Navigation Satellite System signals were
    # used by the receiver.
    
    uint16 SERVICE_GPS =     1
    uint16 SERVICE_GLONASS = 2
    uint16 SERVICE_COMPASS = 4      # includes BeiDou.
    uint16 SERVICE_GALILEO = 8
    
    uint16 service
    
    ================================================================================
    MSG: common_msgs/PixhawkCMD
    Header header
    uint8[3] cmd
    
    ================================================================================
    MSG: common_msgs/PixhawkServo
    Header header
    float64[4] servo
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Mission(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.m_type !== undefined) {
      resolved.m_type = msg.m_type;
    }
    else {
      resolved.m_type = ''
    }

    if (msg.m_id !== undefined) {
      resolved.m_id = msg.m_id;
    }
    else {
      resolved.m_id = 0
    }

    if (msg.waypoint !== undefined) {
      resolved.waypoint = Waypoint.Resolve(msg.waypoint)
    }
    else {
      resolved.waypoint = new Waypoint()
    }

    if (msg.gps_waypoint !== undefined) {
      resolved.gps_waypoint = sensor_msgs.msg.NavSatFix.Resolve(msg.gps_waypoint)
    }
    else {
      resolved.gps_waypoint = new sensor_msgs.msg.NavSatFix()
    }

    if (msg.pixhawkCMD !== undefined) {
      resolved.pixhawkCMD = PixhawkCMD.Resolve(msg.pixhawkCMD)
    }
    else {
      resolved.pixhawkCMD = new PixhawkCMD()
    }

    if (msg.pixhawkServo !== undefined) {
      resolved.pixhawkServo = PixhawkServo.Resolve(msg.pixhawkServo)
    }
    else {
      resolved.pixhawkServo = new PixhawkServo()
    }

    if (msg.redundancy !== undefined) {
      resolved.redundancy = msg.redundancy;
    }
    else {
      resolved.redundancy = []
    }

    return resolved;
    }
};

module.exports = Mission;
