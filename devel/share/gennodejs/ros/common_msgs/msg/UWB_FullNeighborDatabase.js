// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let UWB_FullNeighborDatabaseEntry = require('./UWB_FullNeighborDatabaseEntry.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class UWB_FullNeighborDatabase {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.nodeId = null;
      this.msgType = null;
      this.msgId = null;
      this.numNeighborEntries = null;
      this.sortType = null;
      this.reserved = null;
      this.timestamp = null;
      this.status = null;
      this.neighbors = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('nodeId')) {
        this.nodeId = initObj.nodeId
      }
      else {
        this.nodeId = 0;
      }
      if (initObj.hasOwnProperty('msgType')) {
        this.msgType = initObj.msgType
      }
      else {
        this.msgType = 0;
      }
      if (initObj.hasOwnProperty('msgId')) {
        this.msgId = initObj.msgId
      }
      else {
        this.msgId = 0;
      }
      if (initObj.hasOwnProperty('numNeighborEntries')) {
        this.numNeighborEntries = initObj.numNeighborEntries
      }
      else {
        this.numNeighborEntries = 0;
      }
      if (initObj.hasOwnProperty('sortType')) {
        this.sortType = initObj.sortType
      }
      else {
        this.sortType = 0;
      }
      if (initObj.hasOwnProperty('reserved')) {
        this.reserved = initObj.reserved
      }
      else {
        this.reserved = 0;
      }
      if (initObj.hasOwnProperty('timestamp')) {
        this.timestamp = initObj.timestamp
      }
      else {
        this.timestamp = 0;
      }
      if (initObj.hasOwnProperty('status')) {
        this.status = initObj.status
      }
      else {
        this.status = 0;
      }
      if (initObj.hasOwnProperty('neighbors')) {
        this.neighbors = initObj.neighbors
      }
      else {
        this.neighbors = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type UWB_FullNeighborDatabase
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [nodeId]
    bufferOffset = _serializer.uint32(obj.nodeId, buffer, bufferOffset);
    // Serialize message field [msgType]
    bufferOffset = _serializer.uint16(obj.msgType, buffer, bufferOffset);
    // Serialize message field [msgId]
    bufferOffset = _serializer.uint16(obj.msgId, buffer, bufferOffset);
    // Serialize message field [numNeighborEntries]
    bufferOffset = _serializer.uint8(obj.numNeighborEntries, buffer, bufferOffset);
    // Serialize message field [sortType]
    bufferOffset = _serializer.uint8(obj.sortType, buffer, bufferOffset);
    // Serialize message field [reserved]
    bufferOffset = _serializer.uint16(obj.reserved, buffer, bufferOffset);
    // Serialize message field [timestamp]
    bufferOffset = _serializer.uint32(obj.timestamp, buffer, bufferOffset);
    // Serialize message field [status]
    bufferOffset = _serializer.uint32(obj.status, buffer, bufferOffset);
    // Serialize message field [neighbors]
    // Serialize the length for message field [neighbors]
    bufferOffset = _serializer.uint32(obj.neighbors.length, buffer, bufferOffset);
    obj.neighbors.forEach((val) => {
      bufferOffset = UWB_FullNeighborDatabaseEntry.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type UWB_FullNeighborDatabase
    let len;
    let data = new UWB_FullNeighborDatabase(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [nodeId]
    data.nodeId = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [msgType]
    data.msgType = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [msgId]
    data.msgId = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [numNeighborEntries]
    data.numNeighborEntries = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [sortType]
    data.sortType = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [reserved]
    data.reserved = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [timestamp]
    data.timestamp = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [status]
    data.status = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [neighbors]
    // Deserialize array length for message field [neighbors]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.neighbors = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.neighbors[i] = UWB_FullNeighborDatabaseEntry.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    object.neighbors.forEach((val) => {
      length += UWB_FullNeighborDatabaseEntry.getMessageSize(val);
    });
    return length + 24;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/UWB_FullNeighborDatabase';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'e5469a87bded28f455f13fbf487a001b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    uint32 nodeId
    
    uint16 msgType
    uint16 msgId
    uint8 numNeighborEntries
    uint8 sortType
    uint16 reserved
    uint32 timestamp
    uint32 status
    UWB_FullNeighborDatabaseEntry[] neighbors
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: common_msgs/UWB_FullNeighborDatabaseEntry
    Header header
    
    uint32 nodeId
    uint8 rangeStatus
    uint8 antennaMode
    uint16 stopwatchTime
    uint32 rangeMm
    uint16 rangeErrorEstimate
    uint16 rangeVelocity
    uint8 rangeMeasurementType
    uint8 flags
    uint16 ledFlags
    uint16 noise
    uint16 vPeak
    uint16 statsNumRangeAttempts
    uint16 statsNumRangeSuccesses
    uint32 statsAgeMs
    uint32 rangeUpdateTimestampMs
    uint32 lastHeardTimestampMs
    uint32 addedToNDBTimestampMs
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new UWB_FullNeighborDatabase(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.nodeId !== undefined) {
      resolved.nodeId = msg.nodeId;
    }
    else {
      resolved.nodeId = 0
    }

    if (msg.msgType !== undefined) {
      resolved.msgType = msg.msgType;
    }
    else {
      resolved.msgType = 0
    }

    if (msg.msgId !== undefined) {
      resolved.msgId = msg.msgId;
    }
    else {
      resolved.msgId = 0
    }

    if (msg.numNeighborEntries !== undefined) {
      resolved.numNeighborEntries = msg.numNeighborEntries;
    }
    else {
      resolved.numNeighborEntries = 0
    }

    if (msg.sortType !== undefined) {
      resolved.sortType = msg.sortType;
    }
    else {
      resolved.sortType = 0
    }

    if (msg.reserved !== undefined) {
      resolved.reserved = msg.reserved;
    }
    else {
      resolved.reserved = 0
    }

    if (msg.timestamp !== undefined) {
      resolved.timestamp = msg.timestamp;
    }
    else {
      resolved.timestamp = 0
    }

    if (msg.status !== undefined) {
      resolved.status = msg.status;
    }
    else {
      resolved.status = 0
    }

    if (msg.neighbors !== undefined) {
      resolved.neighbors = new Array(msg.neighbors.length);
      for (let i = 0; i < resolved.neighbors.length; ++i) {
        resolved.neighbors[i] = UWB_FullNeighborDatabaseEntry.Resolve(msg.neighbors[i]);
      }
    }
    else {
      resolved.neighbors = []
    }

    return resolved;
    }
};

module.exports = UWB_FullNeighborDatabase;
