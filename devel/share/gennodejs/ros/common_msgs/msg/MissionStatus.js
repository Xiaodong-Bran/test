// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let Mission = require('./Mission.js');
let PathPlanningStatus = require('./PathPlanningStatus.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class MissionStatus {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.mission = null;
      this.pp_status = null;
      this.is_reference_reached = null;
      this.is_measurement_reached = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('mission')) {
        this.mission = initObj.mission
      }
      else {
        this.mission = new Mission();
      }
      if (initObj.hasOwnProperty('pp_status')) {
        this.pp_status = initObj.pp_status
      }
      else {
        this.pp_status = new PathPlanningStatus();
      }
      if (initObj.hasOwnProperty('is_reference_reached')) {
        this.is_reference_reached = initObj.is_reference_reached
      }
      else {
        this.is_reference_reached = false;
      }
      if (initObj.hasOwnProperty('is_measurement_reached')) {
        this.is_measurement_reached = initObj.is_measurement_reached
      }
      else {
        this.is_measurement_reached = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MissionStatus
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [mission]
    bufferOffset = Mission.serialize(obj.mission, buffer, bufferOffset);
    // Serialize message field [pp_status]
    bufferOffset = PathPlanningStatus.serialize(obj.pp_status, buffer, bufferOffset);
    // Serialize message field [is_reference_reached]
    bufferOffset = _serializer.bool(obj.is_reference_reached, buffer, bufferOffset);
    // Serialize message field [is_measurement_reached]
    bufferOffset = _serializer.bool(obj.is_measurement_reached, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MissionStatus
    let len;
    let data = new MissionStatus(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [mission]
    data.mission = Mission.deserialize(buffer, bufferOffset);
    // Deserialize message field [pp_status]
    data.pp_status = PathPlanningStatus.deserialize(buffer, bufferOffset);
    // Deserialize message field [is_reference_reached]
    data.is_reference_reached = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [is_measurement_reached]
    data.is_measurement_reached = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += Mission.getMessageSize(object.mission);
    length += PathPlanningStatus.getMessageSize(object.pp_status);
    return length + 2;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/MissionStatus';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '52a8faff66c070afec63a12e44128dac';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    Mission mission
    PathPlanningStatus pp_status
    bool is_reference_reached
    bool is_measurement_reached
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: common_msgs/Mission
    Header header
    string m_type
    uint32 m_id
    Waypoint waypoint
    sensor_msgs/NavSatFix gps_waypoint
    PixhawkCMD pixhawkCMD
    PixhawkServo pixhawkServo
    float64[] redundancy
    
    ================================================================================
    MSG: common_msgs/Waypoint
    Header header
    common_msgs/NavigationState navigation_state
    string m_type
    uint32 m_id
    
    ================================================================================
    MSG: common_msgs/NavigationState
    Header header
    geometry_msgs/Pose pose
    geometry_msgs/Twist velocity
    geometry_msgs/Twist acceleration
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    ================================================================================
    MSG: sensor_msgs/NavSatFix
    # Navigation Satellite fix for any Global Navigation Satellite System
    #
    # Specified using the WGS 84 reference ellipsoid
    
    # header.stamp specifies the ROS time for this measurement (the
    #        corresponding satellite time may be reported using the
    #        sensor_msgs/TimeReference message).
    #
    # header.frame_id is the frame of reference reported by the satellite
    #        receiver, usually the location of the antenna.  This is a
    #        Euclidean frame relative to the vehicle, not a reference
    #        ellipsoid.
    Header header
    
    # satellite fix status information
    NavSatStatus status
    
    # Latitude [degrees]. Positive is north of equator; negative is south.
    float64 latitude
    
    # Longitude [degrees]. Positive is east of prime meridian; negative is west.
    float64 longitude
    
    # Altitude [m]. Positive is above the WGS 84 ellipsoid
    # (quiet NaN if no altitude is available).
    float64 altitude
    
    # Position covariance [m^2] defined relative to a tangential plane
    # through the reported position. The components are East, North, and
    # Up (ENU), in row-major order.
    #
    # Beware: this coordinate system exhibits singularities at the poles.
    
    float64[9] position_covariance
    
    # If the covariance of the fix is known, fill it in completely. If the
    # GPS receiver provides the variance of each measurement, put them
    # along the diagonal. If only Dilution of Precision is available,
    # estimate an approximate covariance from that.
    
    uint8 COVARIANCE_TYPE_UNKNOWN = 0
    uint8 COVARIANCE_TYPE_APPROXIMATED = 1
    uint8 COVARIANCE_TYPE_DIAGONAL_KNOWN = 2
    uint8 COVARIANCE_TYPE_KNOWN = 3
    
    uint8 position_covariance_type
    
    ================================================================================
    MSG: sensor_msgs/NavSatStatus
    # Navigation Satellite fix status for any Global Navigation Satellite System
    
    # Whether to output an augmented fix is determined by both the fix
    # type and the last time differential corrections were received.  A
    # fix is valid when status >= STATUS_FIX.
    
    int8 STATUS_NO_FIX =  -1        # unable to fix position
    int8 STATUS_FIX =      0        # unaugmented fix
    int8 STATUS_SBAS_FIX = 1        # with satellite-based augmentation
    int8 STATUS_GBAS_FIX = 2        # with ground-based augmentation
    
    int8 status
    
    # Bits defining which Global Navigation Satellite System signals were
    # used by the receiver.
    
    uint16 SERVICE_GPS =     1
    uint16 SERVICE_GLONASS = 2
    uint16 SERVICE_COMPASS = 4      # includes BeiDou.
    uint16 SERVICE_GALILEO = 8
    
    uint16 service
    
    ================================================================================
    MSG: common_msgs/PixhawkCMD
    Header header
    uint8[3] cmd
    
    ================================================================================
    MSG: common_msgs/PixhawkServo
    Header header
    float64[4] servo
    
    ================================================================================
    MSG: common_msgs/PathPlanningStatus
    Header header
    bool is_reached
    bool is_target_reachable
    common_msgs/Waypoint waypoint
    geometry_msgs/Pose alternative_target_position_NWU
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MissionStatus(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.mission !== undefined) {
      resolved.mission = Mission.Resolve(msg.mission)
    }
    else {
      resolved.mission = new Mission()
    }

    if (msg.pp_status !== undefined) {
      resolved.pp_status = PathPlanningStatus.Resolve(msg.pp_status)
    }
    else {
      resolved.pp_status = new PathPlanningStatus()
    }

    if (msg.is_reference_reached !== undefined) {
      resolved.is_reference_reached = msg.is_reference_reached;
    }
    else {
      resolved.is_reference_reached = false
    }

    if (msg.is_measurement_reached !== undefined) {
      resolved.is_measurement_reached = msg.is_measurement_reached;
    }
    else {
      resolved.is_measurement_reached = false
    }

    return resolved;
    }
};

module.exports = MissionStatus;
