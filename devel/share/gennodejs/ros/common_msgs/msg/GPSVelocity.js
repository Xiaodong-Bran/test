// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class GPSVelocity {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.lat_vel = null;
      this.lon_vel = null;
      this.alt_vel = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('lat_vel')) {
        this.lat_vel = initObj.lat_vel
      }
      else {
        this.lat_vel = 0.0;
      }
      if (initObj.hasOwnProperty('lon_vel')) {
        this.lon_vel = initObj.lon_vel
      }
      else {
        this.lon_vel = 0.0;
      }
      if (initObj.hasOwnProperty('alt_vel')) {
        this.alt_vel = initObj.alt_vel
      }
      else {
        this.alt_vel = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GPSVelocity
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [lat_vel]
    bufferOffset = _serializer.float64(obj.lat_vel, buffer, bufferOffset);
    // Serialize message field [lon_vel]
    bufferOffset = _serializer.float64(obj.lon_vel, buffer, bufferOffset);
    // Serialize message field [alt_vel]
    bufferOffset = _serializer.float64(obj.alt_vel, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GPSVelocity
    let len;
    let data = new GPSVelocity(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [lat_vel]
    data.lat_vel = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [lon_vel]
    data.lon_vel = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [alt_vel]
    data.alt_vel = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 24;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/GPSVelocity';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'cd07811c37397d55c764919d4433214e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    float64 lat_vel
    float64 lon_vel
    float64 alt_vel
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GPSVelocity(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.lat_vel !== undefined) {
      resolved.lat_vel = msg.lat_vel;
    }
    else {
      resolved.lat_vel = 0.0
    }

    if (msg.lon_vel !== undefined) {
      resolved.lon_vel = msg.lon_vel;
    }
    else {
      resolved.lon_vel = 0.0
    }

    if (msg.alt_vel !== undefined) {
      resolved.alt_vel = msg.alt_vel;
    }
    else {
      resolved.alt_vel = 0.0
    }

    return resolved;
    }
};

module.exports = GPSVelocity;
