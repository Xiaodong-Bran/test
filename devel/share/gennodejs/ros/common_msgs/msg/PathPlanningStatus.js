// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let Waypoint = require('./Waypoint.js');
let geometry_msgs = _finder('geometry_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class PathPlanningStatus {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.is_reached = null;
      this.is_target_reachable = null;
      this.waypoint = null;
      this.alternative_target_position_NWU = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('is_reached')) {
        this.is_reached = initObj.is_reached
      }
      else {
        this.is_reached = false;
      }
      if (initObj.hasOwnProperty('is_target_reachable')) {
        this.is_target_reachable = initObj.is_target_reachable
      }
      else {
        this.is_target_reachable = false;
      }
      if (initObj.hasOwnProperty('waypoint')) {
        this.waypoint = initObj.waypoint
      }
      else {
        this.waypoint = new Waypoint();
      }
      if (initObj.hasOwnProperty('alternative_target_position_NWU')) {
        this.alternative_target_position_NWU = initObj.alternative_target_position_NWU
      }
      else {
        this.alternative_target_position_NWU = new geometry_msgs.msg.Pose();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PathPlanningStatus
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [is_reached]
    bufferOffset = _serializer.bool(obj.is_reached, buffer, bufferOffset);
    // Serialize message field [is_target_reachable]
    bufferOffset = _serializer.bool(obj.is_target_reachable, buffer, bufferOffset);
    // Serialize message field [waypoint]
    bufferOffset = Waypoint.serialize(obj.waypoint, buffer, bufferOffset);
    // Serialize message field [alternative_target_position_NWU]
    bufferOffset = geometry_msgs.msg.Pose.serialize(obj.alternative_target_position_NWU, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PathPlanningStatus
    let len;
    let data = new PathPlanningStatus(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [is_reached]
    data.is_reached = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [is_target_reachable]
    data.is_target_reachable = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [waypoint]
    data.waypoint = Waypoint.deserialize(buffer, bufferOffset);
    // Deserialize message field [alternative_target_position_NWU]
    data.alternative_target_position_NWU = geometry_msgs.msg.Pose.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += Waypoint.getMessageSize(object.waypoint);
    return length + 58;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/PathPlanningStatus';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '45bec79eaf000e465a864699c7374601';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    bool is_reached
    bool is_target_reachable
    common_msgs/Waypoint waypoint
    geometry_msgs/Pose alternative_target_position_NWU
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: common_msgs/Waypoint
    Header header
    common_msgs/NavigationState navigation_state
    string m_type
    uint32 m_id
    
    ================================================================================
    MSG: common_msgs/NavigationState
    Header header
    geometry_msgs/Pose pose
    geometry_msgs/Twist velocity
    geometry_msgs/Twist acceleration
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PathPlanningStatus(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.is_reached !== undefined) {
      resolved.is_reached = msg.is_reached;
    }
    else {
      resolved.is_reached = false
    }

    if (msg.is_target_reachable !== undefined) {
      resolved.is_target_reachable = msg.is_target_reachable;
    }
    else {
      resolved.is_target_reachable = false
    }

    if (msg.waypoint !== undefined) {
      resolved.waypoint = Waypoint.Resolve(msg.waypoint)
    }
    else {
      resolved.waypoint = new Waypoint()
    }

    if (msg.alternative_target_position_NWU !== undefined) {
      resolved.alternative_target_position_NWU = geometry_msgs.msg.Pose.Resolve(msg.alternative_target_position_NWU)
    }
    else {
      resolved.alternative_target_position_NWU = new geometry_msgs.msg.Pose()
    }

    return resolved;
    }
};

module.exports = PathPlanningStatus;
