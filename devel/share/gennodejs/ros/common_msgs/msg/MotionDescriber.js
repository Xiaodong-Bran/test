// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let Waypoint = require('./Waypoint.js');
let MaxDynamics = require('./MaxDynamics.js');

//-----------------------------------------------------------

class MotionDescriber {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.planned_waypoint = null;
      this.max_dynamics = null;
    }
    else {
      if (initObj.hasOwnProperty('planned_waypoint')) {
        this.planned_waypoint = initObj.planned_waypoint
      }
      else {
        this.planned_waypoint = new Waypoint();
      }
      if (initObj.hasOwnProperty('max_dynamics')) {
        this.max_dynamics = initObj.max_dynamics
      }
      else {
        this.max_dynamics = new MaxDynamics();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MotionDescriber
    // Serialize message field [planned_waypoint]
    bufferOffset = Waypoint.serialize(obj.planned_waypoint, buffer, bufferOffset);
    // Serialize message field [max_dynamics]
    bufferOffset = MaxDynamics.serialize(obj.max_dynamics, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MotionDescriber
    let len;
    let data = new MotionDescriber(null);
    // Deserialize message field [planned_waypoint]
    data.planned_waypoint = Waypoint.deserialize(buffer, bufferOffset);
    // Deserialize message field [max_dynamics]
    data.max_dynamics = MaxDynamics.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += Waypoint.getMessageSize(object.planned_waypoint);
    length += MaxDynamics.getMessageSize(object.max_dynamics);
    return length;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/MotionDescriber';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'bd2b714d2bfb859055e9c10c12ad6ca0';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    common_msgs/Waypoint planned_waypoint
    common_msgs/MaxDynamics max_dynamics
    
    ================================================================================
    MSG: common_msgs/Waypoint
    Header header
    common_msgs/NavigationState navigation_state
    string m_type
    uint32 m_id
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: common_msgs/NavigationState
    Header header
    geometry_msgs/Pose pose
    geometry_msgs/Twist velocity
    geometry_msgs/Twist acceleration
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    ================================================================================
    MSG: common_msgs/MaxDynamics
    Header header
    geometry_msgs/Twist vel_max
    geometry_msgs/Twist acc_max
    geometry_msgs/Twist jerk_max
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MotionDescriber(null);
    if (msg.planned_waypoint !== undefined) {
      resolved.planned_waypoint = Waypoint.Resolve(msg.planned_waypoint)
    }
    else {
      resolved.planned_waypoint = new Waypoint()
    }

    if (msg.max_dynamics !== undefined) {
      resolved.max_dynamics = MaxDynamics.Resolve(msg.max_dynamics)
    }
    else {
      resolved.max_dynamics = new MaxDynamics()
    }

    return resolved;
    }
};

module.exports = MotionDescriber;
