// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let NavigationState = require('./NavigationState.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class Waypoint {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.navigation_state = null;
      this.m_type = null;
      this.m_id = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('navigation_state')) {
        this.navigation_state = initObj.navigation_state
      }
      else {
        this.navigation_state = new NavigationState();
      }
      if (initObj.hasOwnProperty('m_type')) {
        this.m_type = initObj.m_type
      }
      else {
        this.m_type = '';
      }
      if (initObj.hasOwnProperty('m_id')) {
        this.m_id = initObj.m_id
      }
      else {
        this.m_id = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Waypoint
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [navigation_state]
    bufferOffset = NavigationState.serialize(obj.navigation_state, buffer, bufferOffset);
    // Serialize message field [m_type]
    bufferOffset = _serializer.string(obj.m_type, buffer, bufferOffset);
    // Serialize message field [m_id]
    bufferOffset = _serializer.uint32(obj.m_id, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Waypoint
    let len;
    let data = new Waypoint(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [navigation_state]
    data.navigation_state = NavigationState.deserialize(buffer, bufferOffset);
    // Deserialize message field [m_type]
    data.m_type = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [m_id]
    data.m_id = _deserializer.uint32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += NavigationState.getMessageSize(object.navigation_state);
    length += object.m_type.length;
    return length + 8;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/Waypoint';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'cb8be93f3e3ff3f65f66e7f742783b2f';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    common_msgs/NavigationState navigation_state
    string m_type
    uint32 m_id
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: common_msgs/NavigationState
    Header header
    geometry_msgs/Pose pose
    geometry_msgs/Twist velocity
    geometry_msgs/Twist acceleration
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Waypoint(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.navigation_state !== undefined) {
      resolved.navigation_state = NavigationState.Resolve(msg.navigation_state)
    }
    else {
      resolved.navigation_state = new NavigationState()
    }

    if (msg.m_type !== undefined) {
      resolved.m_type = msg.m_type;
    }
    else {
      resolved.m_type = ''
    }

    if (msg.m_id !== undefined) {
      resolved.m_id = msg.m_id;
    }
    else {
      resolved.m_id = 0
    }

    return resolved;
    }
};

module.exports = Waypoint;
