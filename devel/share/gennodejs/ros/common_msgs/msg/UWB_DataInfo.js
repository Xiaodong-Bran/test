// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class UWB_DataInfo {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.nodeId = null;
      this.msgType = null;
      this.msgId = null;
      this.sourceId = null;
      this.noise = null;
      this.vPeak = null;
      this.timestamp = null;
      this.antennaId = null;
      this.reserved = null;
      this.dataSize = null;
      this.data = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('nodeId')) {
        this.nodeId = initObj.nodeId
      }
      else {
        this.nodeId = 0;
      }
      if (initObj.hasOwnProperty('msgType')) {
        this.msgType = initObj.msgType
      }
      else {
        this.msgType = 0;
      }
      if (initObj.hasOwnProperty('msgId')) {
        this.msgId = initObj.msgId
      }
      else {
        this.msgId = 0;
      }
      if (initObj.hasOwnProperty('sourceId')) {
        this.sourceId = initObj.sourceId
      }
      else {
        this.sourceId = 0;
      }
      if (initObj.hasOwnProperty('noise')) {
        this.noise = initObj.noise
      }
      else {
        this.noise = 0;
      }
      if (initObj.hasOwnProperty('vPeak')) {
        this.vPeak = initObj.vPeak
      }
      else {
        this.vPeak = 0;
      }
      if (initObj.hasOwnProperty('timestamp')) {
        this.timestamp = initObj.timestamp
      }
      else {
        this.timestamp = 0;
      }
      if (initObj.hasOwnProperty('antennaId')) {
        this.antennaId = initObj.antennaId
      }
      else {
        this.antennaId = 0;
      }
      if (initObj.hasOwnProperty('reserved')) {
        this.reserved = initObj.reserved
      }
      else {
        this.reserved = 0;
      }
      if (initObj.hasOwnProperty('dataSize')) {
        this.dataSize = initObj.dataSize
      }
      else {
        this.dataSize = 0;
      }
      if (initObj.hasOwnProperty('data')) {
        this.data = initObj.data
      }
      else {
        this.data = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type UWB_DataInfo
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [nodeId]
    bufferOffset = _serializer.uint32(obj.nodeId, buffer, bufferOffset);
    // Serialize message field [msgType]
    bufferOffset = _serializer.uint16(obj.msgType, buffer, bufferOffset);
    // Serialize message field [msgId]
    bufferOffset = _serializer.uint16(obj.msgId, buffer, bufferOffset);
    // Serialize message field [sourceId]
    bufferOffset = _serializer.uint32(obj.sourceId, buffer, bufferOffset);
    // Serialize message field [noise]
    bufferOffset = _serializer.uint16(obj.noise, buffer, bufferOffset);
    // Serialize message field [vPeak]
    bufferOffset = _serializer.uint16(obj.vPeak, buffer, bufferOffset);
    // Serialize message field [timestamp]
    bufferOffset = _serializer.uint32(obj.timestamp, buffer, bufferOffset);
    // Serialize message field [antennaId]
    bufferOffset = _serializer.uint8(obj.antennaId, buffer, bufferOffset);
    // Serialize message field [reserved]
    bufferOffset = _serializer.uint8(obj.reserved, buffer, bufferOffset);
    // Serialize message field [dataSize]
    bufferOffset = _serializer.uint16(obj.dataSize, buffer, bufferOffset);
    // Serialize message field [data]
    bufferOffset = _arraySerializer.uint8(obj.data, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type UWB_DataInfo
    let len;
    let data = new UWB_DataInfo(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [nodeId]
    data.nodeId = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [msgType]
    data.msgType = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [msgId]
    data.msgId = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [sourceId]
    data.sourceId = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [noise]
    data.noise = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [vPeak]
    data.vPeak = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [timestamp]
    data.timestamp = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [antennaId]
    data.antennaId = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [reserved]
    data.reserved = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [dataSize]
    data.dataSize = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [data]
    data.data = _arrayDeserializer.uint8(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += object.data.length;
    return length + 28;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/UWB_DataInfo';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '5f00cf25f5c037224b4f3c82fda10bf0';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    uint32 nodeId
    
    uint16 msgType
    uint16 msgId
    
    uint32 sourceId
    
    uint16 noise
    uint16 vPeak
    
    uint32 timestamp
    
    uint8 antennaId
    uint8 reserved
    
    uint16 dataSize
    uint8[] data
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new UWB_DataInfo(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.nodeId !== undefined) {
      resolved.nodeId = msg.nodeId;
    }
    else {
      resolved.nodeId = 0
    }

    if (msg.msgType !== undefined) {
      resolved.msgType = msg.msgType;
    }
    else {
      resolved.msgType = 0
    }

    if (msg.msgId !== undefined) {
      resolved.msgId = msg.msgId;
    }
    else {
      resolved.msgId = 0
    }

    if (msg.sourceId !== undefined) {
      resolved.sourceId = msg.sourceId;
    }
    else {
      resolved.sourceId = 0
    }

    if (msg.noise !== undefined) {
      resolved.noise = msg.noise;
    }
    else {
      resolved.noise = 0
    }

    if (msg.vPeak !== undefined) {
      resolved.vPeak = msg.vPeak;
    }
    else {
      resolved.vPeak = 0
    }

    if (msg.timestamp !== undefined) {
      resolved.timestamp = msg.timestamp;
    }
    else {
      resolved.timestamp = 0
    }

    if (msg.antennaId !== undefined) {
      resolved.antennaId = msg.antennaId;
    }
    else {
      resolved.antennaId = 0
    }

    if (msg.reserved !== undefined) {
      resolved.reserved = msg.reserved;
    }
    else {
      resolved.reserved = 0
    }

    if (msg.dataSize !== undefined) {
      resolved.dataSize = msg.dataSize;
    }
    else {
      resolved.dataSize = 0
    }

    if (msg.data !== undefined) {
      resolved.data = msg.data;
    }
    else {
      resolved.data = []
    }

    return resolved;
    }
};

module.exports = UWB_DataInfo;
