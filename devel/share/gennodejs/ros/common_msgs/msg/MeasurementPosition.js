// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let geometry_msgs = _finder('geometry_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class MeasurementPosition {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.pose = null;
      this.is_xy_valid = null;
      this.is_z_valid = null;
      this.is_yaw_valid = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('pose')) {
        this.pose = initObj.pose
      }
      else {
        this.pose = new geometry_msgs.msg.Pose();
      }
      if (initObj.hasOwnProperty('is_xy_valid')) {
        this.is_xy_valid = initObj.is_xy_valid
      }
      else {
        this.is_xy_valid = false;
      }
      if (initObj.hasOwnProperty('is_z_valid')) {
        this.is_z_valid = initObj.is_z_valid
      }
      else {
        this.is_z_valid = false;
      }
      if (initObj.hasOwnProperty('is_yaw_valid')) {
        this.is_yaw_valid = initObj.is_yaw_valid
      }
      else {
        this.is_yaw_valid = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MeasurementPosition
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [pose]
    bufferOffset = geometry_msgs.msg.Pose.serialize(obj.pose, buffer, bufferOffset);
    // Serialize message field [is_xy_valid]
    bufferOffset = _serializer.bool(obj.is_xy_valid, buffer, bufferOffset);
    // Serialize message field [is_z_valid]
    bufferOffset = _serializer.bool(obj.is_z_valid, buffer, bufferOffset);
    // Serialize message field [is_yaw_valid]
    bufferOffset = _serializer.bool(obj.is_yaw_valid, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MeasurementPosition
    let len;
    let data = new MeasurementPosition(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [pose]
    data.pose = geometry_msgs.msg.Pose.deserialize(buffer, bufferOffset);
    // Deserialize message field [is_xy_valid]
    data.is_xy_valid = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [is_z_valid]
    data.is_z_valid = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [is_yaw_valid]
    data.is_yaw_valid = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 59;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/MeasurementPosition';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '49fef8348ffe3fc1ef41fbe01ce739a3';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    geometry_msgs/Pose pose
    bool is_xy_valid
    bool is_z_valid
    bool is_yaw_valid
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MeasurementPosition(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.pose !== undefined) {
      resolved.pose = geometry_msgs.msg.Pose.Resolve(msg.pose)
    }
    else {
      resolved.pose = new geometry_msgs.msg.Pose()
    }

    if (msg.is_xy_valid !== undefined) {
      resolved.is_xy_valid = msg.is_xy_valid;
    }
    else {
      resolved.is_xy_valid = false
    }

    if (msg.is_z_valid !== undefined) {
      resolved.is_z_valid = msg.is_z_valid;
    }
    else {
      resolved.is_z_valid = false
    }

    if (msg.is_yaw_valid !== undefined) {
      resolved.is_yaw_valid = msg.is_yaw_valid;
    }
    else {
      resolved.is_yaw_valid = false
    }

    return resolved;
    }
};

module.exports = MeasurementPosition;
