// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let geometry_msgs = _finder('geometry_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class MeasurementVelocity {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.velocity = null;
      this.is_xy_valid = null;
      this.is_z_valid = null;
      this.is_yaw_valid = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('velocity')) {
        this.velocity = initObj.velocity
      }
      else {
        this.velocity = new geometry_msgs.msg.Twist();
      }
      if (initObj.hasOwnProperty('is_xy_valid')) {
        this.is_xy_valid = initObj.is_xy_valid
      }
      else {
        this.is_xy_valid = false;
      }
      if (initObj.hasOwnProperty('is_z_valid')) {
        this.is_z_valid = initObj.is_z_valid
      }
      else {
        this.is_z_valid = false;
      }
      if (initObj.hasOwnProperty('is_yaw_valid')) {
        this.is_yaw_valid = initObj.is_yaw_valid
      }
      else {
        this.is_yaw_valid = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MeasurementVelocity
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [velocity]
    bufferOffset = geometry_msgs.msg.Twist.serialize(obj.velocity, buffer, bufferOffset);
    // Serialize message field [is_xy_valid]
    bufferOffset = _serializer.bool(obj.is_xy_valid, buffer, bufferOffset);
    // Serialize message field [is_z_valid]
    bufferOffset = _serializer.bool(obj.is_z_valid, buffer, bufferOffset);
    // Serialize message field [is_yaw_valid]
    bufferOffset = _serializer.bool(obj.is_yaw_valid, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MeasurementVelocity
    let len;
    let data = new MeasurementVelocity(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [velocity]
    data.velocity = geometry_msgs.msg.Twist.deserialize(buffer, bufferOffset);
    // Deserialize message field [is_xy_valid]
    data.is_xy_valid = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [is_z_valid]
    data.is_z_valid = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [is_yaw_valid]
    data.is_yaw_valid = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 51;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/MeasurementVelocity';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '1764c5d2ae75808f9fa4aeb5c6990809';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    geometry_msgs/Twist velocity
    bool is_xy_valid
    bool is_z_valid
    bool is_yaw_valid
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MeasurementVelocity(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.velocity !== undefined) {
      resolved.velocity = geometry_msgs.msg.Twist.Resolve(msg.velocity)
    }
    else {
      resolved.velocity = new geometry_msgs.msg.Twist()
    }

    if (msg.is_xy_valid !== undefined) {
      resolved.is_xy_valid = msg.is_xy_valid;
    }
    else {
      resolved.is_xy_valid = false
    }

    if (msg.is_z_valid !== undefined) {
      resolved.is_z_valid = msg.is_z_valid;
    }
    else {
      resolved.is_z_valid = false
    }

    if (msg.is_yaw_valid !== undefined) {
      resolved.is_yaw_valid = msg.is_yaw_valid;
    }
    else {
      resolved.is_yaw_valid = false
    }

    return resolved;
    }
};

module.exports = MeasurementVelocity;
