// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class UWB_FullNeighborDatabaseEntry {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.nodeId = null;
      this.rangeStatus = null;
      this.antennaMode = null;
      this.stopwatchTime = null;
      this.rangeMm = null;
      this.rangeErrorEstimate = null;
      this.rangeVelocity = null;
      this.rangeMeasurementType = null;
      this.flags = null;
      this.ledFlags = null;
      this.noise = null;
      this.vPeak = null;
      this.statsNumRangeAttempts = null;
      this.statsNumRangeSuccesses = null;
      this.statsAgeMs = null;
      this.rangeUpdateTimestampMs = null;
      this.lastHeardTimestampMs = null;
      this.addedToNDBTimestampMs = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('nodeId')) {
        this.nodeId = initObj.nodeId
      }
      else {
        this.nodeId = 0;
      }
      if (initObj.hasOwnProperty('rangeStatus')) {
        this.rangeStatus = initObj.rangeStatus
      }
      else {
        this.rangeStatus = 0;
      }
      if (initObj.hasOwnProperty('antennaMode')) {
        this.antennaMode = initObj.antennaMode
      }
      else {
        this.antennaMode = 0;
      }
      if (initObj.hasOwnProperty('stopwatchTime')) {
        this.stopwatchTime = initObj.stopwatchTime
      }
      else {
        this.stopwatchTime = 0;
      }
      if (initObj.hasOwnProperty('rangeMm')) {
        this.rangeMm = initObj.rangeMm
      }
      else {
        this.rangeMm = 0;
      }
      if (initObj.hasOwnProperty('rangeErrorEstimate')) {
        this.rangeErrorEstimate = initObj.rangeErrorEstimate
      }
      else {
        this.rangeErrorEstimate = 0;
      }
      if (initObj.hasOwnProperty('rangeVelocity')) {
        this.rangeVelocity = initObj.rangeVelocity
      }
      else {
        this.rangeVelocity = 0;
      }
      if (initObj.hasOwnProperty('rangeMeasurementType')) {
        this.rangeMeasurementType = initObj.rangeMeasurementType
      }
      else {
        this.rangeMeasurementType = 0;
      }
      if (initObj.hasOwnProperty('flags')) {
        this.flags = initObj.flags
      }
      else {
        this.flags = 0;
      }
      if (initObj.hasOwnProperty('ledFlags')) {
        this.ledFlags = initObj.ledFlags
      }
      else {
        this.ledFlags = 0;
      }
      if (initObj.hasOwnProperty('noise')) {
        this.noise = initObj.noise
      }
      else {
        this.noise = 0;
      }
      if (initObj.hasOwnProperty('vPeak')) {
        this.vPeak = initObj.vPeak
      }
      else {
        this.vPeak = 0;
      }
      if (initObj.hasOwnProperty('statsNumRangeAttempts')) {
        this.statsNumRangeAttempts = initObj.statsNumRangeAttempts
      }
      else {
        this.statsNumRangeAttempts = 0;
      }
      if (initObj.hasOwnProperty('statsNumRangeSuccesses')) {
        this.statsNumRangeSuccesses = initObj.statsNumRangeSuccesses
      }
      else {
        this.statsNumRangeSuccesses = 0;
      }
      if (initObj.hasOwnProperty('statsAgeMs')) {
        this.statsAgeMs = initObj.statsAgeMs
      }
      else {
        this.statsAgeMs = 0;
      }
      if (initObj.hasOwnProperty('rangeUpdateTimestampMs')) {
        this.rangeUpdateTimestampMs = initObj.rangeUpdateTimestampMs
      }
      else {
        this.rangeUpdateTimestampMs = 0;
      }
      if (initObj.hasOwnProperty('lastHeardTimestampMs')) {
        this.lastHeardTimestampMs = initObj.lastHeardTimestampMs
      }
      else {
        this.lastHeardTimestampMs = 0;
      }
      if (initObj.hasOwnProperty('addedToNDBTimestampMs')) {
        this.addedToNDBTimestampMs = initObj.addedToNDBTimestampMs
      }
      else {
        this.addedToNDBTimestampMs = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type UWB_FullNeighborDatabaseEntry
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [nodeId]
    bufferOffset = _serializer.uint32(obj.nodeId, buffer, bufferOffset);
    // Serialize message field [rangeStatus]
    bufferOffset = _serializer.uint8(obj.rangeStatus, buffer, bufferOffset);
    // Serialize message field [antennaMode]
    bufferOffset = _serializer.uint8(obj.antennaMode, buffer, bufferOffset);
    // Serialize message field [stopwatchTime]
    bufferOffset = _serializer.uint16(obj.stopwatchTime, buffer, bufferOffset);
    // Serialize message field [rangeMm]
    bufferOffset = _serializer.uint32(obj.rangeMm, buffer, bufferOffset);
    // Serialize message field [rangeErrorEstimate]
    bufferOffset = _serializer.uint16(obj.rangeErrorEstimate, buffer, bufferOffset);
    // Serialize message field [rangeVelocity]
    bufferOffset = _serializer.uint16(obj.rangeVelocity, buffer, bufferOffset);
    // Serialize message field [rangeMeasurementType]
    bufferOffset = _serializer.uint8(obj.rangeMeasurementType, buffer, bufferOffset);
    // Serialize message field [flags]
    bufferOffset = _serializer.uint8(obj.flags, buffer, bufferOffset);
    // Serialize message field [ledFlags]
    bufferOffset = _serializer.uint16(obj.ledFlags, buffer, bufferOffset);
    // Serialize message field [noise]
    bufferOffset = _serializer.uint16(obj.noise, buffer, bufferOffset);
    // Serialize message field [vPeak]
    bufferOffset = _serializer.uint16(obj.vPeak, buffer, bufferOffset);
    // Serialize message field [statsNumRangeAttempts]
    bufferOffset = _serializer.uint16(obj.statsNumRangeAttempts, buffer, bufferOffset);
    // Serialize message field [statsNumRangeSuccesses]
    bufferOffset = _serializer.uint16(obj.statsNumRangeSuccesses, buffer, bufferOffset);
    // Serialize message field [statsAgeMs]
    bufferOffset = _serializer.uint32(obj.statsAgeMs, buffer, bufferOffset);
    // Serialize message field [rangeUpdateTimestampMs]
    bufferOffset = _serializer.uint32(obj.rangeUpdateTimestampMs, buffer, bufferOffset);
    // Serialize message field [lastHeardTimestampMs]
    bufferOffset = _serializer.uint32(obj.lastHeardTimestampMs, buffer, bufferOffset);
    // Serialize message field [addedToNDBTimestampMs]
    bufferOffset = _serializer.uint32(obj.addedToNDBTimestampMs, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type UWB_FullNeighborDatabaseEntry
    let len;
    let data = new UWB_FullNeighborDatabaseEntry(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [nodeId]
    data.nodeId = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [rangeStatus]
    data.rangeStatus = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [antennaMode]
    data.antennaMode = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [stopwatchTime]
    data.stopwatchTime = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [rangeMm]
    data.rangeMm = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [rangeErrorEstimate]
    data.rangeErrorEstimate = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [rangeVelocity]
    data.rangeVelocity = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [rangeMeasurementType]
    data.rangeMeasurementType = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [flags]
    data.flags = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [ledFlags]
    data.ledFlags = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [noise]
    data.noise = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [vPeak]
    data.vPeak = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [statsNumRangeAttempts]
    data.statsNumRangeAttempts = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [statsNumRangeSuccesses]
    data.statsNumRangeSuccesses = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [statsAgeMs]
    data.statsAgeMs = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [rangeUpdateTimestampMs]
    data.rangeUpdateTimestampMs = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [lastHeardTimestampMs]
    data.lastHeardTimestampMs = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [addedToNDBTimestampMs]
    data.addedToNDBTimestampMs = _deserializer.uint32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 44;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/UWB_FullNeighborDatabaseEntry';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'b9108662cebcb5984d0800a52513d126';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    uint32 nodeId
    uint8 rangeStatus
    uint8 antennaMode
    uint16 stopwatchTime
    uint32 rangeMm
    uint16 rangeErrorEstimate
    uint16 rangeVelocity
    uint8 rangeMeasurementType
    uint8 flags
    uint16 ledFlags
    uint16 noise
    uint16 vPeak
    uint16 statsNumRangeAttempts
    uint16 statsNumRangeSuccesses
    uint32 statsAgeMs
    uint32 rangeUpdateTimestampMs
    uint32 lastHeardTimestampMs
    uint32 addedToNDBTimestampMs
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new UWB_FullNeighborDatabaseEntry(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.nodeId !== undefined) {
      resolved.nodeId = msg.nodeId;
    }
    else {
      resolved.nodeId = 0
    }

    if (msg.rangeStatus !== undefined) {
      resolved.rangeStatus = msg.rangeStatus;
    }
    else {
      resolved.rangeStatus = 0
    }

    if (msg.antennaMode !== undefined) {
      resolved.antennaMode = msg.antennaMode;
    }
    else {
      resolved.antennaMode = 0
    }

    if (msg.stopwatchTime !== undefined) {
      resolved.stopwatchTime = msg.stopwatchTime;
    }
    else {
      resolved.stopwatchTime = 0
    }

    if (msg.rangeMm !== undefined) {
      resolved.rangeMm = msg.rangeMm;
    }
    else {
      resolved.rangeMm = 0
    }

    if (msg.rangeErrorEstimate !== undefined) {
      resolved.rangeErrorEstimate = msg.rangeErrorEstimate;
    }
    else {
      resolved.rangeErrorEstimate = 0
    }

    if (msg.rangeVelocity !== undefined) {
      resolved.rangeVelocity = msg.rangeVelocity;
    }
    else {
      resolved.rangeVelocity = 0
    }

    if (msg.rangeMeasurementType !== undefined) {
      resolved.rangeMeasurementType = msg.rangeMeasurementType;
    }
    else {
      resolved.rangeMeasurementType = 0
    }

    if (msg.flags !== undefined) {
      resolved.flags = msg.flags;
    }
    else {
      resolved.flags = 0
    }

    if (msg.ledFlags !== undefined) {
      resolved.ledFlags = msg.ledFlags;
    }
    else {
      resolved.ledFlags = 0
    }

    if (msg.noise !== undefined) {
      resolved.noise = msg.noise;
    }
    else {
      resolved.noise = 0
    }

    if (msg.vPeak !== undefined) {
      resolved.vPeak = msg.vPeak;
    }
    else {
      resolved.vPeak = 0
    }

    if (msg.statsNumRangeAttempts !== undefined) {
      resolved.statsNumRangeAttempts = msg.statsNumRangeAttempts;
    }
    else {
      resolved.statsNumRangeAttempts = 0
    }

    if (msg.statsNumRangeSuccesses !== undefined) {
      resolved.statsNumRangeSuccesses = msg.statsNumRangeSuccesses;
    }
    else {
      resolved.statsNumRangeSuccesses = 0
    }

    if (msg.statsAgeMs !== undefined) {
      resolved.statsAgeMs = msg.statsAgeMs;
    }
    else {
      resolved.statsAgeMs = 0
    }

    if (msg.rangeUpdateTimestampMs !== undefined) {
      resolved.rangeUpdateTimestampMs = msg.rangeUpdateTimestampMs;
    }
    else {
      resolved.rangeUpdateTimestampMs = 0
    }

    if (msg.lastHeardTimestampMs !== undefined) {
      resolved.lastHeardTimestampMs = msg.lastHeardTimestampMs;
    }
    else {
      resolved.lastHeardTimestampMs = 0
    }

    if (msg.addedToNDBTimestampMs !== undefined) {
      resolved.addedToNDBTimestampMs = msg.addedToNDBTimestampMs;
    }
    else {
      resolved.addedToNDBTimestampMs = 0
    }

    return resolved;
    }
};

module.exports = UWB_FullNeighborDatabaseEntry;
