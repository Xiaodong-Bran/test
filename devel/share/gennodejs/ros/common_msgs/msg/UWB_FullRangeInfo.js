// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class UWB_FullRangeInfo {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.nodeId = null;
      this.msgType = null;
      this.msgId = null;
      this.responderId = null;
      this.rangeStatus = null;
      this.antennaMode = null;
      this.stopwatchTime = null;
      this.precisionRangeMm = null;
      this.coarseRangeMm = null;
      this.filteredRangeMm = null;
      this.precisionRangeErrEst = null;
      this.coarseRangeErrEst = null;
      this.filteredRangeErrEst = null;
      this.filteredRangeVel = null;
      this.filteredRangeVelErrEst = null;
      this.rangeMeasurementType = null;
      this.reserved = null;
      this.reqLEDFlags = null;
      this.respLEDFlags = null;
      this.noise = null;
      this.vPeak = null;
      this.coarseTOFInBins = null;
      this.timestamp = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('nodeId')) {
        this.nodeId = initObj.nodeId
      }
      else {
        this.nodeId = 0;
      }
      if (initObj.hasOwnProperty('msgType')) {
        this.msgType = initObj.msgType
      }
      else {
        this.msgType = 0;
      }
      if (initObj.hasOwnProperty('msgId')) {
        this.msgId = initObj.msgId
      }
      else {
        this.msgId = 0;
      }
      if (initObj.hasOwnProperty('responderId')) {
        this.responderId = initObj.responderId
      }
      else {
        this.responderId = 0;
      }
      if (initObj.hasOwnProperty('rangeStatus')) {
        this.rangeStatus = initObj.rangeStatus
      }
      else {
        this.rangeStatus = 0;
      }
      if (initObj.hasOwnProperty('antennaMode')) {
        this.antennaMode = initObj.antennaMode
      }
      else {
        this.antennaMode = 0;
      }
      if (initObj.hasOwnProperty('stopwatchTime')) {
        this.stopwatchTime = initObj.stopwatchTime
      }
      else {
        this.stopwatchTime = 0;
      }
      if (initObj.hasOwnProperty('precisionRangeMm')) {
        this.precisionRangeMm = initObj.precisionRangeMm
      }
      else {
        this.precisionRangeMm = 0;
      }
      if (initObj.hasOwnProperty('coarseRangeMm')) {
        this.coarseRangeMm = initObj.coarseRangeMm
      }
      else {
        this.coarseRangeMm = 0;
      }
      if (initObj.hasOwnProperty('filteredRangeMm')) {
        this.filteredRangeMm = initObj.filteredRangeMm
      }
      else {
        this.filteredRangeMm = 0;
      }
      if (initObj.hasOwnProperty('precisionRangeErrEst')) {
        this.precisionRangeErrEst = initObj.precisionRangeErrEst
      }
      else {
        this.precisionRangeErrEst = 0;
      }
      if (initObj.hasOwnProperty('coarseRangeErrEst')) {
        this.coarseRangeErrEst = initObj.coarseRangeErrEst
      }
      else {
        this.coarseRangeErrEst = 0;
      }
      if (initObj.hasOwnProperty('filteredRangeErrEst')) {
        this.filteredRangeErrEst = initObj.filteredRangeErrEst
      }
      else {
        this.filteredRangeErrEst = 0;
      }
      if (initObj.hasOwnProperty('filteredRangeVel')) {
        this.filteredRangeVel = initObj.filteredRangeVel
      }
      else {
        this.filteredRangeVel = 0;
      }
      if (initObj.hasOwnProperty('filteredRangeVelErrEst')) {
        this.filteredRangeVelErrEst = initObj.filteredRangeVelErrEst
      }
      else {
        this.filteredRangeVelErrEst = 0;
      }
      if (initObj.hasOwnProperty('rangeMeasurementType')) {
        this.rangeMeasurementType = initObj.rangeMeasurementType
      }
      else {
        this.rangeMeasurementType = 0;
      }
      if (initObj.hasOwnProperty('reserved')) {
        this.reserved = initObj.reserved
      }
      else {
        this.reserved = 0;
      }
      if (initObj.hasOwnProperty('reqLEDFlags')) {
        this.reqLEDFlags = initObj.reqLEDFlags
      }
      else {
        this.reqLEDFlags = 0;
      }
      if (initObj.hasOwnProperty('respLEDFlags')) {
        this.respLEDFlags = initObj.respLEDFlags
      }
      else {
        this.respLEDFlags = 0;
      }
      if (initObj.hasOwnProperty('noise')) {
        this.noise = initObj.noise
      }
      else {
        this.noise = 0;
      }
      if (initObj.hasOwnProperty('vPeak')) {
        this.vPeak = initObj.vPeak
      }
      else {
        this.vPeak = 0;
      }
      if (initObj.hasOwnProperty('coarseTOFInBins')) {
        this.coarseTOFInBins = initObj.coarseTOFInBins
      }
      else {
        this.coarseTOFInBins = 0;
      }
      if (initObj.hasOwnProperty('timestamp')) {
        this.timestamp = initObj.timestamp
      }
      else {
        this.timestamp = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type UWB_FullRangeInfo
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [nodeId]
    bufferOffset = _serializer.uint32(obj.nodeId, buffer, bufferOffset);
    // Serialize message field [msgType]
    bufferOffset = _serializer.uint16(obj.msgType, buffer, bufferOffset);
    // Serialize message field [msgId]
    bufferOffset = _serializer.uint16(obj.msgId, buffer, bufferOffset);
    // Serialize message field [responderId]
    bufferOffset = _serializer.uint32(obj.responderId, buffer, bufferOffset);
    // Serialize message field [rangeStatus]
    bufferOffset = _serializer.uint8(obj.rangeStatus, buffer, bufferOffset);
    // Serialize message field [antennaMode]
    bufferOffset = _serializer.uint8(obj.antennaMode, buffer, bufferOffset);
    // Serialize message field [stopwatchTime]
    bufferOffset = _serializer.uint16(obj.stopwatchTime, buffer, bufferOffset);
    // Serialize message field [precisionRangeMm]
    bufferOffset = _serializer.uint32(obj.precisionRangeMm, buffer, bufferOffset);
    // Serialize message field [coarseRangeMm]
    bufferOffset = _serializer.uint32(obj.coarseRangeMm, buffer, bufferOffset);
    // Serialize message field [filteredRangeMm]
    bufferOffset = _serializer.uint32(obj.filteredRangeMm, buffer, bufferOffset);
    // Serialize message field [precisionRangeErrEst]
    bufferOffset = _serializer.uint16(obj.precisionRangeErrEst, buffer, bufferOffset);
    // Serialize message field [coarseRangeErrEst]
    bufferOffset = _serializer.uint16(obj.coarseRangeErrEst, buffer, bufferOffset);
    // Serialize message field [filteredRangeErrEst]
    bufferOffset = _serializer.uint16(obj.filteredRangeErrEst, buffer, bufferOffset);
    // Serialize message field [filteredRangeVel]
    bufferOffset = _serializer.uint16(obj.filteredRangeVel, buffer, bufferOffset);
    // Serialize message field [filteredRangeVelErrEst]
    bufferOffset = _serializer.uint16(obj.filteredRangeVelErrEst, buffer, bufferOffset);
    // Serialize message field [rangeMeasurementType]
    bufferOffset = _serializer.uint8(obj.rangeMeasurementType, buffer, bufferOffset);
    // Serialize message field [reserved]
    bufferOffset = _serializer.uint8(obj.reserved, buffer, bufferOffset);
    // Serialize message field [reqLEDFlags]
    bufferOffset = _serializer.uint16(obj.reqLEDFlags, buffer, bufferOffset);
    // Serialize message field [respLEDFlags]
    bufferOffset = _serializer.uint16(obj.respLEDFlags, buffer, bufferOffset);
    // Serialize message field [noise]
    bufferOffset = _serializer.uint16(obj.noise, buffer, bufferOffset);
    // Serialize message field [vPeak]
    bufferOffset = _serializer.uint16(obj.vPeak, buffer, bufferOffset);
    // Serialize message field [coarseTOFInBins]
    bufferOffset = _serializer.uint32(obj.coarseTOFInBins, buffer, bufferOffset);
    // Serialize message field [timestamp]
    bufferOffset = _serializer.uint32(obj.timestamp, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type UWB_FullRangeInfo
    let len;
    let data = new UWB_FullRangeInfo(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [nodeId]
    data.nodeId = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [msgType]
    data.msgType = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [msgId]
    data.msgId = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [responderId]
    data.responderId = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [rangeStatus]
    data.rangeStatus = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [antennaMode]
    data.antennaMode = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [stopwatchTime]
    data.stopwatchTime = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [precisionRangeMm]
    data.precisionRangeMm = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [coarseRangeMm]
    data.coarseRangeMm = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [filteredRangeMm]
    data.filteredRangeMm = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [precisionRangeErrEst]
    data.precisionRangeErrEst = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [coarseRangeErrEst]
    data.coarseRangeErrEst = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [filteredRangeErrEst]
    data.filteredRangeErrEst = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [filteredRangeVel]
    data.filteredRangeVel = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [filteredRangeVelErrEst]
    data.filteredRangeVelErrEst = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [rangeMeasurementType]
    data.rangeMeasurementType = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [reserved]
    data.reserved = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [reqLEDFlags]
    data.reqLEDFlags = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [respLEDFlags]
    data.respLEDFlags = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [noise]
    data.noise = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [vPeak]
    data.vPeak = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [coarseTOFInBins]
    data.coarseTOFInBins = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [timestamp]
    data.timestamp = _deserializer.uint32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 56;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/UWB_FullRangeInfo';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '6eed76e0c8b91a8a7fb2c268438b77be';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    uint32 nodeId
    
    uint16 msgType
    uint16 msgId
    
    uint32 responderId
    
    uint8 rangeStatus
    uint8 antennaMode
    uint16 stopwatchTime
    
    uint32 precisionRangeMm
    uint32 coarseRangeMm
    uint32 filteredRangeMm
    
    uint16 precisionRangeErrEst
    uint16 coarseRangeErrEst
    uint16 filteredRangeErrEst
    
    uint16 filteredRangeVel
    uint16 filteredRangeVelErrEst
    
    uint8 rangeMeasurementType
    uint8 reserved
    
    uint16 reqLEDFlags
    uint16 respLEDFlags
    
    uint16 noise
    uint16 vPeak
    
    uint32 coarseTOFInBins
    
    uint32 timestamp
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new UWB_FullRangeInfo(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.nodeId !== undefined) {
      resolved.nodeId = msg.nodeId;
    }
    else {
      resolved.nodeId = 0
    }

    if (msg.msgType !== undefined) {
      resolved.msgType = msg.msgType;
    }
    else {
      resolved.msgType = 0
    }

    if (msg.msgId !== undefined) {
      resolved.msgId = msg.msgId;
    }
    else {
      resolved.msgId = 0
    }

    if (msg.responderId !== undefined) {
      resolved.responderId = msg.responderId;
    }
    else {
      resolved.responderId = 0
    }

    if (msg.rangeStatus !== undefined) {
      resolved.rangeStatus = msg.rangeStatus;
    }
    else {
      resolved.rangeStatus = 0
    }

    if (msg.antennaMode !== undefined) {
      resolved.antennaMode = msg.antennaMode;
    }
    else {
      resolved.antennaMode = 0
    }

    if (msg.stopwatchTime !== undefined) {
      resolved.stopwatchTime = msg.stopwatchTime;
    }
    else {
      resolved.stopwatchTime = 0
    }

    if (msg.precisionRangeMm !== undefined) {
      resolved.precisionRangeMm = msg.precisionRangeMm;
    }
    else {
      resolved.precisionRangeMm = 0
    }

    if (msg.coarseRangeMm !== undefined) {
      resolved.coarseRangeMm = msg.coarseRangeMm;
    }
    else {
      resolved.coarseRangeMm = 0
    }

    if (msg.filteredRangeMm !== undefined) {
      resolved.filteredRangeMm = msg.filteredRangeMm;
    }
    else {
      resolved.filteredRangeMm = 0
    }

    if (msg.precisionRangeErrEst !== undefined) {
      resolved.precisionRangeErrEst = msg.precisionRangeErrEst;
    }
    else {
      resolved.precisionRangeErrEst = 0
    }

    if (msg.coarseRangeErrEst !== undefined) {
      resolved.coarseRangeErrEst = msg.coarseRangeErrEst;
    }
    else {
      resolved.coarseRangeErrEst = 0
    }

    if (msg.filteredRangeErrEst !== undefined) {
      resolved.filteredRangeErrEst = msg.filteredRangeErrEst;
    }
    else {
      resolved.filteredRangeErrEst = 0
    }

    if (msg.filteredRangeVel !== undefined) {
      resolved.filteredRangeVel = msg.filteredRangeVel;
    }
    else {
      resolved.filteredRangeVel = 0
    }

    if (msg.filteredRangeVelErrEst !== undefined) {
      resolved.filteredRangeVelErrEst = msg.filteredRangeVelErrEst;
    }
    else {
      resolved.filteredRangeVelErrEst = 0
    }

    if (msg.rangeMeasurementType !== undefined) {
      resolved.rangeMeasurementType = msg.rangeMeasurementType;
    }
    else {
      resolved.rangeMeasurementType = 0
    }

    if (msg.reserved !== undefined) {
      resolved.reserved = msg.reserved;
    }
    else {
      resolved.reserved = 0
    }

    if (msg.reqLEDFlags !== undefined) {
      resolved.reqLEDFlags = msg.reqLEDFlags;
    }
    else {
      resolved.reqLEDFlags = 0
    }

    if (msg.respLEDFlags !== undefined) {
      resolved.respLEDFlags = msg.respLEDFlags;
    }
    else {
      resolved.respLEDFlags = 0
    }

    if (msg.noise !== undefined) {
      resolved.noise = msg.noise;
    }
    else {
      resolved.noise = 0
    }

    if (msg.vPeak !== undefined) {
      resolved.vPeak = msg.vPeak;
    }
    else {
      resolved.vPeak = 0
    }

    if (msg.coarseTOFInBins !== undefined) {
      resolved.coarseTOFInBins = msg.coarseTOFInBins;
    }
    else {
      resolved.coarseTOFInBins = 0
    }

    if (msg.timestamp !== undefined) {
      resolved.timestamp = msg.timestamp;
    }
    else {
      resolved.timestamp = 0
    }

    return resolved;
    }
};

module.exports = UWB_FullRangeInfo;
