; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude MeasurementPosition.msg.html

(cl:defclass <MeasurementPosition> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (pose
    :reader pose
    :initarg :pose
    :type geometry_msgs-msg:Pose
    :initform (cl:make-instance 'geometry_msgs-msg:Pose))
   (is_xy_valid
    :reader is_xy_valid
    :initarg :is_xy_valid
    :type cl:boolean
    :initform cl:nil)
   (is_z_valid
    :reader is_z_valid
    :initarg :is_z_valid
    :type cl:boolean
    :initform cl:nil)
   (is_yaw_valid
    :reader is_yaw_valid
    :initarg :is_yaw_valid
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass MeasurementPosition (<MeasurementPosition>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MeasurementPosition>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MeasurementPosition)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<MeasurementPosition> is deprecated: use common_msgs-msg:MeasurementPosition instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MeasurementPosition>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'pose-val :lambda-list '(m))
(cl:defmethod pose-val ((m <MeasurementPosition>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:pose-val is deprecated.  Use common_msgs-msg:pose instead.")
  (pose m))

(cl:ensure-generic-function 'is_xy_valid-val :lambda-list '(m))
(cl:defmethod is_xy_valid-val ((m <MeasurementPosition>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:is_xy_valid-val is deprecated.  Use common_msgs-msg:is_xy_valid instead.")
  (is_xy_valid m))

(cl:ensure-generic-function 'is_z_valid-val :lambda-list '(m))
(cl:defmethod is_z_valid-val ((m <MeasurementPosition>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:is_z_valid-val is deprecated.  Use common_msgs-msg:is_z_valid instead.")
  (is_z_valid m))

(cl:ensure-generic-function 'is_yaw_valid-val :lambda-list '(m))
(cl:defmethod is_yaw_valid-val ((m <MeasurementPosition>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:is_yaw_valid-val is deprecated.  Use common_msgs-msg:is_yaw_valid instead.")
  (is_yaw_valid m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MeasurementPosition>) ostream)
  "Serializes a message object of type '<MeasurementPosition>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'pose) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_xy_valid) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_z_valid) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_yaw_valid) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MeasurementPosition>) istream)
  "Deserializes a message object of type '<MeasurementPosition>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'pose) istream)
    (cl:setf (cl:slot-value msg 'is_xy_valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'is_z_valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'is_yaw_valid) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MeasurementPosition>)))
  "Returns string type for a message object of type '<MeasurementPosition>"
  "common_msgs/MeasurementPosition")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MeasurementPosition)))
  "Returns string type for a message object of type 'MeasurementPosition"
  "common_msgs/MeasurementPosition")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MeasurementPosition>)))
  "Returns md5sum for a message object of type '<MeasurementPosition>"
  "49fef8348ffe3fc1ef41fbe01ce739a3")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MeasurementPosition)))
  "Returns md5sum for a message object of type 'MeasurementPosition"
  "49fef8348ffe3fc1ef41fbe01ce739a3")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MeasurementPosition>)))
  "Returns full string definition for message of type '<MeasurementPosition>"
  (cl:format cl:nil "Header header~%geometry_msgs/Pose pose~%bool is_xy_valid~%bool is_z_valid~%bool is_yaw_valid~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MeasurementPosition)))
  "Returns full string definition for message of type 'MeasurementPosition"
  (cl:format cl:nil "Header header~%geometry_msgs/Pose pose~%bool is_xy_valid~%bool is_z_valid~%bool is_yaw_valid~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MeasurementPosition>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'pose))
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MeasurementPosition>))
  "Converts a ROS message object to a list"
  (cl:list 'MeasurementPosition
    (cl:cons ':header (header msg))
    (cl:cons ':pose (pose msg))
    (cl:cons ':is_xy_valid (is_xy_valid msg))
    (cl:cons ':is_z_valid (is_z_valid msg))
    (cl:cons ':is_yaw_valid (is_yaw_valid msg))
))
