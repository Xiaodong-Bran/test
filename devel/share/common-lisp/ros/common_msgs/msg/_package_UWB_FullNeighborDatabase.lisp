(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          NODEID-VAL
          NODEID
          MSGTYPE-VAL
          MSGTYPE
          MSGID-VAL
          MSGID
          NUMNEIGHBORENTRIES-VAL
          NUMNEIGHBORENTRIES
          SORTTYPE-VAL
          SORTTYPE
          RESERVED-VAL
          RESERVED
          TIMESTAMP-VAL
          TIMESTAMP
          STATUS-VAL
          STATUS
          NEIGHBORS-VAL
          NEIGHBORS
))