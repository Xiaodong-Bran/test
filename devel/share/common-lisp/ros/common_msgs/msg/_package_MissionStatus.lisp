(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          MISSION-VAL
          MISSION
          PP_STATUS-VAL
          PP_STATUS
          IS_REFERENCE_REACHED-VAL
          IS_REFERENCE_REACHED
          IS_MEASUREMENT_REACHED-VAL
          IS_MEASUREMENT_REACHED
))