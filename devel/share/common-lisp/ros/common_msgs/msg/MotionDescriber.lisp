; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude MotionDescriber.msg.html

(cl:defclass <MotionDescriber> (roslisp-msg-protocol:ros-message)
  ((planned_waypoint
    :reader planned_waypoint
    :initarg :planned_waypoint
    :type common_msgs-msg:Waypoint
    :initform (cl:make-instance 'common_msgs-msg:Waypoint))
   (max_dynamics
    :reader max_dynamics
    :initarg :max_dynamics
    :type common_msgs-msg:MaxDynamics
    :initform (cl:make-instance 'common_msgs-msg:MaxDynamics)))
)

(cl:defclass MotionDescriber (<MotionDescriber>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MotionDescriber>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MotionDescriber)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<MotionDescriber> is deprecated: use common_msgs-msg:MotionDescriber instead.")))

(cl:ensure-generic-function 'planned_waypoint-val :lambda-list '(m))
(cl:defmethod planned_waypoint-val ((m <MotionDescriber>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:planned_waypoint-val is deprecated.  Use common_msgs-msg:planned_waypoint instead.")
  (planned_waypoint m))

(cl:ensure-generic-function 'max_dynamics-val :lambda-list '(m))
(cl:defmethod max_dynamics-val ((m <MotionDescriber>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:max_dynamics-val is deprecated.  Use common_msgs-msg:max_dynamics instead.")
  (max_dynamics m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MotionDescriber>) ostream)
  "Serializes a message object of type '<MotionDescriber>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'planned_waypoint) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'max_dynamics) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MotionDescriber>) istream)
  "Deserializes a message object of type '<MotionDescriber>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'planned_waypoint) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'max_dynamics) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MotionDescriber>)))
  "Returns string type for a message object of type '<MotionDescriber>"
  "common_msgs/MotionDescriber")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MotionDescriber)))
  "Returns string type for a message object of type 'MotionDescriber"
  "common_msgs/MotionDescriber")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MotionDescriber>)))
  "Returns md5sum for a message object of type '<MotionDescriber>"
  "bd2b714d2bfb859055e9c10c12ad6ca0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MotionDescriber)))
  "Returns md5sum for a message object of type 'MotionDescriber"
  "bd2b714d2bfb859055e9c10c12ad6ca0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MotionDescriber>)))
  "Returns full string definition for message of type '<MotionDescriber>"
  (cl:format cl:nil "common_msgs/Waypoint planned_waypoint~%common_msgs/MaxDynamics max_dynamics~%~%================================================================================~%MSG: common_msgs/Waypoint~%Header header~%common_msgs/NavigationState navigation_state~%string m_type~%uint32 m_id~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: common_msgs/NavigationState~%Header header~%geometry_msgs/Pose pose~%geometry_msgs/Twist velocity~%geometry_msgs/Twist acceleration~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: common_msgs/MaxDynamics~%Header header~%geometry_msgs/Twist vel_max~%geometry_msgs/Twist acc_max~%geometry_msgs/Twist jerk_max~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MotionDescriber)))
  "Returns full string definition for message of type 'MotionDescriber"
  (cl:format cl:nil "common_msgs/Waypoint planned_waypoint~%common_msgs/MaxDynamics max_dynamics~%~%================================================================================~%MSG: common_msgs/Waypoint~%Header header~%common_msgs/NavigationState navigation_state~%string m_type~%uint32 m_id~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: common_msgs/NavigationState~%Header header~%geometry_msgs/Pose pose~%geometry_msgs/Twist velocity~%geometry_msgs/Twist acceleration~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: common_msgs/MaxDynamics~%Header header~%geometry_msgs/Twist vel_max~%geometry_msgs/Twist acc_max~%geometry_msgs/Twist jerk_max~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MotionDescriber>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'planned_waypoint))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'max_dynamics))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MotionDescriber>))
  "Converts a ROS message object to a list"
  (cl:list 'MotionDescriber
    (cl:cons ':planned_waypoint (planned_waypoint msg))
    (cl:cons ':max_dynamics (max_dynamics msg))
))
