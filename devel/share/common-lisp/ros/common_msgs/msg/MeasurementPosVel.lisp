; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude MeasurementPosVel.msg.html

(cl:defclass <MeasurementPosVel> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (position
    :reader position
    :initarg :position
    :type common_msgs-msg:MeasurementPosition
    :initform (cl:make-instance 'common_msgs-msg:MeasurementPosition))
   (velocity
    :reader velocity
    :initarg :velocity
    :type common_msgs-msg:MeasurementVelocity
    :initform (cl:make-instance 'common_msgs-msg:MeasurementVelocity)))
)

(cl:defclass MeasurementPosVel (<MeasurementPosVel>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MeasurementPosVel>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MeasurementPosVel)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<MeasurementPosVel> is deprecated: use common_msgs-msg:MeasurementPosVel instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MeasurementPosVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <MeasurementPosVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:position-val is deprecated.  Use common_msgs-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'velocity-val :lambda-list '(m))
(cl:defmethod velocity-val ((m <MeasurementPosVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:velocity-val is deprecated.  Use common_msgs-msg:velocity instead.")
  (velocity m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MeasurementPosVel>) ostream)
  "Serializes a message object of type '<MeasurementPosVel>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'position) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'velocity) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MeasurementPosVel>) istream)
  "Deserializes a message object of type '<MeasurementPosVel>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'position) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'velocity) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MeasurementPosVel>)))
  "Returns string type for a message object of type '<MeasurementPosVel>"
  "common_msgs/MeasurementPosVel")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MeasurementPosVel)))
  "Returns string type for a message object of type 'MeasurementPosVel"
  "common_msgs/MeasurementPosVel")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MeasurementPosVel>)))
  "Returns md5sum for a message object of type '<MeasurementPosVel>"
  "9357e8ec18526a1fb373c79a69f060dd")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MeasurementPosVel)))
  "Returns md5sum for a message object of type 'MeasurementPosVel"
  "9357e8ec18526a1fb373c79a69f060dd")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MeasurementPosVel>)))
  "Returns full string definition for message of type '<MeasurementPosVel>"
  (cl:format cl:nil "Header header~%MeasurementPosition position~%MeasurementVelocity velocity~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: common_msgs/MeasurementPosition~%Header header~%geometry_msgs/Pose pose~%bool is_xy_valid~%bool is_z_valid~%bool is_yaw_valid~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: common_msgs/MeasurementVelocity~%Header header~%geometry_msgs/Twist velocity~%bool is_xy_valid~%bool is_z_valid~%bool is_yaw_valid~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MeasurementPosVel)))
  "Returns full string definition for message of type 'MeasurementPosVel"
  (cl:format cl:nil "Header header~%MeasurementPosition position~%MeasurementVelocity velocity~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: common_msgs/MeasurementPosition~%Header header~%geometry_msgs/Pose pose~%bool is_xy_valid~%bool is_z_valid~%bool is_yaw_valid~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: common_msgs/MeasurementVelocity~%Header header~%geometry_msgs/Twist velocity~%bool is_xy_valid~%bool is_z_valid~%bool is_yaw_valid~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MeasurementPosVel>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'position))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'velocity))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MeasurementPosVel>))
  "Converts a ROS message object to a list"
  (cl:list 'MeasurementPosVel
    (cl:cons ':header (header msg))
    (cl:cons ':position (position msg))
    (cl:cons ':velocity (velocity msg))
))
