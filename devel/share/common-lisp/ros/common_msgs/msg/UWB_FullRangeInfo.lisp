; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude UWB_FullRangeInfo.msg.html

(cl:defclass <UWB_FullRangeInfo> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (nodeId
    :reader nodeId
    :initarg :nodeId
    :type cl:integer
    :initform 0)
   (msgType
    :reader msgType
    :initarg :msgType
    :type cl:fixnum
    :initform 0)
   (msgId
    :reader msgId
    :initarg :msgId
    :type cl:fixnum
    :initform 0)
   (responderId
    :reader responderId
    :initarg :responderId
    :type cl:integer
    :initform 0)
   (rangeStatus
    :reader rangeStatus
    :initarg :rangeStatus
    :type cl:fixnum
    :initform 0)
   (antennaMode
    :reader antennaMode
    :initarg :antennaMode
    :type cl:fixnum
    :initform 0)
   (stopwatchTime
    :reader stopwatchTime
    :initarg :stopwatchTime
    :type cl:fixnum
    :initform 0)
   (precisionRangeMm
    :reader precisionRangeMm
    :initarg :precisionRangeMm
    :type cl:integer
    :initform 0)
   (coarseRangeMm
    :reader coarseRangeMm
    :initarg :coarseRangeMm
    :type cl:integer
    :initform 0)
   (filteredRangeMm
    :reader filteredRangeMm
    :initarg :filteredRangeMm
    :type cl:integer
    :initform 0)
   (precisionRangeErrEst
    :reader precisionRangeErrEst
    :initarg :precisionRangeErrEst
    :type cl:fixnum
    :initform 0)
   (coarseRangeErrEst
    :reader coarseRangeErrEst
    :initarg :coarseRangeErrEst
    :type cl:fixnum
    :initform 0)
   (filteredRangeErrEst
    :reader filteredRangeErrEst
    :initarg :filteredRangeErrEst
    :type cl:fixnum
    :initform 0)
   (filteredRangeVel
    :reader filteredRangeVel
    :initarg :filteredRangeVel
    :type cl:fixnum
    :initform 0)
   (filteredRangeVelErrEst
    :reader filteredRangeVelErrEst
    :initarg :filteredRangeVelErrEst
    :type cl:fixnum
    :initform 0)
   (rangeMeasurementType
    :reader rangeMeasurementType
    :initarg :rangeMeasurementType
    :type cl:fixnum
    :initform 0)
   (reserved
    :reader reserved
    :initarg :reserved
    :type cl:fixnum
    :initform 0)
   (reqLEDFlags
    :reader reqLEDFlags
    :initarg :reqLEDFlags
    :type cl:fixnum
    :initform 0)
   (respLEDFlags
    :reader respLEDFlags
    :initarg :respLEDFlags
    :type cl:fixnum
    :initform 0)
   (noise
    :reader noise
    :initarg :noise
    :type cl:fixnum
    :initform 0)
   (vPeak
    :reader vPeak
    :initarg :vPeak
    :type cl:fixnum
    :initform 0)
   (coarseTOFInBins
    :reader coarseTOFInBins
    :initarg :coarseTOFInBins
    :type cl:integer
    :initform 0)
   (timestamp
    :reader timestamp
    :initarg :timestamp
    :type cl:integer
    :initform 0))
)

(cl:defclass UWB_FullRangeInfo (<UWB_FullRangeInfo>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <UWB_FullRangeInfo>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'UWB_FullRangeInfo)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<UWB_FullRangeInfo> is deprecated: use common_msgs-msg:UWB_FullRangeInfo instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'nodeId-val :lambda-list '(m))
(cl:defmethod nodeId-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:nodeId-val is deprecated.  Use common_msgs-msg:nodeId instead.")
  (nodeId m))

(cl:ensure-generic-function 'msgType-val :lambda-list '(m))
(cl:defmethod msgType-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:msgType-val is deprecated.  Use common_msgs-msg:msgType instead.")
  (msgType m))

(cl:ensure-generic-function 'msgId-val :lambda-list '(m))
(cl:defmethod msgId-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:msgId-val is deprecated.  Use common_msgs-msg:msgId instead.")
  (msgId m))

(cl:ensure-generic-function 'responderId-val :lambda-list '(m))
(cl:defmethod responderId-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:responderId-val is deprecated.  Use common_msgs-msg:responderId instead.")
  (responderId m))

(cl:ensure-generic-function 'rangeStatus-val :lambda-list '(m))
(cl:defmethod rangeStatus-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:rangeStatus-val is deprecated.  Use common_msgs-msg:rangeStatus instead.")
  (rangeStatus m))

(cl:ensure-generic-function 'antennaMode-val :lambda-list '(m))
(cl:defmethod antennaMode-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:antennaMode-val is deprecated.  Use common_msgs-msg:antennaMode instead.")
  (antennaMode m))

(cl:ensure-generic-function 'stopwatchTime-val :lambda-list '(m))
(cl:defmethod stopwatchTime-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:stopwatchTime-val is deprecated.  Use common_msgs-msg:stopwatchTime instead.")
  (stopwatchTime m))

(cl:ensure-generic-function 'precisionRangeMm-val :lambda-list '(m))
(cl:defmethod precisionRangeMm-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:precisionRangeMm-val is deprecated.  Use common_msgs-msg:precisionRangeMm instead.")
  (precisionRangeMm m))

(cl:ensure-generic-function 'coarseRangeMm-val :lambda-list '(m))
(cl:defmethod coarseRangeMm-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:coarseRangeMm-val is deprecated.  Use common_msgs-msg:coarseRangeMm instead.")
  (coarseRangeMm m))

(cl:ensure-generic-function 'filteredRangeMm-val :lambda-list '(m))
(cl:defmethod filteredRangeMm-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:filteredRangeMm-val is deprecated.  Use common_msgs-msg:filteredRangeMm instead.")
  (filteredRangeMm m))

(cl:ensure-generic-function 'precisionRangeErrEst-val :lambda-list '(m))
(cl:defmethod precisionRangeErrEst-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:precisionRangeErrEst-val is deprecated.  Use common_msgs-msg:precisionRangeErrEst instead.")
  (precisionRangeErrEst m))

(cl:ensure-generic-function 'coarseRangeErrEst-val :lambda-list '(m))
(cl:defmethod coarseRangeErrEst-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:coarseRangeErrEst-val is deprecated.  Use common_msgs-msg:coarseRangeErrEst instead.")
  (coarseRangeErrEst m))

(cl:ensure-generic-function 'filteredRangeErrEst-val :lambda-list '(m))
(cl:defmethod filteredRangeErrEst-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:filteredRangeErrEst-val is deprecated.  Use common_msgs-msg:filteredRangeErrEst instead.")
  (filteredRangeErrEst m))

(cl:ensure-generic-function 'filteredRangeVel-val :lambda-list '(m))
(cl:defmethod filteredRangeVel-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:filteredRangeVel-val is deprecated.  Use common_msgs-msg:filteredRangeVel instead.")
  (filteredRangeVel m))

(cl:ensure-generic-function 'filteredRangeVelErrEst-val :lambda-list '(m))
(cl:defmethod filteredRangeVelErrEst-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:filteredRangeVelErrEst-val is deprecated.  Use common_msgs-msg:filteredRangeVelErrEst instead.")
  (filteredRangeVelErrEst m))

(cl:ensure-generic-function 'rangeMeasurementType-val :lambda-list '(m))
(cl:defmethod rangeMeasurementType-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:rangeMeasurementType-val is deprecated.  Use common_msgs-msg:rangeMeasurementType instead.")
  (rangeMeasurementType m))

(cl:ensure-generic-function 'reserved-val :lambda-list '(m))
(cl:defmethod reserved-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:reserved-val is deprecated.  Use common_msgs-msg:reserved instead.")
  (reserved m))

(cl:ensure-generic-function 'reqLEDFlags-val :lambda-list '(m))
(cl:defmethod reqLEDFlags-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:reqLEDFlags-val is deprecated.  Use common_msgs-msg:reqLEDFlags instead.")
  (reqLEDFlags m))

(cl:ensure-generic-function 'respLEDFlags-val :lambda-list '(m))
(cl:defmethod respLEDFlags-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:respLEDFlags-val is deprecated.  Use common_msgs-msg:respLEDFlags instead.")
  (respLEDFlags m))

(cl:ensure-generic-function 'noise-val :lambda-list '(m))
(cl:defmethod noise-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:noise-val is deprecated.  Use common_msgs-msg:noise instead.")
  (noise m))

(cl:ensure-generic-function 'vPeak-val :lambda-list '(m))
(cl:defmethod vPeak-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:vPeak-val is deprecated.  Use common_msgs-msg:vPeak instead.")
  (vPeak m))

(cl:ensure-generic-function 'coarseTOFInBins-val :lambda-list '(m))
(cl:defmethod coarseTOFInBins-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:coarseTOFInBins-val is deprecated.  Use common_msgs-msg:coarseTOFInBins instead.")
  (coarseTOFInBins m))

(cl:ensure-generic-function 'timestamp-val :lambda-list '(m))
(cl:defmethod timestamp-val ((m <UWB_FullRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:timestamp-val is deprecated.  Use common_msgs-msg:timestamp instead.")
  (timestamp m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <UWB_FullRangeInfo>) ostream)
  "Serializes a message object of type '<UWB_FullRangeInfo>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'responderId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'responderId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'responderId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'responderId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeStatus)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'antennaMode)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'stopwatchTime)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'stopwatchTime)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'precisionRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'precisionRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'precisionRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'precisionRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'coarseRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'coarseRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'coarseRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'coarseRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'filteredRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'filteredRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'filteredRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'filteredRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'precisionRangeErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'precisionRangeErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'coarseRangeErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'coarseRangeErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'filteredRangeErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'filteredRangeErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'filteredRangeVel)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'filteredRangeVel)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'filteredRangeVelErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'filteredRangeVelErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeMeasurementType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'reserved)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'reqLEDFlags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'reqLEDFlags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'respLEDFlags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'respLEDFlags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'noise)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'noise)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'vPeak)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'vPeak)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'coarseTOFInBins)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'coarseTOFInBins)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'coarseTOFInBins)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'coarseTOFInBins)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'timestamp)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <UWB_FullRangeInfo>) istream)
  "Deserializes a message object of type '<UWB_FullRangeInfo>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'responderId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'responderId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'responderId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'responderId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeStatus)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'antennaMode)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'stopwatchTime)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'stopwatchTime)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'precisionRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'precisionRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'precisionRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'precisionRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'coarseRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'coarseRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'coarseRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'coarseRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'filteredRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'filteredRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'filteredRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'filteredRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'precisionRangeErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'precisionRangeErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'coarseRangeErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'coarseRangeErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'filteredRangeErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'filteredRangeErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'filteredRangeVel)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'filteredRangeVel)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'filteredRangeVelErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'filteredRangeVelErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeMeasurementType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'reserved)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'reqLEDFlags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'reqLEDFlags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'respLEDFlags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'respLEDFlags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'noise)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'noise)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'vPeak)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'vPeak)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'coarseTOFInBins)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'coarseTOFInBins)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'coarseTOFInBins)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'coarseTOFInBins)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<UWB_FullRangeInfo>)))
  "Returns string type for a message object of type '<UWB_FullRangeInfo>"
  "common_msgs/UWB_FullRangeInfo")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'UWB_FullRangeInfo)))
  "Returns string type for a message object of type 'UWB_FullRangeInfo"
  "common_msgs/UWB_FullRangeInfo")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<UWB_FullRangeInfo>)))
  "Returns md5sum for a message object of type '<UWB_FullRangeInfo>"
  "6eed76e0c8b91a8a7fb2c268438b77be")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'UWB_FullRangeInfo)))
  "Returns md5sum for a message object of type 'UWB_FullRangeInfo"
  "6eed76e0c8b91a8a7fb2c268438b77be")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<UWB_FullRangeInfo>)))
  "Returns full string definition for message of type '<UWB_FullRangeInfo>"
  (cl:format cl:nil "Header header~%~%uint32 nodeId~%~%uint16 msgType~%uint16 msgId~%~%uint32 responderId~%~%uint8 rangeStatus~%uint8 antennaMode~%uint16 stopwatchTime~%~%uint32 precisionRangeMm~%uint32 coarseRangeMm~%uint32 filteredRangeMm~%~%uint16 precisionRangeErrEst~%uint16 coarseRangeErrEst~%uint16 filteredRangeErrEst~%~%uint16 filteredRangeVel~%uint16 filteredRangeVelErrEst~%~%uint8 rangeMeasurementType~%uint8 reserved~%~%uint16 reqLEDFlags~%uint16 respLEDFlags~%~%uint16 noise~%uint16 vPeak~%~%uint32 coarseTOFInBins~%~%uint32 timestamp~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'UWB_FullRangeInfo)))
  "Returns full string definition for message of type 'UWB_FullRangeInfo"
  (cl:format cl:nil "Header header~%~%uint32 nodeId~%~%uint16 msgType~%uint16 msgId~%~%uint32 responderId~%~%uint8 rangeStatus~%uint8 antennaMode~%uint16 stopwatchTime~%~%uint32 precisionRangeMm~%uint32 coarseRangeMm~%uint32 filteredRangeMm~%~%uint16 precisionRangeErrEst~%uint16 coarseRangeErrEst~%uint16 filteredRangeErrEst~%~%uint16 filteredRangeVel~%uint16 filteredRangeVelErrEst~%~%uint8 rangeMeasurementType~%uint8 reserved~%~%uint16 reqLEDFlags~%uint16 respLEDFlags~%~%uint16 noise~%uint16 vPeak~%~%uint32 coarseTOFInBins~%~%uint32 timestamp~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <UWB_FullRangeInfo>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     2
     2
     4
     1
     1
     2
     4
     4
     4
     2
     2
     2
     2
     2
     1
     1
     2
     2
     2
     2
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <UWB_FullRangeInfo>))
  "Converts a ROS message object to a list"
  (cl:list 'UWB_FullRangeInfo
    (cl:cons ':header (header msg))
    (cl:cons ':nodeId (nodeId msg))
    (cl:cons ':msgType (msgType msg))
    (cl:cons ':msgId (msgId msg))
    (cl:cons ':responderId (responderId msg))
    (cl:cons ':rangeStatus (rangeStatus msg))
    (cl:cons ':antennaMode (antennaMode msg))
    (cl:cons ':stopwatchTime (stopwatchTime msg))
    (cl:cons ':precisionRangeMm (precisionRangeMm msg))
    (cl:cons ':coarseRangeMm (coarseRangeMm msg))
    (cl:cons ':filteredRangeMm (filteredRangeMm msg))
    (cl:cons ':precisionRangeErrEst (precisionRangeErrEst msg))
    (cl:cons ':coarseRangeErrEst (coarseRangeErrEst msg))
    (cl:cons ':filteredRangeErrEst (filteredRangeErrEst msg))
    (cl:cons ':filteredRangeVel (filteredRangeVel msg))
    (cl:cons ':filteredRangeVelErrEst (filteredRangeVelErrEst msg))
    (cl:cons ':rangeMeasurementType (rangeMeasurementType msg))
    (cl:cons ':reserved (reserved msg))
    (cl:cons ':reqLEDFlags (reqLEDFlags msg))
    (cl:cons ':respLEDFlags (respLEDFlags msg))
    (cl:cons ':noise (noise msg))
    (cl:cons ':vPeak (vPeak msg))
    (cl:cons ':coarseTOFInBins (coarseTOFInBins msg))
    (cl:cons ':timestamp (timestamp msg))
))
