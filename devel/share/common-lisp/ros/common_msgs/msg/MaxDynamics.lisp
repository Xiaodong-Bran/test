; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude MaxDynamics.msg.html

(cl:defclass <MaxDynamics> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (vel_max
    :reader vel_max
    :initarg :vel_max
    :type geometry_msgs-msg:Twist
    :initform (cl:make-instance 'geometry_msgs-msg:Twist))
   (acc_max
    :reader acc_max
    :initarg :acc_max
    :type geometry_msgs-msg:Twist
    :initform (cl:make-instance 'geometry_msgs-msg:Twist))
   (jerk_max
    :reader jerk_max
    :initarg :jerk_max
    :type geometry_msgs-msg:Twist
    :initform (cl:make-instance 'geometry_msgs-msg:Twist)))
)

(cl:defclass MaxDynamics (<MaxDynamics>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MaxDynamics>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MaxDynamics)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<MaxDynamics> is deprecated: use common_msgs-msg:MaxDynamics instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MaxDynamics>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'vel_max-val :lambda-list '(m))
(cl:defmethod vel_max-val ((m <MaxDynamics>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:vel_max-val is deprecated.  Use common_msgs-msg:vel_max instead.")
  (vel_max m))

(cl:ensure-generic-function 'acc_max-val :lambda-list '(m))
(cl:defmethod acc_max-val ((m <MaxDynamics>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:acc_max-val is deprecated.  Use common_msgs-msg:acc_max instead.")
  (acc_max m))

(cl:ensure-generic-function 'jerk_max-val :lambda-list '(m))
(cl:defmethod jerk_max-val ((m <MaxDynamics>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:jerk_max-val is deprecated.  Use common_msgs-msg:jerk_max instead.")
  (jerk_max m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MaxDynamics>) ostream)
  "Serializes a message object of type '<MaxDynamics>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'vel_max) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'acc_max) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'jerk_max) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MaxDynamics>) istream)
  "Deserializes a message object of type '<MaxDynamics>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'vel_max) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'acc_max) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'jerk_max) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MaxDynamics>)))
  "Returns string type for a message object of type '<MaxDynamics>"
  "common_msgs/MaxDynamics")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MaxDynamics)))
  "Returns string type for a message object of type 'MaxDynamics"
  "common_msgs/MaxDynamics")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MaxDynamics>)))
  "Returns md5sum for a message object of type '<MaxDynamics>"
  "c3877f5e318c0ad8b9786835e1cba30d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MaxDynamics)))
  "Returns md5sum for a message object of type 'MaxDynamics"
  "c3877f5e318c0ad8b9786835e1cba30d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MaxDynamics>)))
  "Returns full string definition for message of type '<MaxDynamics>"
  (cl:format cl:nil "Header header~%geometry_msgs/Twist vel_max~%geometry_msgs/Twist acc_max~%geometry_msgs/Twist jerk_max~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MaxDynamics)))
  "Returns full string definition for message of type 'MaxDynamics"
  (cl:format cl:nil "Header header~%geometry_msgs/Twist vel_max~%geometry_msgs/Twist acc_max~%geometry_msgs/Twist jerk_max~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MaxDynamics>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'vel_max))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'acc_max))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'jerk_max))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MaxDynamics>))
  "Converts a ROS message object to a list"
  (cl:list 'MaxDynamics
    (cl:cons ':header (header msg))
    (cl:cons ':vel_max (vel_max msg))
    (cl:cons ':acc_max (acc_max msg))
    (cl:cons ':jerk_max (jerk_max msg))
))
