(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          POSE-VAL
          POSE
          IS_XY_VALID-VAL
          IS_XY_VALID
          IS_Z_VALID-VAL
          IS_Z_VALID
          IS_YAW_VALID-VAL
          IS_YAW_VALID
))