; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude PathPlanningStatus.msg.html

(cl:defclass <PathPlanningStatus> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (is_reached
    :reader is_reached
    :initarg :is_reached
    :type cl:boolean
    :initform cl:nil)
   (is_target_reachable
    :reader is_target_reachable
    :initarg :is_target_reachable
    :type cl:boolean
    :initform cl:nil)
   (waypoint
    :reader waypoint
    :initarg :waypoint
    :type common_msgs-msg:Waypoint
    :initform (cl:make-instance 'common_msgs-msg:Waypoint))
   (alternative_target_position_NWU
    :reader alternative_target_position_NWU
    :initarg :alternative_target_position_NWU
    :type geometry_msgs-msg:Pose
    :initform (cl:make-instance 'geometry_msgs-msg:Pose)))
)

(cl:defclass PathPlanningStatus (<PathPlanningStatus>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathPlanningStatus>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathPlanningStatus)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<PathPlanningStatus> is deprecated: use common_msgs-msg:PathPlanningStatus instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PathPlanningStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'is_reached-val :lambda-list '(m))
(cl:defmethod is_reached-val ((m <PathPlanningStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:is_reached-val is deprecated.  Use common_msgs-msg:is_reached instead.")
  (is_reached m))

(cl:ensure-generic-function 'is_target_reachable-val :lambda-list '(m))
(cl:defmethod is_target_reachable-val ((m <PathPlanningStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:is_target_reachable-val is deprecated.  Use common_msgs-msg:is_target_reachable instead.")
  (is_target_reachable m))

(cl:ensure-generic-function 'waypoint-val :lambda-list '(m))
(cl:defmethod waypoint-val ((m <PathPlanningStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:waypoint-val is deprecated.  Use common_msgs-msg:waypoint instead.")
  (waypoint m))

(cl:ensure-generic-function 'alternative_target_position_NWU-val :lambda-list '(m))
(cl:defmethod alternative_target_position_NWU-val ((m <PathPlanningStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:alternative_target_position_NWU-val is deprecated.  Use common_msgs-msg:alternative_target_position_NWU instead.")
  (alternative_target_position_NWU m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathPlanningStatus>) ostream)
  "Serializes a message object of type '<PathPlanningStatus>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_reached) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_target_reachable) 1 0)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'waypoint) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'alternative_target_position_NWU) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathPlanningStatus>) istream)
  "Deserializes a message object of type '<PathPlanningStatus>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'is_reached) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'is_target_reachable) (cl:not (cl:zerop (cl:read-byte istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'waypoint) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'alternative_target_position_NWU) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathPlanningStatus>)))
  "Returns string type for a message object of type '<PathPlanningStatus>"
  "common_msgs/PathPlanningStatus")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathPlanningStatus)))
  "Returns string type for a message object of type 'PathPlanningStatus"
  "common_msgs/PathPlanningStatus")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathPlanningStatus>)))
  "Returns md5sum for a message object of type '<PathPlanningStatus>"
  "45bec79eaf000e465a864699c7374601")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathPlanningStatus)))
  "Returns md5sum for a message object of type 'PathPlanningStatus"
  "45bec79eaf000e465a864699c7374601")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathPlanningStatus>)))
  "Returns full string definition for message of type '<PathPlanningStatus>"
  (cl:format cl:nil "Header header~%bool is_reached~%bool is_target_reachable~%common_msgs/Waypoint waypoint~%geometry_msgs/Pose alternative_target_position_NWU~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: common_msgs/Waypoint~%Header header~%common_msgs/NavigationState navigation_state~%string m_type~%uint32 m_id~%~%================================================================================~%MSG: common_msgs/NavigationState~%Header header~%geometry_msgs/Pose pose~%geometry_msgs/Twist velocity~%geometry_msgs/Twist acceleration~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathPlanningStatus)))
  "Returns full string definition for message of type 'PathPlanningStatus"
  (cl:format cl:nil "Header header~%bool is_reached~%bool is_target_reachable~%common_msgs/Waypoint waypoint~%geometry_msgs/Pose alternative_target_position_NWU~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: common_msgs/Waypoint~%Header header~%common_msgs/NavigationState navigation_state~%string m_type~%uint32 m_id~%~%================================================================================~%MSG: common_msgs/NavigationState~%Header header~%geometry_msgs/Pose pose~%geometry_msgs/Twist velocity~%geometry_msgs/Twist acceleration~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathPlanningStatus>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'waypoint))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'alternative_target_position_NWU))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathPlanningStatus>))
  "Converts a ROS message object to a list"
  (cl:list 'PathPlanningStatus
    (cl:cons ':header (header msg))
    (cl:cons ':is_reached (is_reached msg))
    (cl:cons ':is_target_reachable (is_target_reachable msg))
    (cl:cons ':waypoint (waypoint msg))
    (cl:cons ':alternative_target_position_NWU (alternative_target_position_NWU msg))
))
