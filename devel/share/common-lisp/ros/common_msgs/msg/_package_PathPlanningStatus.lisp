(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          IS_REACHED-VAL
          IS_REACHED
          IS_TARGET_REACHABLE-VAL
          IS_TARGET_REACHABLE
          WAYPOINT-VAL
          WAYPOINT
          ALTERNATIVE_TARGET_POSITION_NWU-VAL
          ALTERNATIVE_TARGET_POSITION_NWU
))