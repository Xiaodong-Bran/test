; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude UWB_FullNeighborDatabase.msg.html

(cl:defclass <UWB_FullNeighborDatabase> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (nodeId
    :reader nodeId
    :initarg :nodeId
    :type cl:integer
    :initform 0)
   (msgType
    :reader msgType
    :initarg :msgType
    :type cl:fixnum
    :initform 0)
   (msgId
    :reader msgId
    :initarg :msgId
    :type cl:fixnum
    :initform 0)
   (numNeighborEntries
    :reader numNeighborEntries
    :initarg :numNeighborEntries
    :type cl:fixnum
    :initform 0)
   (sortType
    :reader sortType
    :initarg :sortType
    :type cl:fixnum
    :initform 0)
   (reserved
    :reader reserved
    :initarg :reserved
    :type cl:fixnum
    :initform 0)
   (timestamp
    :reader timestamp
    :initarg :timestamp
    :type cl:integer
    :initform 0)
   (status
    :reader status
    :initarg :status
    :type cl:integer
    :initform 0)
   (neighbors
    :reader neighbors
    :initarg :neighbors
    :type (cl:vector common_msgs-msg:UWB_FullNeighborDatabaseEntry)
   :initform (cl:make-array 0 :element-type 'common_msgs-msg:UWB_FullNeighborDatabaseEntry :initial-element (cl:make-instance 'common_msgs-msg:UWB_FullNeighborDatabaseEntry))))
)

(cl:defclass UWB_FullNeighborDatabase (<UWB_FullNeighborDatabase>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <UWB_FullNeighborDatabase>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'UWB_FullNeighborDatabase)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<UWB_FullNeighborDatabase> is deprecated: use common_msgs-msg:UWB_FullNeighborDatabase instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'nodeId-val :lambda-list '(m))
(cl:defmethod nodeId-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:nodeId-val is deprecated.  Use common_msgs-msg:nodeId instead.")
  (nodeId m))

(cl:ensure-generic-function 'msgType-val :lambda-list '(m))
(cl:defmethod msgType-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:msgType-val is deprecated.  Use common_msgs-msg:msgType instead.")
  (msgType m))

(cl:ensure-generic-function 'msgId-val :lambda-list '(m))
(cl:defmethod msgId-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:msgId-val is deprecated.  Use common_msgs-msg:msgId instead.")
  (msgId m))

(cl:ensure-generic-function 'numNeighborEntries-val :lambda-list '(m))
(cl:defmethod numNeighborEntries-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:numNeighborEntries-val is deprecated.  Use common_msgs-msg:numNeighborEntries instead.")
  (numNeighborEntries m))

(cl:ensure-generic-function 'sortType-val :lambda-list '(m))
(cl:defmethod sortType-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:sortType-val is deprecated.  Use common_msgs-msg:sortType instead.")
  (sortType m))

(cl:ensure-generic-function 'reserved-val :lambda-list '(m))
(cl:defmethod reserved-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:reserved-val is deprecated.  Use common_msgs-msg:reserved instead.")
  (reserved m))

(cl:ensure-generic-function 'timestamp-val :lambda-list '(m))
(cl:defmethod timestamp-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:timestamp-val is deprecated.  Use common_msgs-msg:timestamp instead.")
  (timestamp m))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:status-val is deprecated.  Use common_msgs-msg:status instead.")
  (status m))

(cl:ensure-generic-function 'neighbors-val :lambda-list '(m))
(cl:defmethod neighbors-val ((m <UWB_FullNeighborDatabase>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:neighbors-val is deprecated.  Use common_msgs-msg:neighbors instead.")
  (neighbors m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <UWB_FullNeighborDatabase>) ostream)
  "Serializes a message object of type '<UWB_FullNeighborDatabase>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'numNeighborEntries)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'sortType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'reserved)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'reserved)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'status)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'status)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'status)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'neighbors))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'neighbors))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <UWB_FullNeighborDatabase>) istream)
  "Deserializes a message object of type '<UWB_FullNeighborDatabase>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'numNeighborEntries)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'sortType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'reserved)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'reserved)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'status)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'status)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'status)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'neighbors) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'neighbors)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'common_msgs-msg:UWB_FullNeighborDatabaseEntry))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<UWB_FullNeighborDatabase>)))
  "Returns string type for a message object of type '<UWB_FullNeighborDatabase>"
  "common_msgs/UWB_FullNeighborDatabase")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'UWB_FullNeighborDatabase)))
  "Returns string type for a message object of type 'UWB_FullNeighborDatabase"
  "common_msgs/UWB_FullNeighborDatabase")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<UWB_FullNeighborDatabase>)))
  "Returns md5sum for a message object of type '<UWB_FullNeighborDatabase>"
  "e5469a87bded28f455f13fbf487a001b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'UWB_FullNeighborDatabase)))
  "Returns md5sum for a message object of type 'UWB_FullNeighborDatabase"
  "e5469a87bded28f455f13fbf487a001b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<UWB_FullNeighborDatabase>)))
  "Returns full string definition for message of type '<UWB_FullNeighborDatabase>"
  (cl:format cl:nil "Header header~%~%uint32 nodeId~%~%uint16 msgType~%uint16 msgId~%uint8 numNeighborEntries~%uint8 sortType~%uint16 reserved~%uint32 timestamp~%uint32 status~%UWB_FullNeighborDatabaseEntry[] neighbors~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: common_msgs/UWB_FullNeighborDatabaseEntry~%Header header~%~%uint32 nodeId~%uint8 rangeStatus~%uint8 antennaMode~%uint16 stopwatchTime~%uint32 rangeMm~%uint16 rangeErrorEstimate~%uint16 rangeVelocity~%uint8 rangeMeasurementType~%uint8 flags~%uint16 ledFlags~%uint16 noise~%uint16 vPeak~%uint16 statsNumRangeAttempts~%uint16 statsNumRangeSuccesses~%uint32 statsAgeMs~%uint32 rangeUpdateTimestampMs~%uint32 lastHeardTimestampMs~%uint32 addedToNDBTimestampMs~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'UWB_FullNeighborDatabase)))
  "Returns full string definition for message of type 'UWB_FullNeighborDatabase"
  (cl:format cl:nil "Header header~%~%uint32 nodeId~%~%uint16 msgType~%uint16 msgId~%uint8 numNeighborEntries~%uint8 sortType~%uint16 reserved~%uint32 timestamp~%uint32 status~%UWB_FullNeighborDatabaseEntry[] neighbors~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: common_msgs/UWB_FullNeighborDatabaseEntry~%Header header~%~%uint32 nodeId~%uint8 rangeStatus~%uint8 antennaMode~%uint16 stopwatchTime~%uint32 rangeMm~%uint16 rangeErrorEstimate~%uint16 rangeVelocity~%uint8 rangeMeasurementType~%uint8 flags~%uint16 ledFlags~%uint16 noise~%uint16 vPeak~%uint16 statsNumRangeAttempts~%uint16 statsNumRangeSuccesses~%uint32 statsAgeMs~%uint32 rangeUpdateTimestampMs~%uint32 lastHeardTimestampMs~%uint32 addedToNDBTimestampMs~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <UWB_FullNeighborDatabase>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     2
     2
     1
     1
     2
     4
     4
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'neighbors) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <UWB_FullNeighborDatabase>))
  "Converts a ROS message object to a list"
  (cl:list 'UWB_FullNeighborDatabase
    (cl:cons ':header (header msg))
    (cl:cons ':nodeId (nodeId msg))
    (cl:cons ':msgType (msgType msg))
    (cl:cons ':msgId (msgId msg))
    (cl:cons ':numNeighborEntries (numNeighborEntries msg))
    (cl:cons ':sortType (sortType msg))
    (cl:cons ':reserved (reserved msg))
    (cl:cons ':timestamp (timestamp msg))
    (cl:cons ':status (status msg))
    (cl:cons ':neighbors (neighbors msg))
))
