; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude MeasurementVelocity.msg.html

(cl:defclass <MeasurementVelocity> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (velocity
    :reader velocity
    :initarg :velocity
    :type geometry_msgs-msg:Twist
    :initform (cl:make-instance 'geometry_msgs-msg:Twist))
   (is_xy_valid
    :reader is_xy_valid
    :initarg :is_xy_valid
    :type cl:boolean
    :initform cl:nil)
   (is_z_valid
    :reader is_z_valid
    :initarg :is_z_valid
    :type cl:boolean
    :initform cl:nil)
   (is_yaw_valid
    :reader is_yaw_valid
    :initarg :is_yaw_valid
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass MeasurementVelocity (<MeasurementVelocity>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MeasurementVelocity>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MeasurementVelocity)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<MeasurementVelocity> is deprecated: use common_msgs-msg:MeasurementVelocity instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MeasurementVelocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'velocity-val :lambda-list '(m))
(cl:defmethod velocity-val ((m <MeasurementVelocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:velocity-val is deprecated.  Use common_msgs-msg:velocity instead.")
  (velocity m))

(cl:ensure-generic-function 'is_xy_valid-val :lambda-list '(m))
(cl:defmethod is_xy_valid-val ((m <MeasurementVelocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:is_xy_valid-val is deprecated.  Use common_msgs-msg:is_xy_valid instead.")
  (is_xy_valid m))

(cl:ensure-generic-function 'is_z_valid-val :lambda-list '(m))
(cl:defmethod is_z_valid-val ((m <MeasurementVelocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:is_z_valid-val is deprecated.  Use common_msgs-msg:is_z_valid instead.")
  (is_z_valid m))

(cl:ensure-generic-function 'is_yaw_valid-val :lambda-list '(m))
(cl:defmethod is_yaw_valid-val ((m <MeasurementVelocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:is_yaw_valid-val is deprecated.  Use common_msgs-msg:is_yaw_valid instead.")
  (is_yaw_valid m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MeasurementVelocity>) ostream)
  "Serializes a message object of type '<MeasurementVelocity>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'velocity) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_xy_valid) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_z_valid) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_yaw_valid) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MeasurementVelocity>) istream)
  "Deserializes a message object of type '<MeasurementVelocity>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'velocity) istream)
    (cl:setf (cl:slot-value msg 'is_xy_valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'is_z_valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'is_yaw_valid) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MeasurementVelocity>)))
  "Returns string type for a message object of type '<MeasurementVelocity>"
  "common_msgs/MeasurementVelocity")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MeasurementVelocity)))
  "Returns string type for a message object of type 'MeasurementVelocity"
  "common_msgs/MeasurementVelocity")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MeasurementVelocity>)))
  "Returns md5sum for a message object of type '<MeasurementVelocity>"
  "1764c5d2ae75808f9fa4aeb5c6990809")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MeasurementVelocity)))
  "Returns md5sum for a message object of type 'MeasurementVelocity"
  "1764c5d2ae75808f9fa4aeb5c6990809")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MeasurementVelocity>)))
  "Returns full string definition for message of type '<MeasurementVelocity>"
  (cl:format cl:nil "Header header~%geometry_msgs/Twist velocity~%bool is_xy_valid~%bool is_z_valid~%bool is_yaw_valid~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MeasurementVelocity)))
  "Returns full string definition for message of type 'MeasurementVelocity"
  (cl:format cl:nil "Header header~%geometry_msgs/Twist velocity~%bool is_xy_valid~%bool is_z_valid~%bool is_yaw_valid~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MeasurementVelocity>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'velocity))
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MeasurementVelocity>))
  "Converts a ROS message object to a list"
  (cl:list 'MeasurementVelocity
    (cl:cons ':header (header msg))
    (cl:cons ':velocity (velocity msg))
    (cl:cons ':is_xy_valid (is_xy_valid msg))
    (cl:cons ':is_z_valid (is_z_valid msg))
    (cl:cons ':is_yaw_valid (is_yaw_valid msg))
))
