(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          MSGTYPE-VAL
          MSGTYPE
          MSGID-VAL
          MSGID
          REQUESTERID-VAL
          REQUESTERID
          RESPONDERID-VAL
          RESPONDERID
          PRECISIONRANGEMM-VAL
          PRECISIONRANGEMM
          PRECISIONRANGEERREST-VAL
          PRECISIONRANGEERREST
          LEDFLAGS-VAL
          LEDFLAGS
          TIMESTAMP-VAL
          TIMESTAMP
))