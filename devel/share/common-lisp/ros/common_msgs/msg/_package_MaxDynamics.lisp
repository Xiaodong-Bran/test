(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          VEL_MAX-VAL
          VEL_MAX
          ACC_MAX-VAL
          ACC_MAX
          JERK_MAX-VAL
          JERK_MAX
))