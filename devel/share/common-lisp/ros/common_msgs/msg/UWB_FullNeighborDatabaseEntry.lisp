; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude UWB_FullNeighborDatabaseEntry.msg.html

(cl:defclass <UWB_FullNeighborDatabaseEntry> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (nodeId
    :reader nodeId
    :initarg :nodeId
    :type cl:integer
    :initform 0)
   (rangeStatus
    :reader rangeStatus
    :initarg :rangeStatus
    :type cl:fixnum
    :initform 0)
   (antennaMode
    :reader antennaMode
    :initarg :antennaMode
    :type cl:fixnum
    :initform 0)
   (stopwatchTime
    :reader stopwatchTime
    :initarg :stopwatchTime
    :type cl:fixnum
    :initform 0)
   (rangeMm
    :reader rangeMm
    :initarg :rangeMm
    :type cl:integer
    :initform 0)
   (rangeErrorEstimate
    :reader rangeErrorEstimate
    :initarg :rangeErrorEstimate
    :type cl:fixnum
    :initform 0)
   (rangeVelocity
    :reader rangeVelocity
    :initarg :rangeVelocity
    :type cl:fixnum
    :initform 0)
   (rangeMeasurementType
    :reader rangeMeasurementType
    :initarg :rangeMeasurementType
    :type cl:fixnum
    :initform 0)
   (flags
    :reader flags
    :initarg :flags
    :type cl:fixnum
    :initform 0)
   (ledFlags
    :reader ledFlags
    :initarg :ledFlags
    :type cl:fixnum
    :initform 0)
   (noise
    :reader noise
    :initarg :noise
    :type cl:fixnum
    :initform 0)
   (vPeak
    :reader vPeak
    :initarg :vPeak
    :type cl:fixnum
    :initform 0)
   (statsNumRangeAttempts
    :reader statsNumRangeAttempts
    :initarg :statsNumRangeAttempts
    :type cl:fixnum
    :initform 0)
   (statsNumRangeSuccesses
    :reader statsNumRangeSuccesses
    :initarg :statsNumRangeSuccesses
    :type cl:fixnum
    :initform 0)
   (statsAgeMs
    :reader statsAgeMs
    :initarg :statsAgeMs
    :type cl:integer
    :initform 0)
   (rangeUpdateTimestampMs
    :reader rangeUpdateTimestampMs
    :initarg :rangeUpdateTimestampMs
    :type cl:integer
    :initform 0)
   (lastHeardTimestampMs
    :reader lastHeardTimestampMs
    :initarg :lastHeardTimestampMs
    :type cl:integer
    :initform 0)
   (addedToNDBTimestampMs
    :reader addedToNDBTimestampMs
    :initarg :addedToNDBTimestampMs
    :type cl:integer
    :initform 0))
)

(cl:defclass UWB_FullNeighborDatabaseEntry (<UWB_FullNeighborDatabaseEntry>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <UWB_FullNeighborDatabaseEntry>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'UWB_FullNeighborDatabaseEntry)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<UWB_FullNeighborDatabaseEntry> is deprecated: use common_msgs-msg:UWB_FullNeighborDatabaseEntry instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'nodeId-val :lambda-list '(m))
(cl:defmethod nodeId-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:nodeId-val is deprecated.  Use common_msgs-msg:nodeId instead.")
  (nodeId m))

(cl:ensure-generic-function 'rangeStatus-val :lambda-list '(m))
(cl:defmethod rangeStatus-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:rangeStatus-val is deprecated.  Use common_msgs-msg:rangeStatus instead.")
  (rangeStatus m))

(cl:ensure-generic-function 'antennaMode-val :lambda-list '(m))
(cl:defmethod antennaMode-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:antennaMode-val is deprecated.  Use common_msgs-msg:antennaMode instead.")
  (antennaMode m))

(cl:ensure-generic-function 'stopwatchTime-val :lambda-list '(m))
(cl:defmethod stopwatchTime-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:stopwatchTime-val is deprecated.  Use common_msgs-msg:stopwatchTime instead.")
  (stopwatchTime m))

(cl:ensure-generic-function 'rangeMm-val :lambda-list '(m))
(cl:defmethod rangeMm-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:rangeMm-val is deprecated.  Use common_msgs-msg:rangeMm instead.")
  (rangeMm m))

(cl:ensure-generic-function 'rangeErrorEstimate-val :lambda-list '(m))
(cl:defmethod rangeErrorEstimate-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:rangeErrorEstimate-val is deprecated.  Use common_msgs-msg:rangeErrorEstimate instead.")
  (rangeErrorEstimate m))

(cl:ensure-generic-function 'rangeVelocity-val :lambda-list '(m))
(cl:defmethod rangeVelocity-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:rangeVelocity-val is deprecated.  Use common_msgs-msg:rangeVelocity instead.")
  (rangeVelocity m))

(cl:ensure-generic-function 'rangeMeasurementType-val :lambda-list '(m))
(cl:defmethod rangeMeasurementType-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:rangeMeasurementType-val is deprecated.  Use common_msgs-msg:rangeMeasurementType instead.")
  (rangeMeasurementType m))

(cl:ensure-generic-function 'flags-val :lambda-list '(m))
(cl:defmethod flags-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:flags-val is deprecated.  Use common_msgs-msg:flags instead.")
  (flags m))

(cl:ensure-generic-function 'ledFlags-val :lambda-list '(m))
(cl:defmethod ledFlags-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:ledFlags-val is deprecated.  Use common_msgs-msg:ledFlags instead.")
  (ledFlags m))

(cl:ensure-generic-function 'noise-val :lambda-list '(m))
(cl:defmethod noise-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:noise-val is deprecated.  Use common_msgs-msg:noise instead.")
  (noise m))

(cl:ensure-generic-function 'vPeak-val :lambda-list '(m))
(cl:defmethod vPeak-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:vPeak-val is deprecated.  Use common_msgs-msg:vPeak instead.")
  (vPeak m))

(cl:ensure-generic-function 'statsNumRangeAttempts-val :lambda-list '(m))
(cl:defmethod statsNumRangeAttempts-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:statsNumRangeAttempts-val is deprecated.  Use common_msgs-msg:statsNumRangeAttempts instead.")
  (statsNumRangeAttempts m))

(cl:ensure-generic-function 'statsNumRangeSuccesses-val :lambda-list '(m))
(cl:defmethod statsNumRangeSuccesses-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:statsNumRangeSuccesses-val is deprecated.  Use common_msgs-msg:statsNumRangeSuccesses instead.")
  (statsNumRangeSuccesses m))

(cl:ensure-generic-function 'statsAgeMs-val :lambda-list '(m))
(cl:defmethod statsAgeMs-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:statsAgeMs-val is deprecated.  Use common_msgs-msg:statsAgeMs instead.")
  (statsAgeMs m))

(cl:ensure-generic-function 'rangeUpdateTimestampMs-val :lambda-list '(m))
(cl:defmethod rangeUpdateTimestampMs-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:rangeUpdateTimestampMs-val is deprecated.  Use common_msgs-msg:rangeUpdateTimestampMs instead.")
  (rangeUpdateTimestampMs m))

(cl:ensure-generic-function 'lastHeardTimestampMs-val :lambda-list '(m))
(cl:defmethod lastHeardTimestampMs-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:lastHeardTimestampMs-val is deprecated.  Use common_msgs-msg:lastHeardTimestampMs instead.")
  (lastHeardTimestampMs m))

(cl:ensure-generic-function 'addedToNDBTimestampMs-val :lambda-list '(m))
(cl:defmethod addedToNDBTimestampMs-val ((m <UWB_FullNeighborDatabaseEntry>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:addedToNDBTimestampMs-val is deprecated.  Use common_msgs-msg:addedToNDBTimestampMs instead.")
  (addedToNDBTimestampMs m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <UWB_FullNeighborDatabaseEntry>) ostream)
  "Serializes a message object of type '<UWB_FullNeighborDatabaseEntry>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'nodeId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeStatus)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'antennaMode)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'stopwatchTime)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'stopwatchTime)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'rangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'rangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'rangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeErrorEstimate)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'rangeErrorEstimate)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeVelocity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'rangeVelocity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeMeasurementType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'flags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'ledFlags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'ledFlags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'noise)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'noise)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'vPeak)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'vPeak)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'statsNumRangeAttempts)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'statsNumRangeAttempts)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'statsNumRangeSuccesses)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'statsNumRangeSuccesses)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'statsAgeMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'statsAgeMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'statsAgeMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'statsAgeMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeUpdateTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'rangeUpdateTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'rangeUpdateTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'rangeUpdateTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'lastHeardTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'lastHeardTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'lastHeardTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'lastHeardTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'addedToNDBTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'addedToNDBTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'addedToNDBTimestampMs)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'addedToNDBTimestampMs)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <UWB_FullNeighborDatabaseEntry>) istream)
  "Deserializes a message object of type '<UWB_FullNeighborDatabaseEntry>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'nodeId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeStatus)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'antennaMode)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'stopwatchTime)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'stopwatchTime)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'rangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'rangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'rangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeErrorEstimate)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'rangeErrorEstimate)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeVelocity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'rangeVelocity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeMeasurementType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'flags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'ledFlags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'ledFlags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'noise)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'noise)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'vPeak)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'vPeak)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'statsNumRangeAttempts)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'statsNumRangeAttempts)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'statsNumRangeSuccesses)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'statsNumRangeSuccesses)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'statsAgeMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'statsAgeMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'statsAgeMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'statsAgeMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rangeUpdateTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'rangeUpdateTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'rangeUpdateTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'rangeUpdateTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'lastHeardTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'lastHeardTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'lastHeardTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'lastHeardTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'addedToNDBTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'addedToNDBTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'addedToNDBTimestampMs)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'addedToNDBTimestampMs)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<UWB_FullNeighborDatabaseEntry>)))
  "Returns string type for a message object of type '<UWB_FullNeighborDatabaseEntry>"
  "common_msgs/UWB_FullNeighborDatabaseEntry")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'UWB_FullNeighborDatabaseEntry)))
  "Returns string type for a message object of type 'UWB_FullNeighborDatabaseEntry"
  "common_msgs/UWB_FullNeighborDatabaseEntry")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<UWB_FullNeighborDatabaseEntry>)))
  "Returns md5sum for a message object of type '<UWB_FullNeighborDatabaseEntry>"
  "b9108662cebcb5984d0800a52513d126")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'UWB_FullNeighborDatabaseEntry)))
  "Returns md5sum for a message object of type 'UWB_FullNeighborDatabaseEntry"
  "b9108662cebcb5984d0800a52513d126")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<UWB_FullNeighborDatabaseEntry>)))
  "Returns full string definition for message of type '<UWB_FullNeighborDatabaseEntry>"
  (cl:format cl:nil "Header header~%~%uint32 nodeId~%uint8 rangeStatus~%uint8 antennaMode~%uint16 stopwatchTime~%uint32 rangeMm~%uint16 rangeErrorEstimate~%uint16 rangeVelocity~%uint8 rangeMeasurementType~%uint8 flags~%uint16 ledFlags~%uint16 noise~%uint16 vPeak~%uint16 statsNumRangeAttempts~%uint16 statsNumRangeSuccesses~%uint32 statsAgeMs~%uint32 rangeUpdateTimestampMs~%uint32 lastHeardTimestampMs~%uint32 addedToNDBTimestampMs~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'UWB_FullNeighborDatabaseEntry)))
  "Returns full string definition for message of type 'UWB_FullNeighborDatabaseEntry"
  (cl:format cl:nil "Header header~%~%uint32 nodeId~%uint8 rangeStatus~%uint8 antennaMode~%uint16 stopwatchTime~%uint32 rangeMm~%uint16 rangeErrorEstimate~%uint16 rangeVelocity~%uint8 rangeMeasurementType~%uint8 flags~%uint16 ledFlags~%uint16 noise~%uint16 vPeak~%uint16 statsNumRangeAttempts~%uint16 statsNumRangeSuccesses~%uint32 statsAgeMs~%uint32 rangeUpdateTimestampMs~%uint32 lastHeardTimestampMs~%uint32 addedToNDBTimestampMs~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <UWB_FullNeighborDatabaseEntry>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     1
     1
     2
     4
     2
     2
     1
     1
     2
     2
     2
     2
     2
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <UWB_FullNeighborDatabaseEntry>))
  "Converts a ROS message object to a list"
  (cl:list 'UWB_FullNeighborDatabaseEntry
    (cl:cons ':header (header msg))
    (cl:cons ':nodeId (nodeId msg))
    (cl:cons ':rangeStatus (rangeStatus msg))
    (cl:cons ':antennaMode (antennaMode msg))
    (cl:cons ':stopwatchTime (stopwatchTime msg))
    (cl:cons ':rangeMm (rangeMm msg))
    (cl:cons ':rangeErrorEstimate (rangeErrorEstimate msg))
    (cl:cons ':rangeVelocity (rangeVelocity msg))
    (cl:cons ':rangeMeasurementType (rangeMeasurementType msg))
    (cl:cons ':flags (flags msg))
    (cl:cons ':ledFlags (ledFlags msg))
    (cl:cons ':noise (noise msg))
    (cl:cons ':vPeak (vPeak msg))
    (cl:cons ':statsNumRangeAttempts (statsNumRangeAttempts msg))
    (cl:cons ':statsNumRangeSuccesses (statsNumRangeSuccesses msg))
    (cl:cons ':statsAgeMs (statsAgeMs msg))
    (cl:cons ':rangeUpdateTimestampMs (rangeUpdateTimestampMs msg))
    (cl:cons ':lastHeardTimestampMs (lastHeardTimestampMs msg))
    (cl:cons ':addedToNDBTimestampMs (addedToNDBTimestampMs msg))
))
