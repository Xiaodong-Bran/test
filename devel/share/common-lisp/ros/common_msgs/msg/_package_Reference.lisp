(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          STATUS-VAL
          STATUS
          M_ID-VAL
          M_ID
          NAVIGATION-VAL
          NAVIGATION
          PIXHAWKCMD-VAL
          PIXHAWKCMD
          PIXHAWKSERVO-VAL
          PIXHAWKSERVO
))