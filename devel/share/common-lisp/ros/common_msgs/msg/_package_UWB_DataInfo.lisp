(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          NODEID-VAL
          NODEID
          MSGTYPE-VAL
          MSGTYPE
          MSGID-VAL
          MSGID
          SOURCEID-VAL
          SOURCEID
          NOISE-VAL
          NOISE
          VPEAK-VAL
          VPEAK
          TIMESTAMP-VAL
          TIMESTAMP
          ANTENNAID-VAL
          ANTENNAID
          RESERVED-VAL
          RESERVED
          DATASIZE-VAL
          DATASIZE
          DATA-VAL
          DATA
))