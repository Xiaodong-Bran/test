(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          M_TYPE-VAL
          M_TYPE
          M_ID-VAL
          M_ID
          WAYPOINT-VAL
          WAYPOINT
          GPS_WAYPOINT-VAL
          GPS_WAYPOINT
          PIXHAWKCMD-VAL
          PIXHAWKCMD
          PIXHAWKSERVO-VAL
          PIXHAWKSERVO
          REDUNDANCY-VAL
          REDUNDANCY
))