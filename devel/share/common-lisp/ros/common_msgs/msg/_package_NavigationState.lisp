(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          POSE-VAL
          POSE
          VELOCITY-VAL
          VELOCITY
          ACCELERATION-VAL
          ACCELERATION
))