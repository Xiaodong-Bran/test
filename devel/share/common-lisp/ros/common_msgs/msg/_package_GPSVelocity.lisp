(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          LAT_VEL-VAL
          LAT_VEL
          LON_VEL-VAL
          LON_VEL
          ALT_VEL-VAL
          ALT_VEL
))