; Auto-generated. Do not edit!


(cl:in-package common_msgs-msg)


;//! \htmlinclude UWB_EchoedRangeInfo.msg.html

(cl:defclass <UWB_EchoedRangeInfo> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (msgType
    :reader msgType
    :initarg :msgType
    :type cl:fixnum
    :initform 0)
   (msgId
    :reader msgId
    :initarg :msgId
    :type cl:fixnum
    :initform 0)
   (requesterId
    :reader requesterId
    :initarg :requesterId
    :type cl:integer
    :initform 0)
   (responderId
    :reader responderId
    :initarg :responderId
    :type cl:integer
    :initform 0)
   (precisionRangeMm
    :reader precisionRangeMm
    :initarg :precisionRangeMm
    :type cl:integer
    :initform 0)
   (precisionRangeErrEst
    :reader precisionRangeErrEst
    :initarg :precisionRangeErrEst
    :type cl:fixnum
    :initform 0)
   (ledFlags
    :reader ledFlags
    :initarg :ledFlags
    :type cl:fixnum
    :initform 0)
   (timestamp
    :reader timestamp
    :initarg :timestamp
    :type cl:integer
    :initform 0))
)

(cl:defclass UWB_EchoedRangeInfo (<UWB_EchoedRangeInfo>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <UWB_EchoedRangeInfo>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'UWB_EchoedRangeInfo)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-msg:<UWB_EchoedRangeInfo> is deprecated: use common_msgs-msg:UWB_EchoedRangeInfo instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:header-val is deprecated.  Use common_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'msgType-val :lambda-list '(m))
(cl:defmethod msgType-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:msgType-val is deprecated.  Use common_msgs-msg:msgType instead.")
  (msgType m))

(cl:ensure-generic-function 'msgId-val :lambda-list '(m))
(cl:defmethod msgId-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:msgId-val is deprecated.  Use common_msgs-msg:msgId instead.")
  (msgId m))

(cl:ensure-generic-function 'requesterId-val :lambda-list '(m))
(cl:defmethod requesterId-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:requesterId-val is deprecated.  Use common_msgs-msg:requesterId instead.")
  (requesterId m))

(cl:ensure-generic-function 'responderId-val :lambda-list '(m))
(cl:defmethod responderId-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:responderId-val is deprecated.  Use common_msgs-msg:responderId instead.")
  (responderId m))

(cl:ensure-generic-function 'precisionRangeMm-val :lambda-list '(m))
(cl:defmethod precisionRangeMm-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:precisionRangeMm-val is deprecated.  Use common_msgs-msg:precisionRangeMm instead.")
  (precisionRangeMm m))

(cl:ensure-generic-function 'precisionRangeErrEst-val :lambda-list '(m))
(cl:defmethod precisionRangeErrEst-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:precisionRangeErrEst-val is deprecated.  Use common_msgs-msg:precisionRangeErrEst instead.")
  (precisionRangeErrEst m))

(cl:ensure-generic-function 'ledFlags-val :lambda-list '(m))
(cl:defmethod ledFlags-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:ledFlags-val is deprecated.  Use common_msgs-msg:ledFlags instead.")
  (ledFlags m))

(cl:ensure-generic-function 'timestamp-val :lambda-list '(m))
(cl:defmethod timestamp-val ((m <UWB_EchoedRangeInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-msg:timestamp-val is deprecated.  Use common_msgs-msg:timestamp instead.")
  (timestamp m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <UWB_EchoedRangeInfo>) ostream)
  "Serializes a message object of type '<UWB_EchoedRangeInfo>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgType)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'requesterId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'requesterId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'requesterId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'requesterId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'responderId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'responderId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'responderId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'responderId)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'precisionRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'precisionRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'precisionRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'precisionRangeMm)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'precisionRangeErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'precisionRangeErrEst)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'ledFlags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'ledFlags)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'timestamp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'timestamp)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <UWB_EchoedRangeInfo>) istream)
  "Deserializes a message object of type '<UWB_EchoedRangeInfo>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgType)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'msgId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'msgId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'requesterId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'requesterId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'requesterId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'requesterId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'responderId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'responderId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'responderId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'responderId)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'precisionRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'precisionRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'precisionRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'precisionRangeMm)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'precisionRangeErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'precisionRangeErrEst)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'ledFlags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'ledFlags)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'timestamp)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<UWB_EchoedRangeInfo>)))
  "Returns string type for a message object of type '<UWB_EchoedRangeInfo>"
  "common_msgs/UWB_EchoedRangeInfo")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'UWB_EchoedRangeInfo)))
  "Returns string type for a message object of type 'UWB_EchoedRangeInfo"
  "common_msgs/UWB_EchoedRangeInfo")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<UWB_EchoedRangeInfo>)))
  "Returns md5sum for a message object of type '<UWB_EchoedRangeInfo>"
  "e88801ba6b3d031989091b69fd886607")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'UWB_EchoedRangeInfo)))
  "Returns md5sum for a message object of type 'UWB_EchoedRangeInfo"
  "e88801ba6b3d031989091b69fd886607")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<UWB_EchoedRangeInfo>)))
  "Returns full string definition for message of type '<UWB_EchoedRangeInfo>"
  (cl:format cl:nil "Header header~%~%uint16 msgType~%uint16 msgId~%~%uint32 requesterId~%uint32 responderId~%~%uint32 precisionRangeMm~%uint16 precisionRangeErrEst~%~%uint16 ledFlags~%uint32 timestamp~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'UWB_EchoedRangeInfo)))
  "Returns full string definition for message of type 'UWB_EchoedRangeInfo"
  (cl:format cl:nil "Header header~%~%uint16 msgType~%uint16 msgId~%~%uint32 requesterId~%uint32 responderId~%~%uint32 precisionRangeMm~%uint16 precisionRangeErrEst~%~%uint16 ledFlags~%uint32 timestamp~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <UWB_EchoedRangeInfo>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     2
     2
     4
     4
     4
     2
     2
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <UWB_EchoedRangeInfo>))
  "Converts a ROS message object to a list"
  (cl:list 'UWB_EchoedRangeInfo
    (cl:cons ':header (header msg))
    (cl:cons ':msgType (msgType msg))
    (cl:cons ':msgId (msgId msg))
    (cl:cons ':requesterId (requesterId msg))
    (cl:cons ':responderId (responderId msg))
    (cl:cons ':precisionRangeMm (precisionRangeMm msg))
    (cl:cons ':precisionRangeErrEst (precisionRangeErrEst msg))
    (cl:cons ':ledFlags (ledFlags msg))
    (cl:cons ':timestamp (timestamp msg))
))
