(cl:in-package common_msgs-srv)
(cl:export '(TYPE-VAL
          TYPE
          SWITCH_CMD-VAL
          SWITCH_CMD
          DETECT_STATUS-VAL
          DETECT_STATUS
          POSE-VAL
          POSE
          CENTER-VAL
          CENTER
          CORNERS-VAL
          CORNERS
))