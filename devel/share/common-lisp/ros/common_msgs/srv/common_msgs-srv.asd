
(cl:in-package :asdf)

(defsystem "common_msgs-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
)
  :components ((:file "_package")
    (:file "DetectionController" :depends-on ("_package_DetectionController"))
    (:file "_package_DetectionController" :depends-on ("_package"))
  ))