; Auto-generated. Do not edit!


(cl:in-package common_msgs-srv)


;//! \htmlinclude DetectionController-request.msg.html

(cl:defclass <DetectionController-request> (roslisp-msg-protocol:ros-message)
  ((type
    :reader type
    :initarg :type
    :type cl:string
    :initform "")
   (switch_cmd
    :reader switch_cmd
    :initarg :switch_cmd
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass DetectionController-request (<DetectionController-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <DetectionController-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'DetectionController-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-srv:<DetectionController-request> is deprecated: use common_msgs-srv:DetectionController-request instead.")))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <DetectionController-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-srv:type-val is deprecated.  Use common_msgs-srv:type instead.")
  (type m))

(cl:ensure-generic-function 'switch_cmd-val :lambda-list '(m))
(cl:defmethod switch_cmd-val ((m <DetectionController-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-srv:switch_cmd-val is deprecated.  Use common_msgs-srv:switch_cmd instead.")
  (switch_cmd m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <DetectionController-request>) ostream)
  "Serializes a message object of type '<DetectionController-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'type))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'switch_cmd) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <DetectionController-request>) istream)
  "Deserializes a message object of type '<DetectionController-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:slot-value msg 'switch_cmd) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<DetectionController-request>)))
  "Returns string type for a service object of type '<DetectionController-request>"
  "common_msgs/DetectionControllerRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DetectionController-request)))
  "Returns string type for a service object of type 'DetectionController-request"
  "common_msgs/DetectionControllerRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<DetectionController-request>)))
  "Returns md5sum for a message object of type '<DetectionController-request>"
  "b9a7e1d4bca5e0973c393d8be82dc739")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'DetectionController-request)))
  "Returns md5sum for a message object of type 'DetectionController-request"
  "b9a7e1d4bca5e0973c393d8be82dc739")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<DetectionController-request>)))
  "Returns full string definition for message of type '<DetectionController-request>"
  (cl:format cl:nil "~%string type~%bool switch_cmd~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'DetectionController-request)))
  "Returns full string definition for message of type 'DetectionController-request"
  (cl:format cl:nil "~%string type~%bool switch_cmd~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <DetectionController-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'type))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <DetectionController-request>))
  "Converts a ROS message object to a list"
  (cl:list 'DetectionController-request
    (cl:cons ':type (type msg))
    (cl:cons ':switch_cmd (switch_cmd msg))
))
;//! \htmlinclude DetectionController-response.msg.html

(cl:defclass <DetectionController-response> (roslisp-msg-protocol:ros-message)
  ((detect_status
    :reader detect_status
    :initarg :detect_status
    :type cl:integer
    :initform 0)
   (pose
    :reader pose
    :initarg :pose
    :type geometry_msgs-msg:Pose
    :initform (cl:make-instance 'geometry_msgs-msg:Pose))
   (center
    :reader center
    :initarg :center
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (corners
    :reader corners
    :initarg :corners
    :type (cl:vector geometry_msgs-msg:Point)
   :initform (cl:make-array 0 :element-type 'geometry_msgs-msg:Point :initial-element (cl:make-instance 'geometry_msgs-msg:Point))))
)

(cl:defclass DetectionController-response (<DetectionController-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <DetectionController-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'DetectionController-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name common_msgs-srv:<DetectionController-response> is deprecated: use common_msgs-srv:DetectionController-response instead.")))

(cl:ensure-generic-function 'detect_status-val :lambda-list '(m))
(cl:defmethod detect_status-val ((m <DetectionController-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-srv:detect_status-val is deprecated.  Use common_msgs-srv:detect_status instead.")
  (detect_status m))

(cl:ensure-generic-function 'pose-val :lambda-list '(m))
(cl:defmethod pose-val ((m <DetectionController-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-srv:pose-val is deprecated.  Use common_msgs-srv:pose instead.")
  (pose m))

(cl:ensure-generic-function 'center-val :lambda-list '(m))
(cl:defmethod center-val ((m <DetectionController-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-srv:center-val is deprecated.  Use common_msgs-srv:center instead.")
  (center m))

(cl:ensure-generic-function 'corners-val :lambda-list '(m))
(cl:defmethod corners-val ((m <DetectionController-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader common_msgs-srv:corners-val is deprecated.  Use common_msgs-srv:corners instead.")
  (corners m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <DetectionController-response>) ostream)
  "Serializes a message object of type '<DetectionController-response>"
  (cl:let* ((signed (cl:slot-value msg 'detect_status)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'pose) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'center) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'corners))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'corners))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <DetectionController-response>) istream)
  "Deserializes a message object of type '<DetectionController-response>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'detect_status) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'pose) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'center) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'corners) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'corners)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'geometry_msgs-msg:Point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<DetectionController-response>)))
  "Returns string type for a service object of type '<DetectionController-response>"
  "common_msgs/DetectionControllerResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DetectionController-response)))
  "Returns string type for a service object of type 'DetectionController-response"
  "common_msgs/DetectionControllerResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<DetectionController-response>)))
  "Returns md5sum for a message object of type '<DetectionController-response>"
  "b9a7e1d4bca5e0973c393d8be82dc739")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'DetectionController-response)))
  "Returns md5sum for a message object of type 'DetectionController-response"
  "b9a7e1d4bca5e0973c393d8be82dc739")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<DetectionController-response>)))
  "Returns full string definition for message of type '<DetectionController-response>"
  (cl:format cl:nil "~%int32 detect_status~%geometry_msgs/Pose pose~%geometry_msgs/Point center~%geometry_msgs/Point[] corners~%~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'DetectionController-response)))
  "Returns full string definition for message of type 'DetectionController-response"
  (cl:format cl:nil "~%int32 detect_status~%geometry_msgs/Pose pose~%geometry_msgs/Point center~%geometry_msgs/Point[] corners~%~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <DetectionController-response>))
  (cl:+ 0
     4
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'pose))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'center))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'corners) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <DetectionController-response>))
  "Converts a ROS message object to a list"
  (cl:list 'DetectionController-response
    (cl:cons ':detect_status (detect_status msg))
    (cl:cons ':pose (pose msg))
    (cl:cons ':center (center msg))
    (cl:cons ':corners (corners msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'DetectionController)))
  'DetectionController-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'DetectionController)))
  'DetectionController-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DetectionController)))
  "Returns string type for a service object of type '<DetectionController>"
  "common_msgs/DetectionController")