
(cl:in-package :asdf)

(defsystem "apriltag_ros-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "AprilMission" :depends-on ("_package_AprilMission"))
    (:file "_package_AprilMission" :depends-on ("_package"))
    (:file "AprilStatus" :depends-on ("_package_AprilStatus"))
    (:file "_package_AprilStatus" :depends-on ("_package"))
    (:file "Apriltag" :depends-on ("_package_Apriltag"))
    (:file "_package_Apriltag" :depends-on ("_package"))
    (:file "Apriltags" :depends-on ("_package_Apriltags"))
    (:file "_package_Apriltags" :depends-on ("_package"))
  ))