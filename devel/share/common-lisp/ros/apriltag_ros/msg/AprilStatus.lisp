; Auto-generated. Do not edit!


(cl:in-package apriltag_ros-msg)


;//! \htmlinclude AprilStatus.msg.html

(cl:defclass <AprilStatus> (roslisp-msg-protocol:ros-message)
  ((is_valid
    :reader is_valid
    :initarg :is_valid
    :type cl:boolean
    :initform cl:nil)
   (mission_type
    :reader mission_type
    :initarg :mission_type
    :type cl:string
    :initform "")
   (pose
    :reader pose
    :initarg :pose
    :type geometry_msgs-msg:Pose
    :initform (cl:make-instance 'geometry_msgs-msg:Pose)))
)

(cl:defclass AprilStatus (<AprilStatus>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AprilStatus>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AprilStatus)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name apriltag_ros-msg:<AprilStatus> is deprecated: use apriltag_ros-msg:AprilStatus instead.")))

(cl:ensure-generic-function 'is_valid-val :lambda-list '(m))
(cl:defmethod is_valid-val ((m <AprilStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader apriltag_ros-msg:is_valid-val is deprecated.  Use apriltag_ros-msg:is_valid instead.")
  (is_valid m))

(cl:ensure-generic-function 'mission_type-val :lambda-list '(m))
(cl:defmethod mission_type-val ((m <AprilStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader apriltag_ros-msg:mission_type-val is deprecated.  Use apriltag_ros-msg:mission_type instead.")
  (mission_type m))

(cl:ensure-generic-function 'pose-val :lambda-list '(m))
(cl:defmethod pose-val ((m <AprilStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader apriltag_ros-msg:pose-val is deprecated.  Use apriltag_ros-msg:pose instead.")
  (pose m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AprilStatus>) ostream)
  "Serializes a message object of type '<AprilStatus>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'is_valid) 1 0)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'mission_type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'mission_type))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'pose) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AprilStatus>) istream)
  "Deserializes a message object of type '<AprilStatus>"
    (cl:setf (cl:slot-value msg 'is_valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'mission_type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'mission_type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'pose) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AprilStatus>)))
  "Returns string type for a message object of type '<AprilStatus>"
  "apriltag_ros/AprilStatus")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AprilStatus)))
  "Returns string type for a message object of type 'AprilStatus"
  "apriltag_ros/AprilStatus")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AprilStatus>)))
  "Returns md5sum for a message object of type '<AprilStatus>"
  "3cb78f57ed11ac61e061a148deec0fea")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AprilStatus)))
  "Returns md5sum for a message object of type 'AprilStatus"
  "3cb78f57ed11ac61e061a148deec0fea")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AprilStatus>)))
  "Returns full string definition for message of type '<AprilStatus>"
  (cl:format cl:nil "# Apriltag status~%bool is_valid~%string mission_type~%geometry_msgs/Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AprilStatus)))
  "Returns full string definition for message of type 'AprilStatus"
  (cl:format cl:nil "# Apriltag status~%bool is_valid~%string mission_type~%geometry_msgs/Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AprilStatus>))
  (cl:+ 0
     1
     4 (cl:length (cl:slot-value msg 'mission_type))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'pose))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AprilStatus>))
  "Converts a ROS message object to a list"
  (cl:list 'AprilStatus
    (cl:cons ':is_valid (is_valid msg))
    (cl:cons ':mission_type (mission_type msg))
    (cl:cons ':pose (pose msg))
))
