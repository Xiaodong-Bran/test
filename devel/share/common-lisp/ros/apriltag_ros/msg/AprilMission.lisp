; Auto-generated. Do not edit!


(cl:in-package apriltag_ros-msg)


;//! \htmlinclude AprilMission.msg.html

(cl:defclass <AprilMission> (roslisp-msg-protocol:ros-message)
  ((status
    :reader status
    :initarg :status
    :type cl:string
    :initform "")
   (mission_type
    :reader mission_type
    :initarg :mission_type
    :type cl:string
    :initform ""))
)

(cl:defclass AprilMission (<AprilMission>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AprilMission>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AprilMission)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name apriltag_ros-msg:<AprilMission> is deprecated: use apriltag_ros-msg:AprilMission instead.")))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <AprilMission>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader apriltag_ros-msg:status-val is deprecated.  Use apriltag_ros-msg:status instead.")
  (status m))

(cl:ensure-generic-function 'mission_type-val :lambda-list '(m))
(cl:defmethod mission_type-val ((m <AprilMission>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader apriltag_ros-msg:mission_type-val is deprecated.  Use apriltag_ros-msg:mission_type instead.")
  (mission_type m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AprilMission>) ostream)
  "Serializes a message object of type '<AprilMission>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'status))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'status))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'mission_type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'mission_type))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AprilMission>) istream)
  "Deserializes a message object of type '<AprilMission>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'status) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'status) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'mission_type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'mission_type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AprilMission>)))
  "Returns string type for a message object of type '<AprilMission>"
  "apriltag_ros/AprilMission")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AprilMission)))
  "Returns string type for a message object of type 'AprilMission"
  "apriltag_ros/AprilMission")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AprilMission>)))
  "Returns md5sum for a message object of type '<AprilMission>"
  "16b0c36840e5ecefd5f8b4717b215c8b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AprilMission)))
  "Returns md5sum for a message object of type 'AprilMission"
  "16b0c36840e5ecefd5f8b4717b215c8b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AprilMission>)))
  "Returns full string definition for message of type '<AprilMission>"
  (cl:format cl:nil "# Apriltag mission~%string status~%string mission_type~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AprilMission)))
  "Returns full string definition for message of type 'AprilMission"
  (cl:format cl:nil "# Apriltag mission~%string status~%string mission_type~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AprilMission>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'status))
     4 (cl:length (cl:slot-value msg 'mission_type))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AprilMission>))
  "Converts a ROS message object to a list"
  (cl:list 'AprilMission
    (cl:cons ':status (status msg))
    (cl:cons ':mission_type (mission_type msg))
))
