
(cl:in-package :asdf)

(defsystem "apriltag_ros-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :apriltag_ros-msg
)
  :components ((:file "_package")
    (:file "GetAprilStatus" :depends-on ("_package_GetAprilStatus"))
    (:file "_package_GetAprilStatus" :depends-on ("_package"))
  ))