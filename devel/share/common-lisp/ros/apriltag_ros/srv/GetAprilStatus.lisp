; Auto-generated. Do not edit!


(cl:in-package apriltag_ros-srv)


;//! \htmlinclude GetAprilStatus-request.msg.html

(cl:defclass <GetAprilStatus-request> (roslisp-msg-protocol:ros-message)
  ((mission_switch
    :reader mission_switch
    :initarg :mission_switch
    :type cl:string
    :initform "")
   (mission_type
    :reader mission_type
    :initarg :mission_type
    :type cl:string
    :initform ""))
)

(cl:defclass GetAprilStatus-request (<GetAprilStatus-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GetAprilStatus-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GetAprilStatus-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name apriltag_ros-srv:<GetAprilStatus-request> is deprecated: use apriltag_ros-srv:GetAprilStatus-request instead.")))

(cl:ensure-generic-function 'mission_switch-val :lambda-list '(m))
(cl:defmethod mission_switch-val ((m <GetAprilStatus-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader apriltag_ros-srv:mission_switch-val is deprecated.  Use apriltag_ros-srv:mission_switch instead.")
  (mission_switch m))

(cl:ensure-generic-function 'mission_type-val :lambda-list '(m))
(cl:defmethod mission_type-val ((m <GetAprilStatus-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader apriltag_ros-srv:mission_type-val is deprecated.  Use apriltag_ros-srv:mission_type instead.")
  (mission_type m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GetAprilStatus-request>) ostream)
  "Serializes a message object of type '<GetAprilStatus-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'mission_switch))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'mission_switch))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'mission_type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'mission_type))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GetAprilStatus-request>) istream)
  "Deserializes a message object of type '<GetAprilStatus-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'mission_switch) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'mission_switch) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'mission_type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'mission_type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GetAprilStatus-request>)))
  "Returns string type for a service object of type '<GetAprilStatus-request>"
  "apriltag_ros/GetAprilStatusRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetAprilStatus-request)))
  "Returns string type for a service object of type 'GetAprilStatus-request"
  "apriltag_ros/GetAprilStatusRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GetAprilStatus-request>)))
  "Returns md5sum for a message object of type '<GetAprilStatus-request>"
  "8b56a804bb0c4311853fc2df1c9bfb2e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GetAprilStatus-request)))
  "Returns md5sum for a message object of type 'GetAprilStatus-request"
  "8b56a804bb0c4311853fc2df1c9bfb2e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GetAprilStatus-request>)))
  "Returns full string definition for message of type '<GetAprilStatus-request>"
  (cl:format cl:nil "~%string mission_switch~%string mission_type~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GetAprilStatus-request)))
  "Returns full string definition for message of type 'GetAprilStatus-request"
  (cl:format cl:nil "~%string mission_switch~%string mission_type~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GetAprilStatus-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'mission_switch))
     4 (cl:length (cl:slot-value msg 'mission_type))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GetAprilStatus-request>))
  "Converts a ROS message object to a list"
  (cl:list 'GetAprilStatus-request
    (cl:cons ':mission_switch (mission_switch msg))
    (cl:cons ':mission_type (mission_type msg))
))
;//! \htmlinclude GetAprilStatus-response.msg.html

(cl:defclass <GetAprilStatus-response> (roslisp-msg-protocol:ros-message)
  ((april_status
    :reader april_status
    :initarg :april_status
    :type apriltag_ros-msg:AprilStatus
    :initform (cl:make-instance 'apriltag_ros-msg:AprilStatus)))
)

(cl:defclass GetAprilStatus-response (<GetAprilStatus-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GetAprilStatus-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GetAprilStatus-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name apriltag_ros-srv:<GetAprilStatus-response> is deprecated: use apriltag_ros-srv:GetAprilStatus-response instead.")))

(cl:ensure-generic-function 'april_status-val :lambda-list '(m))
(cl:defmethod april_status-val ((m <GetAprilStatus-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader apriltag_ros-srv:april_status-val is deprecated.  Use apriltag_ros-srv:april_status instead.")
  (april_status m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GetAprilStatus-response>) ostream)
  "Serializes a message object of type '<GetAprilStatus-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'april_status) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GetAprilStatus-response>) istream)
  "Deserializes a message object of type '<GetAprilStatus-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'april_status) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GetAprilStatus-response>)))
  "Returns string type for a service object of type '<GetAprilStatus-response>"
  "apriltag_ros/GetAprilStatusResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetAprilStatus-response)))
  "Returns string type for a service object of type 'GetAprilStatus-response"
  "apriltag_ros/GetAprilStatusResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GetAprilStatus-response>)))
  "Returns md5sum for a message object of type '<GetAprilStatus-response>"
  "8b56a804bb0c4311853fc2df1c9bfb2e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GetAprilStatus-response)))
  "Returns md5sum for a message object of type 'GetAprilStatus-response"
  "8b56a804bb0c4311853fc2df1c9bfb2e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GetAprilStatus-response>)))
  "Returns full string definition for message of type '<GetAprilStatus-response>"
  (cl:format cl:nil "~%apriltag_ros/AprilStatus april_status~%~%~%================================================================================~%MSG: apriltag_ros/AprilStatus~%# Apriltag status~%bool is_valid~%string mission_type~%geometry_msgs/Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GetAprilStatus-response)))
  "Returns full string definition for message of type 'GetAprilStatus-response"
  (cl:format cl:nil "~%apriltag_ros/AprilStatus april_status~%~%~%================================================================================~%MSG: apriltag_ros/AprilStatus~%# Apriltag status~%bool is_valid~%string mission_type~%geometry_msgs/Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GetAprilStatus-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'april_status))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GetAprilStatus-response>))
  "Converts a ROS message object to a list"
  (cl:list 'GetAprilStatus-response
    (cl:cons ':april_status (april_status msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'GetAprilStatus)))
  'GetAprilStatus-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'GetAprilStatus)))
  'GetAprilStatus-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetAprilStatus)))
  "Returns string type for a service object of type '<GetAprilStatus>"
  "apriltag_ros/GetAprilStatus")