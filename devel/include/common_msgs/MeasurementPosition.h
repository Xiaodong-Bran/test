// Generated by gencpp from file common_msgs/MeasurementPosition.msg
// DO NOT EDIT!


#ifndef COMMON_MSGS_MESSAGE_MEASUREMENTPOSITION_H
#define COMMON_MSGS_MESSAGE_MEASUREMENTPOSITION_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>
#include <geometry_msgs/Pose.h>

namespace common_msgs
{
template <class ContainerAllocator>
struct MeasurementPosition_
{
  typedef MeasurementPosition_<ContainerAllocator> Type;

  MeasurementPosition_()
    : header()
    , pose()
    , is_xy_valid(false)
    , is_z_valid(false)
    , is_yaw_valid(false)  {
    }
  MeasurementPosition_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , pose(_alloc)
    , is_xy_valid(false)
    , is_z_valid(false)
    , is_yaw_valid(false)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef  ::geometry_msgs::Pose_<ContainerAllocator>  _pose_type;
  _pose_type pose;

   typedef uint8_t _is_xy_valid_type;
  _is_xy_valid_type is_xy_valid;

   typedef uint8_t _is_z_valid_type;
  _is_z_valid_type is_z_valid;

   typedef uint8_t _is_yaw_valid_type;
  _is_yaw_valid_type is_yaw_valid;




  typedef boost::shared_ptr< ::common_msgs::MeasurementPosition_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::common_msgs::MeasurementPosition_<ContainerAllocator> const> ConstPtr;

}; // struct MeasurementPosition_

typedef ::common_msgs::MeasurementPosition_<std::allocator<void> > MeasurementPosition;

typedef boost::shared_ptr< ::common_msgs::MeasurementPosition > MeasurementPositionPtr;
typedef boost::shared_ptr< ::common_msgs::MeasurementPosition const> MeasurementPositionConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::common_msgs::MeasurementPosition_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::common_msgs::MeasurementPosition_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace common_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'common_msgs': ['/home/bran/Documents/gittest/src/common_msgs/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::common_msgs::MeasurementPosition_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::common_msgs::MeasurementPosition_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::common_msgs::MeasurementPosition_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::common_msgs::MeasurementPosition_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::common_msgs::MeasurementPosition_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::common_msgs::MeasurementPosition_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::common_msgs::MeasurementPosition_<ContainerAllocator> >
{
  static const char* value()
  {
    return "49fef8348ffe3fc1ef41fbe01ce739a3";
  }

  static const char* value(const ::common_msgs::MeasurementPosition_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x49fef8348ffe3fc1ULL;
  static const uint64_t static_value2 = 0xef41fbe01ce739a3ULL;
};

template<class ContainerAllocator>
struct DataType< ::common_msgs::MeasurementPosition_<ContainerAllocator> >
{
  static const char* value()
  {
    return "common_msgs/MeasurementPosition";
  }

  static const char* value(const ::common_msgs::MeasurementPosition_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::common_msgs::MeasurementPosition_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n\
geometry_msgs/Pose pose\n\
bool is_xy_valid\n\
bool is_z_valid\n\
bool is_yaw_valid\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Pose\n\
# A representation of pose in free space, composed of position and orientation. \n\
Point position\n\
Quaternion orientation\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Point\n\
# This contains the position of a point in free space\n\
float64 x\n\
float64 y\n\
float64 z\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Quaternion\n\
# This represents an orientation in free space in quaternion form.\n\
\n\
float64 x\n\
float64 y\n\
float64 z\n\
float64 w\n\
";
  }

  static const char* value(const ::common_msgs::MeasurementPosition_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::common_msgs::MeasurementPosition_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.pose);
      stream.next(m.is_xy_valid);
      stream.next(m.is_z_valid);
      stream.next(m.is_yaw_valid);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct MeasurementPosition_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::common_msgs::MeasurementPosition_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::common_msgs::MeasurementPosition_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "pose: ";
    s << std::endl;
    Printer< ::geometry_msgs::Pose_<ContainerAllocator> >::stream(s, indent + "  ", v.pose);
    s << indent << "is_xy_valid: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.is_xy_valid);
    s << indent << "is_z_valid: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.is_z_valid);
    s << indent << "is_yaw_valid: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.is_yaw_valid);
  }
};

} // namespace message_operations
} // namespace ros

#endif // COMMON_MSGS_MESSAGE_MEASUREMENTPOSITION_H
