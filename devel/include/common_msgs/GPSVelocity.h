// Generated by gencpp from file common_msgs/GPSVelocity.msg
// DO NOT EDIT!


#ifndef COMMON_MSGS_MESSAGE_GPSVELOCITY_H
#define COMMON_MSGS_MESSAGE_GPSVELOCITY_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>

namespace common_msgs
{
template <class ContainerAllocator>
struct GPSVelocity_
{
  typedef GPSVelocity_<ContainerAllocator> Type;

  GPSVelocity_()
    : header()
    , lat_vel(0.0)
    , lon_vel(0.0)
    , alt_vel(0.0)  {
    }
  GPSVelocity_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , lat_vel(0.0)
    , lon_vel(0.0)
    , alt_vel(0.0)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef double _lat_vel_type;
  _lat_vel_type lat_vel;

   typedef double _lon_vel_type;
  _lon_vel_type lon_vel;

   typedef double _alt_vel_type;
  _alt_vel_type alt_vel;




  typedef boost::shared_ptr< ::common_msgs::GPSVelocity_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::common_msgs::GPSVelocity_<ContainerAllocator> const> ConstPtr;

}; // struct GPSVelocity_

typedef ::common_msgs::GPSVelocity_<std::allocator<void> > GPSVelocity;

typedef boost::shared_ptr< ::common_msgs::GPSVelocity > GPSVelocityPtr;
typedef boost::shared_ptr< ::common_msgs::GPSVelocity const> GPSVelocityConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::common_msgs::GPSVelocity_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::common_msgs::GPSVelocity_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace common_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'common_msgs': ['/home/bran/Documents/gittest/src/common_msgs/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::common_msgs::GPSVelocity_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::common_msgs::GPSVelocity_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::common_msgs::GPSVelocity_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::common_msgs::GPSVelocity_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::common_msgs::GPSVelocity_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::common_msgs::GPSVelocity_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::common_msgs::GPSVelocity_<ContainerAllocator> >
{
  static const char* value()
  {
    return "cd07811c37397d55c764919d4433214e";
  }

  static const char* value(const ::common_msgs::GPSVelocity_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xcd07811c37397d55ULL;
  static const uint64_t static_value2 = 0xc764919d4433214eULL;
};

template<class ContainerAllocator>
struct DataType< ::common_msgs::GPSVelocity_<ContainerAllocator> >
{
  static const char* value()
  {
    return "common_msgs/GPSVelocity";
  }

  static const char* value(const ::common_msgs::GPSVelocity_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::common_msgs::GPSVelocity_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n\
float64 lat_vel\n\
float64 lon_vel\n\
float64 alt_vel\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
";
  }

  static const char* value(const ::common_msgs::GPSVelocity_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::common_msgs::GPSVelocity_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.lat_vel);
      stream.next(m.lon_vel);
      stream.next(m.alt_vel);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct GPSVelocity_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::common_msgs::GPSVelocity_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::common_msgs::GPSVelocity_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "lat_vel: ";
    Printer<double>::stream(s, indent + "  ", v.lat_vel);
    s << indent << "lon_vel: ";
    Printer<double>::stream(s, indent + "  ", v.lon_vel);
    s << indent << "alt_vel: ";
    Printer<double>::stream(s, indent + "  ", v.alt_vel);
  }
};

} // namespace message_operations
} // namespace ros

#endif // COMMON_MSGS_MESSAGE_GPSVELOCITY_H
