// Generated by gencpp from file common_msgs/Mission.msg
// DO NOT EDIT!


#ifndef COMMON_MSGS_MESSAGE_MISSION_H
#define COMMON_MSGS_MESSAGE_MISSION_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>
#include <common_msgs/Waypoint.h>
#include <sensor_msgs/NavSatFix.h>
#include <common_msgs/PixhawkCMD.h>
#include <common_msgs/PixhawkServo.h>

namespace common_msgs
{
template <class ContainerAllocator>
struct Mission_
{
  typedef Mission_<ContainerAllocator> Type;

  Mission_()
    : header()
    , m_type()
    , m_id(0)
    , waypoint()
    , gps_waypoint()
    , pixhawkCMD()
    , pixhawkServo()
    , redundancy()  {
    }
  Mission_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , m_type(_alloc)
    , m_id(0)
    , waypoint(_alloc)
    , gps_waypoint(_alloc)
    , pixhawkCMD(_alloc)
    , pixhawkServo(_alloc)
    , redundancy(_alloc)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _m_type_type;
  _m_type_type m_type;

   typedef uint32_t _m_id_type;
  _m_id_type m_id;

   typedef  ::common_msgs::Waypoint_<ContainerAllocator>  _waypoint_type;
  _waypoint_type waypoint;

   typedef  ::sensor_msgs::NavSatFix_<ContainerAllocator>  _gps_waypoint_type;
  _gps_waypoint_type gps_waypoint;

   typedef  ::common_msgs::PixhawkCMD_<ContainerAllocator>  _pixhawkCMD_type;
  _pixhawkCMD_type pixhawkCMD;

   typedef  ::common_msgs::PixhawkServo_<ContainerAllocator>  _pixhawkServo_type;
  _pixhawkServo_type pixhawkServo;

   typedef std::vector<double, typename ContainerAllocator::template rebind<double>::other >  _redundancy_type;
  _redundancy_type redundancy;




  typedef boost::shared_ptr< ::common_msgs::Mission_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::common_msgs::Mission_<ContainerAllocator> const> ConstPtr;

}; // struct Mission_

typedef ::common_msgs::Mission_<std::allocator<void> > Mission;

typedef boost::shared_ptr< ::common_msgs::Mission > MissionPtr;
typedef boost::shared_ptr< ::common_msgs::Mission const> MissionConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::common_msgs::Mission_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::common_msgs::Mission_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace common_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'common_msgs': ['/home/bran/Documents/gittest/src/common_msgs/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::common_msgs::Mission_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::common_msgs::Mission_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::common_msgs::Mission_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::common_msgs::Mission_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::common_msgs::Mission_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::common_msgs::Mission_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::common_msgs::Mission_<ContainerAllocator> >
{
  static const char* value()
  {
    return "c9fcf876b16f31ef90c5d998cf1295bb";
  }

  static const char* value(const ::common_msgs::Mission_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xc9fcf876b16f31efULL;
  static const uint64_t static_value2 = 0x90c5d998cf1295bbULL;
};

template<class ContainerAllocator>
struct DataType< ::common_msgs::Mission_<ContainerAllocator> >
{
  static const char* value()
  {
    return "common_msgs/Mission";
  }

  static const char* value(const ::common_msgs::Mission_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::common_msgs::Mission_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n\
string m_type\n\
uint32 m_id\n\
Waypoint waypoint\n\
sensor_msgs/NavSatFix gps_waypoint\n\
PixhawkCMD pixhawkCMD\n\
PixhawkServo pixhawkServo\n\
float64[] redundancy\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
================================================================================\n\
MSG: common_msgs/Waypoint\n\
Header header\n\
common_msgs/NavigationState navigation_state\n\
string m_type\n\
uint32 m_id\n\
\n\
================================================================================\n\
MSG: common_msgs/NavigationState\n\
Header header\n\
geometry_msgs/Pose pose\n\
geometry_msgs/Twist velocity\n\
geometry_msgs/Twist acceleration\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Pose\n\
# A representation of pose in free space, composed of position and orientation. \n\
Point position\n\
Quaternion orientation\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Point\n\
# This contains the position of a point in free space\n\
float64 x\n\
float64 y\n\
float64 z\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Quaternion\n\
# This represents an orientation in free space in quaternion form.\n\
\n\
float64 x\n\
float64 y\n\
float64 z\n\
float64 w\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Twist\n\
# This expresses velocity in free space broken into its linear and angular parts.\n\
Vector3  linear\n\
Vector3  angular\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Vector3\n\
# This represents a vector in free space. \n\
# It is only meant to represent a direction. Therefore, it does not\n\
# make sense to apply a translation to it (e.g., when applying a \n\
# generic rigid transformation to a Vector3, tf2 will only apply the\n\
# rotation). If you want your data to be translatable too, use the\n\
# geometry_msgs/Point message instead.\n\
\n\
float64 x\n\
float64 y\n\
float64 z\n\
================================================================================\n\
MSG: sensor_msgs/NavSatFix\n\
# Navigation Satellite fix for any Global Navigation Satellite System\n\
#\n\
# Specified using the WGS 84 reference ellipsoid\n\
\n\
# header.stamp specifies the ROS time for this measurement (the\n\
#        corresponding satellite time may be reported using the\n\
#        sensor_msgs/TimeReference message).\n\
#\n\
# header.frame_id is the frame of reference reported by the satellite\n\
#        receiver, usually the location of the antenna.  This is a\n\
#        Euclidean frame relative to the vehicle, not a reference\n\
#        ellipsoid.\n\
Header header\n\
\n\
# satellite fix status information\n\
NavSatStatus status\n\
\n\
# Latitude [degrees]. Positive is north of equator; negative is south.\n\
float64 latitude\n\
\n\
# Longitude [degrees]. Positive is east of prime meridian; negative is west.\n\
float64 longitude\n\
\n\
# Altitude [m]. Positive is above the WGS 84 ellipsoid\n\
# (quiet NaN if no altitude is available).\n\
float64 altitude\n\
\n\
# Position covariance [m^2] defined relative to a tangential plane\n\
# through the reported position. The components are East, North, and\n\
# Up (ENU), in row-major order.\n\
#\n\
# Beware: this coordinate system exhibits singularities at the poles.\n\
\n\
float64[9] position_covariance\n\
\n\
# If the covariance of the fix is known, fill it in completely. If the\n\
# GPS receiver provides the variance of each measurement, put them\n\
# along the diagonal. If only Dilution of Precision is available,\n\
# estimate an approximate covariance from that.\n\
\n\
uint8 COVARIANCE_TYPE_UNKNOWN = 0\n\
uint8 COVARIANCE_TYPE_APPROXIMATED = 1\n\
uint8 COVARIANCE_TYPE_DIAGONAL_KNOWN = 2\n\
uint8 COVARIANCE_TYPE_KNOWN = 3\n\
\n\
uint8 position_covariance_type\n\
\n\
================================================================================\n\
MSG: sensor_msgs/NavSatStatus\n\
# Navigation Satellite fix status for any Global Navigation Satellite System\n\
\n\
# Whether to output an augmented fix is determined by both the fix\n\
# type and the last time differential corrections were received.  A\n\
# fix is valid when status >= STATUS_FIX.\n\
\n\
int8 STATUS_NO_FIX =  -1        # unable to fix position\n\
int8 STATUS_FIX =      0        # unaugmented fix\n\
int8 STATUS_SBAS_FIX = 1        # with satellite-based augmentation\n\
int8 STATUS_GBAS_FIX = 2        # with ground-based augmentation\n\
\n\
int8 status\n\
\n\
# Bits defining which Global Navigation Satellite System signals were\n\
# used by the receiver.\n\
\n\
uint16 SERVICE_GPS =     1\n\
uint16 SERVICE_GLONASS = 2\n\
uint16 SERVICE_COMPASS = 4      # includes BeiDou.\n\
uint16 SERVICE_GALILEO = 8\n\
\n\
uint16 service\n\
\n\
================================================================================\n\
MSG: common_msgs/PixhawkCMD\n\
Header header\n\
uint8[3] cmd\n\
\n\
================================================================================\n\
MSG: common_msgs/PixhawkServo\n\
Header header\n\
float64[4] servo\n\
";
  }

  static const char* value(const ::common_msgs::Mission_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::common_msgs::Mission_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.m_type);
      stream.next(m.m_id);
      stream.next(m.waypoint);
      stream.next(m.gps_waypoint);
      stream.next(m.pixhawkCMD);
      stream.next(m.pixhawkServo);
      stream.next(m.redundancy);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Mission_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::common_msgs::Mission_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::common_msgs::Mission_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "m_type: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.m_type);
    s << indent << "m_id: ";
    Printer<uint32_t>::stream(s, indent + "  ", v.m_id);
    s << indent << "waypoint: ";
    s << std::endl;
    Printer< ::common_msgs::Waypoint_<ContainerAllocator> >::stream(s, indent + "  ", v.waypoint);
    s << indent << "gps_waypoint: ";
    s << std::endl;
    Printer< ::sensor_msgs::NavSatFix_<ContainerAllocator> >::stream(s, indent + "  ", v.gps_waypoint);
    s << indent << "pixhawkCMD: ";
    s << std::endl;
    Printer< ::common_msgs::PixhawkCMD_<ContainerAllocator> >::stream(s, indent + "  ", v.pixhawkCMD);
    s << indent << "pixhawkServo: ";
    s << std::endl;
    Printer< ::common_msgs::PixhawkServo_<ContainerAllocator> >::stream(s, indent + "  ", v.pixhawkServo);
    s << indent << "redundancy[]" << std::endl;
    for (size_t i = 0; i < v.redundancy.size(); ++i)
    {
      s << indent << "  redundancy[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.redundancy[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // COMMON_MSGS_MESSAGE_MISSION_H
