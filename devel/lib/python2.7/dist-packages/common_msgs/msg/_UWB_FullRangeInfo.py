# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from common_msgs/UWB_FullRangeInfo.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import std_msgs.msg

class UWB_FullRangeInfo(genpy.Message):
  _md5sum = "6eed76e0c8b91a8a7fb2c268438b77be"
  _type = "common_msgs/UWB_FullRangeInfo"
  _has_header = True #flag to mark the presence of a Header object
  _full_text = """Header header

uint32 nodeId

uint16 msgType
uint16 msgId

uint32 responderId

uint8 rangeStatus
uint8 antennaMode
uint16 stopwatchTime

uint32 precisionRangeMm
uint32 coarseRangeMm
uint32 filteredRangeMm

uint16 precisionRangeErrEst
uint16 coarseRangeErrEst
uint16 filteredRangeErrEst

uint16 filteredRangeVel
uint16 filteredRangeVelErrEst

uint8 rangeMeasurementType
uint8 reserved

uint16 reqLEDFlags
uint16 respLEDFlags

uint16 noise
uint16 vPeak

uint32 coarseTOFInBins

uint32 timestamp

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id
"""
  __slots__ = ['header','nodeId','msgType','msgId','responderId','rangeStatus','antennaMode','stopwatchTime','precisionRangeMm','coarseRangeMm','filteredRangeMm','precisionRangeErrEst','coarseRangeErrEst','filteredRangeErrEst','filteredRangeVel','filteredRangeVelErrEst','rangeMeasurementType','reserved','reqLEDFlags','respLEDFlags','noise','vPeak','coarseTOFInBins','timestamp']
  _slot_types = ['std_msgs/Header','uint32','uint16','uint16','uint32','uint8','uint8','uint16','uint32','uint32','uint32','uint16','uint16','uint16','uint16','uint16','uint8','uint8','uint16','uint16','uint16','uint16','uint32','uint32']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       header,nodeId,msgType,msgId,responderId,rangeStatus,antennaMode,stopwatchTime,precisionRangeMm,coarseRangeMm,filteredRangeMm,precisionRangeErrEst,coarseRangeErrEst,filteredRangeErrEst,filteredRangeVel,filteredRangeVelErrEst,rangeMeasurementType,reserved,reqLEDFlags,respLEDFlags,noise,vPeak,coarseTOFInBins,timestamp

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(UWB_FullRangeInfo, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.header is None:
        self.header = std_msgs.msg.Header()
      if self.nodeId is None:
        self.nodeId = 0
      if self.msgType is None:
        self.msgType = 0
      if self.msgId is None:
        self.msgId = 0
      if self.responderId is None:
        self.responderId = 0
      if self.rangeStatus is None:
        self.rangeStatus = 0
      if self.antennaMode is None:
        self.antennaMode = 0
      if self.stopwatchTime is None:
        self.stopwatchTime = 0
      if self.precisionRangeMm is None:
        self.precisionRangeMm = 0
      if self.coarseRangeMm is None:
        self.coarseRangeMm = 0
      if self.filteredRangeMm is None:
        self.filteredRangeMm = 0
      if self.precisionRangeErrEst is None:
        self.precisionRangeErrEst = 0
      if self.coarseRangeErrEst is None:
        self.coarseRangeErrEst = 0
      if self.filteredRangeErrEst is None:
        self.filteredRangeErrEst = 0
      if self.filteredRangeVel is None:
        self.filteredRangeVel = 0
      if self.filteredRangeVelErrEst is None:
        self.filteredRangeVelErrEst = 0
      if self.rangeMeasurementType is None:
        self.rangeMeasurementType = 0
      if self.reserved is None:
        self.reserved = 0
      if self.reqLEDFlags is None:
        self.reqLEDFlags = 0
      if self.respLEDFlags is None:
        self.respLEDFlags = 0
      if self.noise is None:
        self.noise = 0
      if self.vPeak is None:
        self.vPeak = 0
      if self.coarseTOFInBins is None:
        self.coarseTOFInBins = 0
      if self.timestamp is None:
        self.timestamp = 0
    else:
      self.header = std_msgs.msg.Header()
      self.nodeId = 0
      self.msgType = 0
      self.msgId = 0
      self.responderId = 0
      self.rangeStatus = 0
      self.antennaMode = 0
      self.stopwatchTime = 0
      self.precisionRangeMm = 0
      self.coarseRangeMm = 0
      self.filteredRangeMm = 0
      self.precisionRangeErrEst = 0
      self.coarseRangeErrEst = 0
      self.filteredRangeErrEst = 0
      self.filteredRangeVel = 0
      self.filteredRangeVelErrEst = 0
      self.rangeMeasurementType = 0
      self.reserved = 0
      self.reqLEDFlags = 0
      self.respLEDFlags = 0
      self.noise = 0
      self.vPeak = 0
      self.coarseTOFInBins = 0
      self.timestamp = 0

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_get_struct_3I().pack(_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs))
      _x = self.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_get_struct_I2HI2BH3I5H2B4H2I().pack(_x.nodeId, _x.msgType, _x.msgId, _x.responderId, _x.rangeStatus, _x.antennaMode, _x.stopwatchTime, _x.precisionRangeMm, _x.coarseRangeMm, _x.filteredRangeMm, _x.precisionRangeErrEst, _x.coarseRangeErrEst, _x.filteredRangeErrEst, _x.filteredRangeVel, _x.filteredRangeVelErrEst, _x.rangeMeasurementType, _x.reserved, _x.reqLEDFlags, _x.respLEDFlags, _x.noise, _x.vPeak, _x.coarseTOFInBins, _x.timestamp))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.header is None:
        self.header = std_msgs.msg.Header()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs,) = _get_struct_3I().unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.header.frame_id = str[start:end].decode('utf-8')
      else:
        self.header.frame_id = str[start:end]
      _x = self
      start = end
      end += 56
      (_x.nodeId, _x.msgType, _x.msgId, _x.responderId, _x.rangeStatus, _x.antennaMode, _x.stopwatchTime, _x.precisionRangeMm, _x.coarseRangeMm, _x.filteredRangeMm, _x.precisionRangeErrEst, _x.coarseRangeErrEst, _x.filteredRangeErrEst, _x.filteredRangeVel, _x.filteredRangeVelErrEst, _x.rangeMeasurementType, _x.reserved, _x.reqLEDFlags, _x.respLEDFlags, _x.noise, _x.vPeak, _x.coarseTOFInBins, _x.timestamp,) = _get_struct_I2HI2BH3I5H2B4H2I().unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_get_struct_3I().pack(_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs))
      _x = self.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_get_struct_I2HI2BH3I5H2B4H2I().pack(_x.nodeId, _x.msgType, _x.msgId, _x.responderId, _x.rangeStatus, _x.antennaMode, _x.stopwatchTime, _x.precisionRangeMm, _x.coarseRangeMm, _x.filteredRangeMm, _x.precisionRangeErrEst, _x.coarseRangeErrEst, _x.filteredRangeErrEst, _x.filteredRangeVel, _x.filteredRangeVelErrEst, _x.rangeMeasurementType, _x.reserved, _x.reqLEDFlags, _x.respLEDFlags, _x.noise, _x.vPeak, _x.coarseTOFInBins, _x.timestamp))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.header is None:
        self.header = std_msgs.msg.Header()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs,) = _get_struct_3I().unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.header.frame_id = str[start:end].decode('utf-8')
      else:
        self.header.frame_id = str[start:end]
      _x = self
      start = end
      end += 56
      (_x.nodeId, _x.msgType, _x.msgId, _x.responderId, _x.rangeStatus, _x.antennaMode, _x.stopwatchTime, _x.precisionRangeMm, _x.coarseRangeMm, _x.filteredRangeMm, _x.precisionRangeErrEst, _x.coarseRangeErrEst, _x.filteredRangeErrEst, _x.filteredRangeVel, _x.filteredRangeVelErrEst, _x.rangeMeasurementType, _x.reserved, _x.reqLEDFlags, _x.respLEDFlags, _x.noise, _x.vPeak, _x.coarseTOFInBins, _x.timestamp,) = _get_struct_I2HI2BH3I5H2B4H2I().unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
def _get_struct_I():
    global _struct_I
    return _struct_I
_struct_3I = None
def _get_struct_3I():
    global _struct_3I
    if _struct_3I is None:
        _struct_3I = struct.Struct("<3I")
    return _struct_3I
_struct_I2HI2BH3I5H2B4H2I = None
def _get_struct_I2HI2BH3I5H2B4H2I():
    global _struct_I2HI2BH3I5H2B4H2I
    if _struct_I2HI2BH3I5H2B4H2I is None:
        _struct_I2HI2BH3I5H2B4H2I = struct.Struct("<I2HI2BH3I5H2B4H2I")
    return _struct_I2HI2BH3I5H2B4H2I
